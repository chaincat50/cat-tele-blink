package org.butch.repositories;

import org.butch.bots.gateway.repository.CommonDao;
import org.butch.bots.gateway.repository.Storage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.HashMap;
import java.util.Objects;

public class BlinksGatewayStorageImpl implements Storage {
    private static final String NAME = "blinks_gateway_bot";
    private static final String[] VERSIONS = {
            null,
            "1",
    };
    private static final HashMap<String, InitFunction> INIT_FUNCTIONS = new HashMap<>();

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final DataSource dataSource;
    private final VersionStorage versionStorage;

    static {
        INIT_FUNCTIONS.put(VERSIONS[0], BlinksGatewayStorageImpl::initV0ToV1);
    }

    public BlinksGatewayStorageImpl(DataSource dataSource, VersionStorage versionStorage) {
        this.dataSource = dataSource;
        this.versionStorage = versionStorage;
    }

    @Override
    public void init() {
        String version = versionStorage.getVersion(NAME);
        try (Connection connection = dataSource.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                for (int i = 0; i < VERSIONS.length - 1; ++i) {
                    if (Objects.equals(version, VERSIONS[i])) {
                        final InitFunction initFunction = INIT_FUNCTIONS.get(version);
                        version = VERSIONS[i + 1];
                        LOGGER.info("Updating " + NAME + " schema to version " + version);
                        initFunction.init(stmt);
                    }
                }
            }
        } catch (SQLException ex) {
            LOGGER.error("init", ex);
        }
        if (!versionStorage.setVersion(NAME, VERSIONS[VERSIONS.length - 1])) {
            LOGGER.error("Could not update version: " + NAME);
        }
    }

    private static void initV0ToV1(Statement stmt) throws SQLException {
        stmt.executeUpdate("" +
                "CREATE TABLE IF NOT EXISTS blinks_gateway_config ( " +
                "id SERIAL PRIMARY KEY, " +
                "body TEXT" +
                ");"
        );
    }

    @Override
    public CommonDao getConfig() {
        LOGGER.debug("getConfig");
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "SELECT id, body " +
                    "FROM blinks_gateway_config " +
                    "ORDER BY id DESC " +
                    "LIMIT 1;")) {
                final ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    final CommonDao ret = new CommonDao();
                    ret.setId(resultSet.getInt(1));
                    ret.setBody(resultSet.getString(2));
                    return ret;
                }
                return null;
            }
        } catch (Exception ex) {
            LOGGER.error("getConfig", ex);
            return null;
        }
    }

    @Override
    public int setConfig(CommonDao config) {
        LOGGER.debug("setConfig: " + config);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "INSERT INTO blinks_gateway_config (body) " +
                    "VALUES (?) " +
                    "RETURNING id;")) {
                stmt.setString(1, config.getBody());
                final ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
                LOGGER.error("id was not returned");
                return -1;
            }
        } catch (Exception ex) {
            LOGGER.error("setConfig: " + config, ex);
            return -1;
        }
    }
}
