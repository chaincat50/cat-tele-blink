package org.butch.repositories;

import org.butch.bots.blinks.repository.CommonDao;
import org.butch.bots.blinks.repository.Storage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

public class BlinksStorageImpl implements Storage {
    private static final String NAME = "blinks_bot";
    private static final String[] VERSIONS = {
            null,
            "1",
            "2",
    };
    private static final HashMap<String, InitFunction> INIT_FUNCTIONS = new HashMap<>();

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final String prefix;
    private final DataSource dataSource;
    private final VersionStorage versionStorage;
    private final Tables tables;

    static {
        INIT_FUNCTIONS.put(VERSIONS[0], BlinksStorageImpl::initV0ToV1);
        INIT_FUNCTIONS.put(VERSIONS[1], BlinksStorageImpl::initV1ToV2);
    }

    public BlinksStorageImpl(String prefix, DataSource dataSource, VersionStorage versionStorage) {
        this.prefix = prefix;
        this.dataSource = dataSource;
        this.versionStorage = versionStorage;
        this.tables = new Tables(
                prefix + "blinks_config",
                prefix + "blinks_users",
                prefix + "blinks_logs"
        );
    }

    @Override
    public void init() {
        String version = versionStorage.getVersion(getName());
        try (Connection connection = dataSource.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                for (int i = 0; i < VERSIONS.length - 1; ++i) {
                    if (Objects.equals(version, VERSIONS[i])) {
                        final InitFunction initFunction = INIT_FUNCTIONS.get(version);
                        version = VERSIONS[i + 1];
                        LOGGER.info("Updating " + getName() + " schema to version " + version);
                        initFunction.init(stmt, tables);
                    }
                }
            }
        } catch (SQLException ex) {
            LOGGER.error("init", ex);
        }
        if (!versionStorage.setVersion(getName(), VERSIONS[VERSIONS.length - 1])) {
            LOGGER.error("Could not update version: " + getName());
        }
    }

    private static void initV0ToV1(Statement stmt, Tables tables) throws SQLException {
        stmt.executeUpdate("" +
                "CREATE TABLE IF NOT EXISTS " + tables.getConfig() + " ( " +
                "id SERIAL PRIMARY KEY, " +
                "body TEXT" +
                ");"
        );
        stmt.executeUpdate("" +
                "CREATE TABLE IF NOT EXISTS " + tables.getUsers() + " ( " +
                "id SERIAL PRIMARY KEY, " +
                "body TEXT" +
                ");"
        );
    }

    private static void initV1ToV2(Statement stmt, Tables tables) throws SQLException {
        stmt.executeUpdate("" +
                "CREATE TABLE IF NOT EXISTS " + tables.getLogs() + " ( " +
                "id SERIAL PRIMARY KEY, " +
                "body TEXT" +
                ");"
        );
    }

    @Override
    public CommonDao getConfig() {
        LOGGER.debug("getConfig");
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "SELECT id, body " +
                    "FROM " + tables.getConfig() + " " +
                    "ORDER BY id DESC " +
                    "LIMIT 1;")) {
                final ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    final CommonDao ret = new CommonDao();
                    ret.setId(resultSet.getInt(1));
                    ret.setBody(resultSet.getString(2));
                    return ret;
                }
                return null;
            }
        } catch (Exception ex) {
            LOGGER.error("getConfig", ex);
            return null;
        }
    }

    @Override
    public int setConfig(CommonDao config) {
        LOGGER.debug("setConfig: " + config);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "INSERT INTO " + tables.getConfig() + " (body) " +
                    "VALUES (?) " +
                    "RETURNING id;")) {
                stmt.setString(1, config.getBody());
                final ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
                LOGGER.error("id was not returned");
                return -1;
            }
        } catch (Exception ex) {
            LOGGER.error("setConfig: " + config, ex);
            return -1;
        }
    }

    @Override
    public int insertUser(CommonDao user) {
        LOGGER.debug("insertUser: " + user);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "INSERT INTO " + tables.getUsers() + " (body) " +
                    "VALUES (?) " +
                    "RETURNING id;")) {
                stmt.setString(1, user.getBody());
                final ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
                LOGGER.error("id was not returned");
                return -1;
            }
        } catch (Exception ex) {
            LOGGER.error("insertUser: " + user, ex);
            return -1;
        }
    }

    @Override
    public boolean updateUser(CommonDao user) {
        LOGGER.debug("updateUser: " + user);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "UPDATE " + tables.getUsers() + " SET " +
                    "body = ? " +
                    "WHERE id = ?;")) {
                stmt.setString(1, user.getBody());
                stmt.setInt(2, user.getId());
                return stmt.executeUpdate() == 1;
            }
        } catch (Exception ex) {
            LOGGER.error("updateUser: " + user, ex);
            return false;
        }
    }

    @Override
    public boolean deleteUser(CommonDao user) {
        LOGGER.debug("deleteUser: " + user);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "DELETE FROM " + tables.getUsers() + " WHERE id = ?;")) {
                stmt.setInt(1, user.getId());
                return stmt.executeUpdate() == 1;
            }
        } catch (Exception ex) {
            LOGGER.error("deleteUser: " + user, ex);
            return false;
        }
    }

    @Override
    public List<CommonDao> queryUsers() {
        LOGGER.debug("queryUsers");
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "SELECT id, body FROM " + tables.getUsers() + ";")) {
                final ResultSet resultSet = stmt.executeQuery();
                final List<CommonDao> ret = new ArrayList<>();
                while (resultSet.next()) {
                    final CommonDao dao = new CommonDao();
                    dao.setId(resultSet.getInt(1));
                    dao.setBody(resultSet.getString(2));
                    ret.add(dao);
                }
                return ret;
            }
        } catch (Exception ex) {
            LOGGER.error("queryUsers", ex);
            return Collections.emptyList();
        }
    }

    @Override
    public boolean insertLogEntry(CommonDao logEntry) {
        LOGGER.debug("insertLogEntry: " + logEntry);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "INSERT INTO " + tables.getLogs() + " (body) " +
                    "VALUES (?) " +
                    "RETURNING id;")) {
                stmt.setString(1, logEntry.getBody());
                final ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getInt(1) > 0;
                }
                LOGGER.error("id was not returned");
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("insertLogEntry: " + logEntry, ex);
            return false;
        }
    }

    @Override
    public List<CommonDao> queryLogEntries(int count) {
        LOGGER.debug("queryLogEntries: " + count);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "SELECT id, body FROM " + tables.getLogs() + " " +
                    "ORDER BY id DESC " +
                    "LIMIT ?;")) {
                stmt.setInt(1, count);
                final ResultSet resultSet = stmt.executeQuery();
                final List<CommonDao> ret = new ArrayList<>();
                while (resultSet.next()) {
                    final CommonDao dao = new CommonDao();
                    dao.setId(resultSet.getInt(1));
                    dao.setBody(resultSet.getString(2));
                    ret.add(dao);
                }
                return ret;
            }
        } catch (Exception ex) {
            LOGGER.error("queryLogEntries: " + count, ex);
            return Collections.emptyList();
        }
    }

    private String getName() {
        return prefix + NAME;
    }

    private static class Tables {
        private final String config;
        private final String users;
        private final String logs;

        private Tables(String config, String users, String logs) {
            this.config = config;
            this.users = users;
            this.logs = logs;
        }

        public String getConfig() {
            return config;
        }

        public String getUsers() {
            return users;
        }

        public String getLogs() {
            return logs;
        }
    }

    public interface InitFunction {
        void init(Statement stmt, Tables tables) throws SQLException;
    }
}
