package org.butch.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "blinks-master")
public class BlinksMasterConfig {
    private boolean allowAutoJoin = false;
    private String prefix = "";
    private String token = null;
    private String telegraphWriter = null;
    private String publicKey = null;
    private String privateKey = null;

    public boolean getAllowAutoJoin() {
        return allowAutoJoin;
    }

    public void setAllowAutoJoin(boolean allowAutoJoin) {
        this.allowAutoJoin = allowAutoJoin;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTelegraphWriter() {
        return telegraphWriter;
    }

    public void setTelegraphWriter(String telegraphWriter) {
        this.telegraphWriter = telegraphWriter;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }
}
