package org.butch.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "blinks")
public class BlinksConfig {
    private boolean allowAutoJoin = false;
    @Deprecated
    private Boolean allowForwarding = null;
    private String sendUserHelp = null;
    private List<Bot> bots = new ArrayList<>();

    public boolean getAllowAutoJoin() {
        return allowAutoJoin;
    }

    public void setAllowAutoJoin(boolean allowAutoJoin) {
        this.allowAutoJoin = allowAutoJoin;
    }

    public Boolean getAllowForwarding() {
        return allowForwarding;
    }

    public void setAllowForwarding(Boolean allowForwarding) {
        this.allowForwarding = allowForwarding;
    }

    public String getSendUserHelp() {
        return sendUserHelp;
    }

    public void setSendUserHelp(String sendUserHelp) {
        this.sendUserHelp = sendUserHelp;
    }

    public List<Bot> getBots() {
        return bots;
    }

    public void setBots(List<Bot> bots) {
        this.bots = bots;
    }

    public static class Bot {
        private String prefix = "";
        private String token;

        public String getPrefix() {
            return prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}
