package org.butch.config;

import org.butch.bots.gateway.model.Group;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "blinks-gateway")
public class BlinksGatewayConfig {
    private List<Group> groups = Collections.emptyList();

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
}
