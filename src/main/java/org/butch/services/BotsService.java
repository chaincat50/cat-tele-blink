package org.butch.services;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.butch.bots.base.Bot;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.master.BlinksMasterBot;
import org.butch.bots.gateway.BlinksGatewayBot;
import org.butch.config.BlinksConfig;
import org.butch.config.BlinksGatewayConfig;
import org.butch.config.BlinksMasterConfig;
import org.butch.encryption.CryptoUtils;
import org.butch.repositories.BlinksGatewayStorageImpl;
import org.butch.repositories.BlinksStorageImpl;
import org.butch.repositories.VersionStorage;
import org.butch.telegraph.BTContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
@Service
public class BotsService {
    private final Logger LOGGER = LoggerFactory.getLogger(BotsService.class);

    private final BlinksConfig blinksConfig;
    private final BlinksMasterConfig blinksMasterConfig;
    private final BlinksGatewayConfig blinksGatewayConfig;
    private final List<BotRunnable> bots = new ArrayList<>();

    // Don't use those directly
    private DataSource pDataSource;
    private VersionStorage pVersionStorage;

    @Autowired
    private ApplicationContext appContext;

    @Scheduled(fixedRate = 1000L)
    public void timer() {
        final long now = System.currentTimeMillis();

        for (BotRunnable runnable : bots) {
            try {
                runnable.bot.onTimer(now);
                if (runnable.ex != null) {
                    LOGGER.info(runnable.bot.getBotUsername() + " recovered");
                    runnable.ex = null;
                }
            } catch (Throwable ex) {
                if (runnable.ex == null) {
                    LOGGER.error(runnable.bot.getBotUsername() + " exception", ex);
                }
                runnable.ex = ex;
            }
        }
    }

    @Autowired
    public BotsService(BlinksConfig blinksConfig,
                       BlinksMasterConfig blinksMasterConfig,
                       BlinksGatewayConfig blinksGatewayConfig) {
        this.blinksConfig = blinksConfig;
        this.blinksMasterConfig = blinksMasterConfig;
        this.blinksGatewayConfig = blinksGatewayConfig;
    }

    @PostConstruct
    public void init() throws TelegramApiException {
        BTContext.init();

        final String dbUrl = System.getenv("JDBC_DATABASE_URL");

        final TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);

        final String bbKey = System.getenv("BLINKSBOT_KEY");

        final BlinksMasterBot masterBot;
        if (blinksMasterConfig.getToken() != null &&
                !blinksMasterConfig.getToken().isEmpty() &&
                blinksMasterConfig.getPrefix() != null) {
            if (!blinksConfig.getBots().isEmpty()) {
                if (blinksConfig.getBots()
                        .stream()
                        .map(BlinksConfig.Bot::getPrefix)
                        .collect(Collectors.toSet()).contains(blinksMasterConfig.getPrefix()))
                    throw new RuntimeException("Blinks master bots prefix cannot be the same as one of blinks bots.");
                if (blinksConfig.getBots()
                        .stream()
                        .map(BlinksConfig.Bot::getToken)
                        .collect(Collectors.toSet()).contains(blinksMasterConfig.getToken()))
                    throw new RuntimeException("Blinks master bots token cannot be the same as one of blinks bots.");
            }

            masterBot = tryInitBot(true, "blinks-master", botsApi,
                    () -> {
                        final String prefix = preparePrefix(blinksMasterConfig.getPrefix());
                        final DataSource dataSource = getDataSource(dbUrl);
                        final VersionStorage versionStorage = getVersionStorage(dataSource);
                        final BlinksStorageImpl storage = new BlinksStorageImpl(prefix, dataSource, versionStorage);
                        RSAPublicKey publicKey;
                        try {
                            publicKey = readRSAPublicKey(blinksMasterConfig.getPublicKey());
                        } catch (IOException ex) {
                            LOGGER.warn(ex.toString());
                            publicKey = null;
                        }
                        RSAPrivateKey privateKey;
                        try {
                            privateKey = readRSAPrivateKey(blinksMasterConfig.getPrivateKey());
                        } catch (IOException ex) {
                            LOGGER.warn(ex.toString());
                            privateKey = null;
                        }
                        return new BlinksMasterBot(blinksMasterConfig.getToken(),
                                blinksGatewayConfig.getGroups(), storage,
                                CryptoUtils.keyFromString(bbKey), blinksMasterConfig.getAllowAutoJoin(),
                                blinksMasterConfig.getTelegraphWriter(), publicKey, privateKey);
                    }
            );
        } else {
            masterBot = null;
        }

        final String bbToken = System.getenv("BLINKSBOT_ID");
        if (!blinksConfig.getBots().isEmpty()) {
            LOGGER.info("Launching bots from config.");
            LOGGER.info("allowAutoJoin = " + blinksConfig.getAllowAutoJoin());
            if (blinksConfig.getBots()
                    .stream()
                    .map(BlinksConfig.Bot::getPrefix)
                    .collect(Collectors.toSet())
                    .size() != blinksConfig.getBots().size()) {
                throw new RuntimeException("Blinks bots prefixes contain duplicates.");
            }

            for (final BlinksConfig.Bot botConfig : blinksConfig.getBots()) {
                final String prefix = preparePrefix(botConfig.getPrefix());
                // TODO: consider validating prefixes
                final String token = botConfig.getToken();
                tryInitBot(true, prefix + "blinks", botsApi,
                        () -> {
                            final DataSource dataSource = getDataSource(dbUrl);
                            final VersionStorage versionStorage = getVersionStorage(dataSource);
                            final BlinksStorageImpl storage = new BlinksStorageImpl(prefix, dataSource, versionStorage);
                            return new BlinksBot(token, storage, CryptoUtils.keyFromString(bbKey),
                                    blinksConfig.getAllowAutoJoin(), blinksConfig.getSendUserHelp(), masterBot);
                        }
                );
            }
        } else if (bbToken != null) {
            LOGGER.info("Launching bot from env.");
            LOGGER.info("allowAutoJoin = " + blinksConfig.getAllowAutoJoin());
            tryInitBot(true, "blinks", botsApi,
                    () -> {
                        final DataSource dataSource = getDataSource(dbUrl);
                        final VersionStorage versionStorage = getVersionStorage(dataSource);
                        final BlinksStorageImpl storage = new BlinksStorageImpl("", dataSource, versionStorage);
                        return new BlinksBot(bbToken, storage, CryptoUtils.keyFromString(bbKey),
                                blinksConfig.getAllowAutoJoin(), blinksConfig.getSendUserHelp(), masterBot);
                    }
            );
        } else {
            tryInitBot(false, "blinks", botsApi, () -> null);
        }


        tryInitBot(!blinksGatewayConfig.getGroups().isEmpty(), "blinks-gateway", botsApi,
                () -> {
                    final String token = System.getenv("BLINKSGATEWAYBOT_ID");
                    final String key = System.getenv("BLINKSGATEWAYBOT_KEY");
                    final DataSource dataSource = getDataSource(dbUrl);
                    final VersionStorage versionStorage = getVersionStorage(dataSource);
                    final BlinksGatewayStorageImpl storage = new BlinksGatewayStorageImpl(dataSource, versionStorage);
                    return new BlinksGatewayBot(token, blinksGatewayConfig.getGroups(),
                            storage, CryptoUtils.keyFromString(key));
                }
        );
    }

    private <TBot extends Bot> TBot tryInitBot(boolean enabled,
                                               String name,
                                               TelegramBotsApi botsApi,
                                               Callable<TBot> producer) {
        if (enabled) {
            LOGGER.info("Starting " + name + " bot.");
            try {
                final TBot ret = producer.call();
                initBot(botsApi, ret);
                return ret;
            } catch (Throwable ex) {
                LOGGER.error("Could not init bot " + name, ex);
            }
        } else {
            LOGGER.info("Skipping " + name + " bot.");
        }
        return null;
    }

    private void initBot(TelegramBotsApi botsApi, Bot bot) throws TelegramApiException {
        bots.add(new BotRunnable(bot));
        botsApi.registerBot(bot);
    }

    private DataSource getDataSource(String dbUrl) {
        if (pDataSource == null) {
            final HikariConfig config = new HikariConfig();
            config.setJdbcUrl(dbUrl);
            pDataSource = new HikariDataSource(config);
        }
        return pDataSource;
    }

    private VersionStorage getVersionStorage(DataSource dataSource) {
        if (pVersionStorage == null) {
            pVersionStorage = new VersionStorage(dataSource);
            pVersionStorage.init();
        }
        return pVersionStorage;
    }

    private String preparePrefix(String prefix) {
        return prefix == null || prefix.isEmpty()
                ? ""
                : (prefix.endsWith("_") ? prefix : prefix + "_");
    }

    private RSAPublicKey readRSAPublicKey(String str) throws IOException {
        if (str == null)
            return null;
        try {
            final String key = str
                    .replace("-----BEGIN PUBLIC KEY-----", "")
                    .replace("-----END PUBLIC KEY-----", "")
                    .replace("\n", "")
                    .trim();
            final byte[] bytes = Base64.getMimeDecoder().decode(key);
            final KeyFactory kf = KeyFactory.getInstance("RSA");
            return (RSAPublicKey) kf.generatePublic(new X509EncodedKeySpec(bytes));
        } catch (Exception ex) {
            throw new IOException("Could not read public key.", ex);
        }
    }

    private RSAPrivateKey readRSAPrivateKey(String str) throws IOException {
        if (str == null)
            return null;
        try {
            final String key = str
                    .replace("-----BEGIN PRIVATE KEY-----", "")
                    .replace("-----END PRIVATE KEY-----", "")
                    .replace("\n", "")
                    .trim();
            final byte[] bytes = Base64.getMimeDecoder().decode(key);
            final KeyFactory kf = KeyFactory.getInstance("RSA");
            return (RSAPrivateKey) kf.generatePrivate(new PKCS8EncodedKeySpec(bytes));
        } catch (Exception ex) {
            throw new IOException("Could not read private key.", ex);
        }
    }

    private static class BotRunnable {
        private final Bot bot;
        private Throwable ex = null;

        private BotRunnable(Bot bot) {
            this.bot = bot;
        }
    }
}
