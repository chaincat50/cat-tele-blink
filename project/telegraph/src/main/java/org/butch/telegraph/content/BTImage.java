package org.butch.telegraph.content;

public class BTImage implements BTContentNode {
    private final String url;

    public BTImage(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
