package org.butch.telegraph;

import org.butch.telegraph.content.BTAccount;
import org.butch.telegraph.content.BTContentNode;
import org.butch.telegraph.content.BTImage;
import org.butch.telegraph.content.BTText;
import org.butch.telegraph.model.FileResult;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegraph.api.methods.CreateAccount;
import org.telegram.telegraph.api.methods.CreatePage;
import org.telegram.telegraph.api.objects.*;
import org.telegram.telegraph.exceptions.TelegraphException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class BTWriter {
    private final RestTemplate rest = new RestTemplate();

    public BTAccount createAccount(String name) throws IOException {
        try {
            final Account account = new CreateAccount(name)
                    .setAuthorName(name)
                    .execute();
            return new BTAccount(name, account.getAccessToken());
        } catch (TelegraphException e) {
            throw new IOException(e);
        }
    }

    public BTImage uploadFile(InputStream file, String filename) throws IOException {
        final MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("image", new ImageResource(file, filename));
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

        final ResponseEntity<FileResult[]> response = rest.exchange("https://telegra.ph/upload",
                HttpMethod.POST, requestEntity, FileResult[].class);
        final FileResult[] files = response.getBody();
        if (files == null)
            throw new IOException("null body");
        if (files.length != 1)
            throw new IOException("length != 1: " + Arrays.toString(files));
        final FileResult fileResult = files[0];
        if (fileResult.getSrc() == null)
            throw new IOException(fileResult.getError());
        return new BTImage(fileResult.getSrc());
    }

    public String createPage(BTAccount account,
                             String title,
                             List<BTContentNode> content) throws IOException {
        try {
            final List<Node> contentList = new ArrayList<>();
            for (BTContentNode contentNode : content) {
                if (contentNode instanceof BTText) {
                    contentList.add(creatTextNode((BTText) contentNode));
                } else if (contentNode instanceof BTImage) {
                    contentList.add(createImageNode((BTImage) contentNode));
                } else {
                    throw new IOException("Unexpected content node type: " + contentNode.getClass());
                }
            }

            final Page page = new CreatePage(account.getToken(), title, contentList)
                    .setAuthorName(account.getName())
                    .setReturnContent(false)
                    .execute();
            return page.getUrl();
        } catch (TelegraphException e) {
            throw new IOException(e);
        }
    }

    private Node creatTextNode(BTText content) {
        return new NodeText(content.getText());
    }

    private Node createImageNode(BTImage content) {
        final NodeElement imageNode = new NodeElement();
        imageNode.setTag("img");
        imageNode.setAttrs(new HashMap<>());
        imageNode.getAttrs().put("src", content.getUrl());
        return imageNode;
    }

    private static class ImageResource extends ByteArrayResource {
        private final String filename;

        public ImageResource(InputStream stream, String filename) throws IOException {
            this(stream.readAllBytes(), filename);
        }

        public ImageResource(byte[] bytes, String filename) {
            super(bytes);
            this.filename = filename;
        }

        @Override
        public String getFilename() {
            return filename;
        }
    }
}
