package org.butch.telegraph.content;

public class BTText implements BTContentNode {
    private final String text;

    public BTText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
