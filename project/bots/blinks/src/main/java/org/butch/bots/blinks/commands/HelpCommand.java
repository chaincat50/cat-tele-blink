package org.butch.bots.blinks.commands;

import org.butch.bots.base.commands.BaseHelpCommand;
import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.model.Role;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class HelpCommand extends BaseHelpCommand<BlinksBot> {
    public static final int MAX_LENGTH = 2048;

    public HelpCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getHelp() {
        return "" +
                "<u><b>Описание работы бота.</b></u>\n" +
                "\n" +
                "Бот для анонимного обмена сообщениями.\n" +
                "\n" +
                "<u>Список поддерживаемых команд:</u>\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final Role userRole = bot.getRole(msg.getChatId(), msg.getUserId());
        final StringBuilder builder = new StringBuilder();
        builder.append(getHelp()).append("\n"); // Add this help first
        for (Command<?> command : bot.getCommands()) {
            if (command == this) // Don't print this help like general command
                continue;
            if (!command.addHelp())
                continue;
            final String help;
            if (command instanceof BlinksCommand) {
                final BlinksCommand blinksCommand = (BlinksCommand) command;
                if (!userRole.isAllowedCommand(blinksCommand.getRole()))
                    continue;
                help = blinksCommand.getHelp(userRole);
            } else {
                help = command.getHelp();
            }
            if (help != null) {
                builder.append(help).append("\n");
                if (builder.length() > MAX_LENGTH) {
                    bot.sendHtml(msg.getChatId(), builder.toString());
                    builder.setLength(0);
                }
            }
        }
        if (builder.length() > 0) {
            bot.sendHtml(msg.getChatId(), builder.toString());
        }
    }
}
