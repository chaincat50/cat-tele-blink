package org.butch.bots.blinks.master.commands.superadmin;

import org.butch.bots.BotLimits;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.commands.superadmin.SuperAdminCommand;
import org.butch.bots.blinks.master.BlinksMasterBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class REncodeCommand extends SuperAdminCommand {
    public REncodeCommand(BlinksMasterBot bot) {
        super(bot);
    }

    private BlinksMasterBot getBot() {
        return (BlinksMasterBot) bot;
    }

    @Override
    public String getCommand() {
        return "rencode";
    }

    @Override
    public String getDescription() {
        return "<текст> Закодировать текст (белый)";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;текст&gt;\n" +
                "Запрос на шифрование текста. " +
                "Результат - ссылка на телеграф с красной картинкой.\n" +
                "<b>текст</b> - Текст, который надо зашифровать. " +
                "Не может быть пустым или состоять из одних пробелов.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String chatId = msg.getChatId();
        final Integer messageId = msg.getMessage().getMessageId();
        final String text = msg.getParams().trim();
        if (text.isEmpty()) {
            bot.sendReply(chatId, messageId, "Текст не может быть пустым.");
            return null;
        }
        final String result = getBot().encodeRed(text);
        if (result == null) {
            bot.sendReply(chatId, messageId, "Не получилось закодировать текст.");
        } else {
            if (result.length() > BotLimits.MAX_MESSAGE_LENGTH) {
                bot.sendReply(chatId, messageId, "Сообщение слишком длинное. Попробуйте ещё раз.");
            } else {
                bot.sendReply(chatId, messageId, result, false);
            }
        }
        return null;
    }
}
