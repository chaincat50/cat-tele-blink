package org.butch.bots.blinks.commands.superadmin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.Role;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Objects;

public class SetAdminChatCommand extends SuperAdminCommand {
    public SetAdminChatCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "set_admin_chat";
    }

    @Override
    public String getDescription() {
        return "[name] Присоединить к боту текущий чат";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " [name]\n" +
                "Запрос на присоединение к боту текущего чата с ролью Администратор. " +
                "Чат будет присоединён сразу, в обход логики с токенами.\n" +
                "В качестве необязательного параметра можно передать произвольное имя, " +
                "которое будет ассоциировано с чатом. " +
                "Имя можно задать и после присоединения чата к боту.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String params = msg.getParams().trim();
        if (Objects.equals(msg.getChatId(), msg.getUserId())) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), ResultStatus.JOIN_USER_AS_CHAT.getDescription());
            return new CommandResult(ResultStatus.JOIN_USER_AS_CHAT, params);
        }

        final Result result = bot.addChat(msg.getChatId(), Role.ADMIN, params);
        if (result.getStatus() == ResultStatus.OK) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Чат успешно зарегистрирован. Роль: " + Role.ADMIN
            );
            return new CommandResult(ResultStatus.OK, result.getUser().getDescription());
        } else {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось присоединить чат. " + result.getDescription()
            );
            return new CommandResult(result.getStatus(), params);
        }
    }
}
