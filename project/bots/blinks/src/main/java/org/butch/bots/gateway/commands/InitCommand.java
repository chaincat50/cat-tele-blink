package org.butch.bots.gateway.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.gateway.BlinksGatewayBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Objects;

public class InitCommand extends Command<BlinksGatewayBot> {
    public InitCommand(BlinksGatewayBot bot) {
        super(bot);
    }

    @Override
    public boolean addHint() {
        return false;
    }

    @Override
    public boolean addHelp() {
        return false;
    }

    @Override
    public String getCommand() {
        return "init";
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getHelp() {
        return null;
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        if (!Objects.equals(msg.getChatId(), msg.getUserId())) {
            return;
        }

        final boolean result = bot.init(msg.getChatId());
        if (result) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Бот успешно инициализирован. Теперь вы - его владелец."
            );
        } else {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось инициализировать бота."
            );
        }
    }
}
