package org.butch.bots.blinks.commands.superadmin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.LogEntryDto;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.text.SimpleDateFormat;
import java.util.List;

public class LogsCommand extends SuperAdminCommand {
    private static final int DEFAULT_COUNT = 10;
    private static final int LOGS_PER_MESSAGE = 50;

    public LogsCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "logs";
    }

    @Override
    public String getDescription() {
        return "[count] Напечатать лог";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " [count]\n" +
                "Напечатать последние действия, происходившие с ботом.\n" +
                "Временная зона - UTC (GMT+0).\n" +
                "<b>count</b> - количество записей (по умолчанию - " + DEFAULT_COUNT + ").\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String params = msg.getParams().trim();
        final int count;
        if (params.isEmpty()) {
            count = DEFAULT_COUNT;
        } else {
            try {
                count = Integer.parseInt(params);
            } catch (NumberFormatException ex) {
                bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                        "Неправильное число."
                );
                return null;
            }
        }

        final List<LogEntryDto> entries = bot.getLogs(count);

        final SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd hh:mm");
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < entries.size(); ++i) {
            builder.append(entries.get(i).getDescription(format)).append("\n");
            if ((i + 1) % LOGS_PER_MESSAGE == 0) {
                bot.sendMessage(msg.getChatId(), builder.toString());
                builder.setLength(0);
            }
        }
        if (entries.size() % LOGS_PER_MESSAGE != 0) {
            bot.sendMessage(msg.getChatId(), builder.toString());
        }
        return null;
    }
}
