package org.butch.bots.blinks.commands.owner;

import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.commands.BlinksCommand;
import org.butch.bots.blinks.model.Role;

public abstract class OwnerCommand extends BlinksCommand {
    public OwnerCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public Role getRole() {
        return Role.OWNER;
    }
}
