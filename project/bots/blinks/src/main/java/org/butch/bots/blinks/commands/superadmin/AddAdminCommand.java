package org.butch.bots.blinks.commands.superadmin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.Role;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class AddAdminCommand extends SuperAdminCommand {
    public AddAdminCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "add_admin";
    }

    @Override
    public String getDescription() {
        return "[name] Присоединить к боту администратора";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " [name]\n" +
                "Запрос на присоединение к боту администратора. " +
                "В ответе вернёт токен, который пользователь должен будет использовать " +
                "вместе с командой /join.\n" +
                "В качестве необязательного параметра можно передать произвольное имя, " +
                "которое будет ассоциировано с администратором. " +
                "Имя можно задать и после присоединения администратора к боту.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String params = msg.getParams().trim();
        final String token = bot.generateToken(false, Role.ADMIN, params);
        if (token == null || token.trim().isEmpty()) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось сгенерировать токен. " +
                    "Пожалуйста, сообщите об этом владельцу бота."
            );
            return new CommandResult(ResultStatus.CRITICAL, params);
        }
        bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                "Токен: `" + token + "`. \n" +
                "Передайте этот токен пользователю. " +
                "Он должен выполнить команду бота: \n" +
                "`/join " + token + "`"
        );
        return new CommandResult(ResultStatus.OK, params);
    }
}
