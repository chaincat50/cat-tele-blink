package org.butch.bots.blinks.master.commands.admin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.base.params2.ParamsParser;
import org.butch.bots.base.params2.Result;
import org.butch.bots.base.params2.ResultStatus;
import org.butch.bots.base.params2.conditions.Condition;
import org.butch.bots.base.params2.conditions.ConstCondition;
import org.butch.bots.base.params2.conditions.StringCondition;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.commands.admin.AdminCommand;
import org.butch.bots.blinks.master.BlinksMasterBot;
import org.butch.bots.common.Color;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class BroadcastCommand extends AdminCommand {
    private final Condition[] conditions;
    private final HashMap<String, Color> colors = new HashMap<>();
    private final HashSet<String> groups = new HashSet<>();

    public BroadcastCommand(BlinksMasterBot bot, List<String> groups) {
        super(bot);
        this.colors.put("all", Color.ALL);
        this.colors.put("red", Color.RED);
        this.colors.put("white", Color.WHITE);
        this.groups.addAll(groups);

        conditions = new Condition[]{
                ConstCondition.required(this.colors.keySet().toArray(new String[]{})),
                ConstCondition.optional(this.groups.toArray(new String[]{})),
                StringCondition.required()
        };
    }

    private BlinksMasterBot getBot() {
        return (BlinksMasterBot) bot;
    }

    @Override
    public String getCommand() {
        return "broadcast";
    }

    @Override
    public String getDescription() {
        return "<all|red|white> [group] Разослать сообщение";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;all|red|white&gt; [group]\n" +
                "[text]\n" +
                "Разослать сообщение по подчинённым ботам. " +
                "Поддерживаются только текстовые сообщения.\n" +
                "<b>all|red|white</b> - всем, только красным или только белым ботам.\n" +
                "<b>group</b> - только выбранной группе. " +
                "Если группа отсутствует - всем группам.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String chatId = msg.getChatId();
        final String userId = msg.getUserId();
        final Integer messageId = msg.getMessage().getMessageId();
        final String params = msg.getParams().trim();

        final Color color;
        final String group;
        final String text;

        try {
            final Result parsingResult = ParamsParser.parse(params, conditions);
            if (parsingResult.getStatus() == ResultStatus.NOT_ENOUGH_DATA)
                throw new IllegalArgumentException("Не достаточно данных.");
            if (parsingResult.getStatus() == ResultStatus.PARSING_ERROR)
                throw new IllegalArgumentException("Ошибка ввода: " + parsingResult.getError());
            final List<String> terms = parsingResult.getTerms();
            if (terms.size() != 3)
                throw new RuntimeException("Parsing error: " + params + " -> " + terms);
            color = colors.get(terms.get(0).toLowerCase());
            group = terms.get(1);
            text = terms.get(2).trim();

            if (color == null)
                throw new RuntimeException("color = " + color);
            if (!groups.contains(group))
                throw new RuntimeException("group = " + group);
            if (text.isEmpty())
                throw new RuntimeException("text = " + text);

            getBot().broadcast(chatId, userId, messageId, color, group, text);
            return null;
        } catch (IllegalArgumentException ex) {
            getBot().sendReplyOrError(chatId, messageId, ex.getMessage() + "\n" +
                    "Правильный формат:\n" +
                    COMMAND_PREFIX + getCommand() + " <all|red|white> " + groups + "\n" +
                    "{Отправляемый текст}");
            return null;
        }
    }
}
