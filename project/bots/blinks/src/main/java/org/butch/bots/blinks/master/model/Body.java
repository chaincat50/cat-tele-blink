package org.butch.bots.blinks.master.model;

public class Body {
    private String id;
    private long timestamp;
    private String timeString;
    private String text;
    private String salt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimeString() {
        return timeString;
    }

    public void setTimeString(String timeString) {
        this.timeString = timeString;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    public String toString() {
        return "<b>id</b>: " + getId() + "\n" +
                "<b>timestamp</b>: " + getTimestamp() + "\n" +
                "<b>time string</b>: " + getTimeString() + "\n" +
                "<b>text</b>:\n" +
                getText();
    }
}
