package org.butch.bots.blinks.commands;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.model.Role;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public abstract class BlinksCommand extends LoggableCommand {
    public BlinksCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public boolean addHint() {
        return false;
    }

    public boolean checkAllowed(CommandMessage msg) throws TelegramApiException {
        if (bot.checkAllowed(msg, getRole()))
            return true;

        bot.sendReply(msg.getChatId(),
                msg.getMessage().getMessageId(),
                ResultStatus.NOT_ALLOWED.getDescription());
        return false;
    }

    public abstract Role getRole();

    public String getHelp(Role userRole) {
        return getHelp();
    }
}
