package org.butch.bots.blinks.commands.superadmin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.Role;
import org.butch.bots.blinks.model.UserDto;
import org.butch.bots.blinks.model.UsersComparator;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

public class ListAllCommand extends SuperAdminCommand {
    private static final int USERS_PER_MESSAGE = 50;

    public ListAllCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "listall";
    }

    @Override
    public String getDescription() {
        return "Напечатать список всех";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Печатает список пользователей и администраторов, подключённых к боту.\n" +
                "Администраторы выделены жирным.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final List<UserDto> users = bot.listAll();
        users.sort(UsersComparator.INSTANCE);
        final StringBuilder builder = new StringBuilder();
        int usersCount = 0;
        int chatsCount = 0;
        for (int i = 0; i < users.size(); ++i) {
            final UserDto user = users.get(i);
            if (user.getIsChat()) {
                ++chatsCount;
            } else {
                ++usersCount;
            }
            if (user.getRole() == Role.OWNER) {
                builder.append("<u><b>");
            } else if (user.getRole() == Role.SUPER_ADMIN) {
                builder.append("<b><i>");
            } else if (user.getRole() == Role.ADMIN) {
                builder.append("<b>");
            }
            builder.append(user.getDescription());
            if (user.getRole() == Role.ADMIN) {
                builder.append("</b>");
            } else if (user.getRole() == Role.SUPER_ADMIN) {
                builder.append("</i></b>");
            } else if (user.getRole() == Role.OWNER) {
                builder.append("</b></u>");
            }
            builder.append("\n");
            if ((i + 1) % USERS_PER_MESSAGE == 0) {
                bot.sendHtml(msg.getChatId(), builder.toString());
                builder.setLength(0);
            }
        }
        if (users.size() % USERS_PER_MESSAGE != 0) {
            bot.sendHtml(msg.getChatId(), builder.toString());
        }
        bot.sendMessage(msg.getChatId(),
                String.format("Всего чатов: %d, пользователей: %d", chatsCount, usersCount));
        return null;
    }
}
