package org.butch.bots.blinks.commands.admin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class DenyFilesCommand extends AdminCommand {
    public DenyFilesCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "deny_files";
    }

    @Override
    public String getDescription() {
        return "Запретить пользователям отправку файлов";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Запрос на запрет пользователям отправлять произвольные файлы.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        final Result result = bot.changeAllowSendingFiles(userId, false);
        if (result == Result.OK) {
            bot.sendMessage(chatId, "" +
                    "Пересылка файлов запрещена."
            );
        } else {
            bot.sendMessage(chatId, "" +
                    "Не удалось запретить пересылку файлов. " + result.getDescription()
            );
        }
        return new CommandResult(ResultStatus.OK, "");
    }
}
