package org.butch.bots.gateway.commands.owner;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.gateway.BlinksGatewayBot;
import org.butch.bots.gateway.Constants;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class GreetCommand extends OwnerCommand {
    public GreetCommand(BlinksGatewayBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "greet";
    }

    @Override
    public String getDescription() {
        return "[текст] Напечатать/задать текст приветствия";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " [текст]\n" +
                "Запрос на печать/изменение текста приветствия пользователя.\n" +
                "<b>текст</b> - Новый текст приветствия. " +
                "Если параметр отсутствует, будет напечатан текущий текст.\n" +
                "Специальная последовательность <i>" + Constants.USER_PLACEHOLDER +
                "</i> будет заменена на имя пользователя. " +
                "Пример: <i>Привет, " + Constants.USER_PLACEHOLDER + "!</i>\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        final String text = msg.getParams();
        if (text.trim().isEmpty()) {
            bot.sendMessage(chatId, "Текущий текст приветствия: \n\n" +
                    bot.getGreeting());
            return;
        }

        if (bot.setGreeting(userId, text)) {
            bot.sendMessage(chatId, "Текст приветствия успешно изменён.");
        } else {
            bot.sendMessage(chatId, "Не удалось изменить текст приветствия.");
        }
    }
}
