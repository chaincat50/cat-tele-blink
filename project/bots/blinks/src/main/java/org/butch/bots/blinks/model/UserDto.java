package org.butch.bots.blinks.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

// Defines a registered user or chat.
// If isChat == true, userId contains chatId.
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto extends CommonDto {
    private String userId;
    private boolean isChat;
    private Role role;
    private Status status;
    private String name;
    private String reason;

    public static UserDto createForToken(boolean isChat, Role role, String name) {
        final UserDto ret = new UserDto();
        ret.setIsChat(isChat);
        ret.setRole(role);
        ret.setStatus(Status.NORMAL);
        if (name != null && !name.isEmpty()) {
            ret.setName(name);
        }
        return ret;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean getIsChat() {
        return isChat;
    }

    public void setIsChat(boolean isChat) {
        this.isChat = isChat;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public UserDto clone() {
        final UserDto ret = new UserDto();
        ret.copyFrom(this);
        return ret;
    }

    public void copyFrom(UserDto other) {
        this.setId(other.getId());
        this.setUserId(other.getUserId());
        this.setIsChat(other.getIsChat());
        this.setRole(other.getRole());
        this.setStatus(other.getStatus());
        this.setName(other.getName());
        this.setReason(other.getReason());
    }

    @JsonIgnore
    public String getDescription() {
        final StringBuilder builder = new StringBuilder();
        builder.append(getId()).append(". ");
        if (isChat) {
            builder.append("[");
        }
        if (name != null) {
            builder.append(name);
        } else {
            builder.append(role);
        }
        if (isChat) {
            builder.append("]");
        }
        if (status != Status.NORMAL) {
            builder.append(" // ").append(status);
            if (reason != null) {
                builder.append(" (").append(reason).append(")");
            }
        }
        return builder.toString();
    }

    @JsonIgnore
    public boolean needReason() {
        return status != Status.NORMAL && reason != null && !reason.isEmpty();
    }

    @Override
    public String toString() {
        return "" +
                " id = " + getId() +
                " userId = " + userId +
                " isChat = " + isChat +
                " role = " + role +
                " status = " + status +
                " name = " + name +
                " reason = " + reason
                ;
    }
}
