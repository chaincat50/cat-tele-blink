package org.butch.bots.blinks.commands.user;

import org.butch.bots.base.Utils;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.base.params.ParamsParser;
import org.butch.bots.base.params.ParserOptions;
import org.butch.bots.base.params.conditions.IntegerCondition;
import org.butch.bots.base.params.terms.IntegerTerm;
import org.butch.bots.base.params.terms.StringTerm;
import org.butch.bots.base.params.terms.Term;
import org.butch.bots.base.params.terms.TermType;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.MessageMeta;
import org.butch.bots.blinks.model.Role;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.text.ParseException;
import java.util.List;

public class SendCommand extends UserCommand {
    private static final ParserOptions OPTIONS = new ParserOptions().setFirstLine(true);

    private final String userHelp;

    public SendCommand(BlinksBot bot, String userHelp) {
        super(bot);
        this.userHelp = userHelp != null ? userHelp : "Послать сообщение.";
    }

    @Override
    public String getCommand() {
        return "send";
    }

    @Override
    public String getDescription() {
        return "[text] Послать сообщение";
    }

    @Override
    public String getHelp(Role userRole) {
        if (userRole.getNumber() <= Role.USER.getNumber()) {
            return getHelp();
        } else {
            return "" +
                    COMMAND_PREFIX + getCommand() + " [&lt;id&gt;]\n" +
                    "[text]\n" +
                    "Послать сообщение.\n" +
                    "<b>id</b> - номер пользователя/чата в боте. " +
                    "Если параметр отсутствует, сообщение будет отправлено администраторам.\n" +
                    "Если написать эту команду в ответ на сообщение, " +
                    "бот перешлёт это сообщение. " +
                    "Таким образом можно переслать, например, картинку.\n" +
                    "Кроме того, можно написать эту команду без текста и " +
                    "ответить на сообщение бота.\n";
        }
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " [text]\n" +
                userHelp + "\n" +
                "Если написать эту команду в ответ на сообщение, " +
                "бот перешлёт это сообщение. " +
                "Таким образом можно переслать, например, картинку.\n" +
                "Кроме того, можно написать эту команду без текста и " +
                "ответить на сообщение бота.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String chatId = msg.getChatId();
        final String userId = msg.getUserId();

        final Role role = bot.getRole(chatId, userId);

        int id = -1;
        String text;
        if (role.getNumber() <= Role.USER.getNumber()) {
            text = msg.getParams().trim();
        } else {
            try {
                final List<Term> terms = ParamsParser.parse(msg.getParams(), OPTIONS,
                        IntegerCondition.create()
                );
                if (terms.size() == 1) {
                    final Term first = terms.get(0);
                    if (first.getType() == TermType.INTEGER) {
                        id = ((IntegerTerm) first).getInt();
                        text = null;
                    } else {
                        text = msg.getParams().trim();
                    }
                } else if (terms.size() == 2) {
                    final Term first = terms.get(0);
                    final Term second = terms.get(1);
                    if (first.getType() == TermType.INTEGER && second.getType() == TermType.STRING) {
                        id = ((IntegerTerm) first).getInt();
                        text = ((StringTerm) second).getValue().trim();
                    } else {
                        throw new IllegalStateException("Unexpected terms: " + terms);
                    }
                } else {
                    throw new IllegalStateException("Unexpected number of terms: " + terms);
                }
            } catch (ParseException | IllegalStateException ex) {
                throw new TelegramApiException("Ошибка парсинга", ex);
            }
        }

        final Message replyTo = msg.getMessage().getReplyToMessage();
        if (role.getNumber() <= Role.USER.getNumber() && !bot.isSendingFilesAllowed()) {
            if (replyTo != null && replyTo.hasDocument()) {
                bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                        "Не удалось переслать сообщение. Отправлять файлы запрещено.");
                return null;
            }
        }
        if (replyTo != null) {
            final Result result;
            if (id >= 0) {
                result = bot.sendToAndAdmin(id, replyTo);
            } else {
                result = bot.sendToAdmin(replyTo);
            }
            if (result.getStatus() != ResultStatus.OK) {
                bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                        "Не удалось отправить сообщение. " + result.getDescription()
                );
            }
        } else if (text != null && !text.isEmpty()) {
            final Result result;
            if (id >= 0) {
                result = bot.sendToAndAdmin(chatId, userId, msg.getMessage().getMessageId(), id, text);
            } else {
                result = bot.sendToAdmin(chatId, userId, msg.getMessage().getMessageId(), text);
            }
            if (result.getStatus() != ResultStatus.OK) {
                bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                        "Не удалось отправить сообщение. " + result.getDescription()
                );
            }
        } else {
            if (id >= 0) {
                final User user = msg.getMessage().getFrom();
                final String greet = Utils.getUsername(user);
                final String tag = bot.getTag(userId, MessageMeta.Type.DIRECT_AND_ADMIN, id);
                bot.sendReply(chatId, msg.getMessage().getMessageId(), greet + "" +
                        ", напишите ответ на это сообщение для отправки прямого сообщения. \n\n" + tag);
            } else {
                final User user = msg.getMessage().getFrom();
                final String greet = Utils.getUsername(user);
                final String tag = bot.getTag(userId, MessageMeta.Type.ADMIN, 0);
                if (role.getNumber() <= Role.USER.getNumber()) {
                    bot.sendReply(chatId, msg.getMessage().getMessageId(), greet + "" +
                            ", напишите ответ на это сообщение. \n\n" + tag);
                } else {
                    bot.sendReply(chatId, msg.getMessage().getMessageId(), greet + "" +
                            ", напишите ответ на это сообщение для отправки администраторам. \n\n" + tag);
                }
            }
        }
        return null;
    }
}
