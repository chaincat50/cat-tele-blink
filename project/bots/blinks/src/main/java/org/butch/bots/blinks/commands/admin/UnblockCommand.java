package org.butch.bots.blinks.commands.admin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class UnblockCommand extends AdminCommand {
    public UnblockCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "unblock";
    }

    @Override
    public String getDescription() {
        return "<id> Разблокировать";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;id&gt;\n" +
                "Запрос на разблокировку пользователя/чата.\n" +
                "<b>id</b> - номер пользователя/чата в боте.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String params = msg.getParams().trim();
        final int id;
        try {
            id = Integer.parseInt(params);
        } catch (NumberFormatException ex) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Неправильный номер пользователя."
            );
            return new CommandResult(ResultStatus.PARAMS_ERROR, params);
        }
        final Result result = bot.unblockUser(msg.getUserId(), id);
        if (result.getStatus() == ResultStatus.OK) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Пользователь разблокирован."
            );
        } else {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось разблокировать пользователя. " + result.getDescription()
            );
        }
        return new CommandResult(result.getStatus(), params);
    }
}
