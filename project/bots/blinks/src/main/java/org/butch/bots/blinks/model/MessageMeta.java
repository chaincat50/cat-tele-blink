package org.butch.bots.blinks.model;

public class MessageMeta {
    private String fromId;
    private Type type;
    private int to;

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "" +
                " fromId = " + fromId +
                " type = " + type +
                " to = " + to
                ;
    }

    public enum Type {
        ADMIN, // Message to admin chats
        DIRECT,
        DIRECT_AND_ADMIN,
        BROADCAST
    }
}
