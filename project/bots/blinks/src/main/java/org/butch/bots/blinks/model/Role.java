package org.butch.bots.blinks.model;

// Defines permissions - available commands and messages access.
public enum Role {
    NONE(0),
    USER(25),
    ADMIN(50),
    SUPER_ADMIN(90),
    OWNER(100);

    private final int number;

    Role(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public boolean isAllowedCommand(Role commandRole) {
        return getNumber() >= commandRole.getNumber();
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isAllowedModifyOther(Role other) {
        switch (this) {
            case NONE:
            case USER:
                return false;

            case ADMIN:
                return other.getNumber() < ADMIN.getNumber();

            case SUPER_ADMIN:
                return other != OWNER;

            case OWNER:
                return true;
        }

        throw new IllegalArgumentException("Unsupported role: " + this);
    }

    public static Role fromNumber(int number) {
        switch (number) {
            case 0:
                return NONE;

            case 25:
                return USER;

            case 50:
                return ADMIN;

            case 90:
                return SUPER_ADMIN;

            case 100:
                return OWNER;
        }

        throw new IllegalArgumentException("Unsupported value: " + number);
    }

    @Override
    public String toString() {
        switch (this) {
            case NONE:
                return "Нет";

            case USER:
                return "Пользователь";

            case ADMIN:
                return "Администратор";

            case SUPER_ADMIN:
                return "Главный Администратор";

            case OWNER:
                return "Владелец";
        }

        return "Неизвестна";
    }
}
