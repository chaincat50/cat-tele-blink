package org.butch.bots.blinks.commands.admin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class RenameCommand extends AdminCommand {
    public RenameCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "rename";
    }

    @Override
    public String getDescription() {
        return "<id> [name] Поменять имя";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;id&gt; [name]\n" +
                "Запрос на изменение имени пользователя/чата.\n" +
                "<b>id</b> - номер пользователя/чата в боте.\n" +
                "<b>name</b> - новое имя. Если параметр отсутствует - имя будет удалено.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String params = msg.getParams().trim();
        final int id;
        final String name;
        try {
            final int index = params.indexOf(' ');
            if (index == -1) {
                id = Integer.parseInt(params);
                name = null;
            } else {
                id = Integer.parseInt(params.substring(0, index));
                name = params.substring(index + 1).trim();
            }
        } catch (NumberFormatException ex) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Неправильный номер пользователя."
            );
            return new CommandResult(ResultStatus.PARAMS_ERROR, params);
        }
        final Result result = bot.setUserName(msg.getUserId(), id, name);
        if (result.getStatus() == ResultStatus.OK) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Имя изменено."
            );
        } else {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось изменить имя. " + result.getDescription()
            );
        }
        return new CommandResult(result.getStatus(), params);
    }
}
