package org.butch.bots.blinks.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.butch.bots.blinks.model.CommonDto;
import org.butch.bots.blinks.model.LogEntryDto;
import org.butch.bots.blinks.model.UserDto;
import org.butch.encryption.CryptoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKey;
import java.util.ArrayList;
import java.util.List;

public class StorageManager {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final Storage storage;
    private final SecretKey secretKey;
    private final ObjectMapper mapper = new ObjectMapper();

    public StorageManager(Storage storage, SecretKey secretKey) {
        this.storage = storage;
        this.secretKey = secretKey;
    }

    public void init() {
        storage.init();
    }

    public BlinksConfigDto getConfig() {
        CommonDao dao = null;
        try {
            dao = storage.getConfig();
            if (dao == null) {
                final BlinksConfigDto ret = new BlinksConfigDto();
                final int id = setConfig(ret); // Save default config to database
                if (id > 0) {
                    ret.setId(id);
                }
                return ret;
            }
            return toDto(dao, BlinksConfigDto.class);
        } catch (Throwable ex) {
            LOGGER.warn("getConfig: " + dao, ex);
            return new BlinksConfigDto();
        }
    }

    public int setConfig(BlinksConfigDto dto) {
        try {
            final CommonDao dao = fromDto(dto);
            return storage.setConfig(dao);
        } catch (Throwable ex) {
            LOGGER.warn("setConfig: " + dto, ex);
            return -1;
        }
    }

    public int insertUser(UserDto dto) {
        try {
            final CommonDao dao = fromDto(dto);
            return storage.insertUser(dao);
        } catch (Throwable ex) {
            LOGGER.warn("insertUser: " + dto, ex);
            return -1;
        }
    }

    public boolean updateUser(UserDto dto) {
        try {
            final CommonDao dao = fromDto(dto);
            return storage.updateUser(dao);
        } catch (Throwable ex) {
            LOGGER.warn("updateUser: " + dto, ex);
            return false;
        }
    }

    public boolean deleteUser(UserDto dto) {
        try {
            final CommonDao dao = fromDto(dto);
            return storage.deleteUser(dao);
        } catch (Throwable ex) {
            LOGGER.warn("deleteUser: " + dto, ex);
            return false;
        }
    }

    public List<UserDto> queryUsers() {
        final List<CommonDao> list = storage.queryUsers();
        final List<UserDto> ret = new ArrayList<>();
        for (CommonDao dao : list) {
            try {
                ret.add(toDto(dao, UserDto.class));
            } catch (Throwable ex) {
                LOGGER.warn("queryUsers: " + dao, ex);
            }
        }
        return ret;
    }

    public boolean insertLogEntry(LogEntryDto dto) {
        try {
            final CommonDao dao = fromDto(dto);
            return storage.insertLogEntry(dao);
        } catch (Throwable ex) {
            LOGGER.warn("insertLogEntry: " + dto, ex);
            return false;
        }
    }

    public List<LogEntryDto> queryLogEntries(int count) {
        final List<CommonDao> list = storage.queryLogEntries(count);
        final List<LogEntryDto> ret = new ArrayList<>();
        for (CommonDao dao : list) {
            try {
                ret.add(toDto(dao, LogEntryDto.class));
            } catch (Throwable ex) {
                LOGGER.warn("queryLogEntries: " + dao, ex);
            }
        }
        return ret;
    }

    private <T extends CommonDto> CommonDao fromDto(T dto) throws Exception {
        final CommonDao dao = new CommonDao();
        dao.setId(dto.getId());
        dao.setBody(CryptoUtils.encrypt(secretKey, mapper.writeValueAsString(dto)));
        return dao;
    }

    private <T extends CommonDto> T toDto(CommonDao dao, Class<T> tClass) throws Exception {
        final T dto = mapper.readValue(CryptoUtils.decrypt(secretKey, dao.getBody()), tClass);
        dto.setId(dao.getId());
        return dto;
    }
}
