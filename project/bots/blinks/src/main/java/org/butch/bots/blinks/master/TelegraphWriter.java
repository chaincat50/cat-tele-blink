package org.butch.bots.blinks.master;

import org.butch.telegraph.BTWriter;
import org.butch.telegraph.content.BTAccount;
import org.butch.telegraph.content.BTContentNode;
import org.butch.telegraph.content.BTImage;
import org.butch.telegraph.content.BTText;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TelegraphWriter {
    private final BTWriter writer = new BTWriter();
    private BTAccount author = null;
    private BTImage whiteImage = null;
    private BTImage redImage = null;

    public void init(String name,
                     InputStream whiteStream, String whiteName,
                     InputStream redStream, String redName) throws IOException {
        author = writer.createAccount(name);
        whiteImage = writer.uploadFile(whiteStream, whiteName);
        redImage = writer.uploadFile(redStream, redName);
    }

    public String saveTextWhite(String id, String text) throws IOException {
        final List<BTContentNode> nodes = new ArrayList<>();
        nodes.add(whiteImage);
        nodes.add(new BTText(text));
        return writer.createPage(author, id, nodes);
    }

    public String saveTextRed(String id, String text) throws IOException {
        final List<BTContentNode> nodes = new ArrayList<>();
        nodes.add(redImage);
        nodes.add(new BTText(text));
        return writer.createPage(author, id, nodes);
    }
}
