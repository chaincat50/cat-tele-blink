package org.butch.bots.blinks.commands.admin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class BlockCommand extends AdminCommand {
    public BlockCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "block";
    }

    @Override
    public String getDescription() {
        return "<id> [reason] Заблокировать";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;id&gt; [reason]\n" +
                "Запрос на блокировку пользователя/чата. " +
                "Эффект эквивалентен удалению пользователя/бота, " +
                "однако позволяет его разблокировать без выдачи нового токена " +
                "и потери информации.\n" +
                "<b>id</b> - номер пользователя/чата в боте.\n" +
                "<b>reason</b> - необязательная причина.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String params = msg.getParams().trim();
        final int id;
        final String reason;
        try {
            final int index = params.indexOf(' ');
            if (index == -1) {
                id = Integer.parseInt(params);
                reason = null;
            } else {
                id = Integer.parseInt(params.substring(0, index));
                reason = params.substring(index + 1).trim();
            }
        } catch (NumberFormatException ex) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Неправильный номер пользователя."
            );
            return new CommandResult(ResultStatus.PARAMS_ERROR, params);
        }
        final Result result = bot.blockUser(msg.getUserId(), id, false, reason);
        if (result.getStatus() == ResultStatus.OK) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Пользователь заблокирован."
            );
        } else {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось заблокировать пользователя. " + result.getDescription()
            );
        }
        return new CommandResult(result.getStatus(), params);
    }
}
