package org.butch.bots.blinks;

public enum ResultStatus {
    OK,
    PARAMS_ERROR,
    DB_ERROR,
    NOT_ALLOWED,
    NOT_ALLOWED_USER,
    INIT_ERROR,
    TOKEN_NOT_FOUND,
    JOIN_USER_AS_CHAT,
    USER_NOT_FOUND,
    CANNOT_LOWER_ROLE,
    DENIED,
    CRITICAL;

    public String getDescription() {
        switch (this) {
            case OK:
                return "OK";

            case PARAMS_ERROR:
                return "Ошибка параметров.";

            case DB_ERROR:
                return "Ошибка базы данных.";

            case NOT_ALLOWED:
                return "Нет прав на выполнение команды.";

            case NOT_ALLOWED_USER:
                return "Нет прав для выбранного пользователя.";

            case INIT_ERROR:
                return "Бот уже инициализирован. Нельзя сделать это дважды.";

            case TOKEN_NOT_FOUND:
                return "Неизвестный токен. Возможно, закончилось его время действия.";

            case JOIN_USER_AS_CHAT:
                return "Попытка присоединить пользователя вместо чата.";

            case USER_NOT_FOUND:
                return "Неизвестный пользователь или чат.";

            case CANNOT_LOWER_ROLE:
                return "Данная команда не позволяет понизить роль.";

            case DENIED:
                return "Запрещено.";

            case CRITICAL:
                return "Критическая ошибка в коде бота.";
        }

        return "Неизвестная ошибка.";
    }
}
