package org.butch.bots.blinks;

import org.butch.bots.blinks.model.UserDto;

public class Result {
    public static Result OK = new Result(ResultStatus.OK);

    private final ResultStatus status;
    private final String comment;
    private final UserDto user;

    public Result(ResultStatus status) {
        this(status, null, null);
    }

    public Result(ResultStatus status, UserDto user) {
        this(status, null, user);
    }

    public Result(ResultStatus status, String comment) {
        this(status, comment, null);
    }

    public Result(ResultStatus status, String comment, UserDto user) {
        this.status = status;
        this.comment = comment;
        this.user = user;
    }

    public ResultStatus getStatus() {
        return status;
    }

    public String getComment() {
        return comment;
    }

    public UserDto getUser() {
        return user;
    }

    public String getDescription() {
        if (comment == null) {
            return status.getDescription();
        } else {
            return comment;
        }
    }
}
