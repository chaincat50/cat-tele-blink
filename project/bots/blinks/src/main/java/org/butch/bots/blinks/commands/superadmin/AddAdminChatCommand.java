package org.butch.bots.blinks.commands.superadmin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.Role;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class AddAdminChatCommand extends SuperAdminCommand {
    public AddAdminChatCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "add_admin_chat";
    }

    @Override
    public String getDescription() {
        return "[name] Присоединить к боту административный чат";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " [name]\n" +
                "Запрос на присоединение к боту чата с ролью Администратор. " +
                "В ответе вернёт токен, который пользователь должен будет использовать " +
                "вместе с командой /join.\n" +
                "В качестве необязательного параметра можно передать произвольное имя, " +
                "которое будет ассоциировано с чатом. " +
                "Имя можно задать и после присоединения чата к боту.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String params = msg.getParams().trim();
        final String token = bot.generateToken(true, Role.ADMIN, params);
        if (token == null || token.trim().isEmpty()) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось сгенерировать токен. " +
                    "Пожалуйста, сообщите об этом владельцу бота."
            );
            return new CommandResult(ResultStatus.CRITICAL, params);
        }
        bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                "Токен: `" + token + "`. \n" +
                "Передайте этот токен пользователю. " +
                "Он должен подключить бота к чату и выполнить в этом чате команду: \n" +
                "`/join " + token + "`"
        );
        return new CommandResult(ResultStatus.OK, params);
    }
}
