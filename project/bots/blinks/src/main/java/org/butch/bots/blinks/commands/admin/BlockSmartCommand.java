package org.butch.bots.blinks.commands.admin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.commands.CommandResult;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

// TODO: implement
public class BlockSmartCommand extends AdminCommand {
    public BlockSmartCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "block_smart";
    }

    @Override
    public String getDescription() {
        return "<id> Заблокировать тайно";
    }

    @Override
    public String getHelp() {
        return null;
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        return null;
    }
}
