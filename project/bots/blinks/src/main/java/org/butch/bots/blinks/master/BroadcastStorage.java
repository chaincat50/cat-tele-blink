package org.butch.bots.blinks.master;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.butch.bots.blinks.master.model.BroadcastData;
import org.butch.bots.common.Color;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class BroadcastStorage {
    private final Cache<String, BroadcastData> broadcasts;

    public BroadcastStorage(long timeout) {
        broadcasts = CacheBuilder.newBuilder()
                .expireAfterWrite(timeout, TimeUnit.MILLISECONDS)
                .build();
    }

    public String store(String senderName, Color color, String group, String text, String fromChatId) {
        final String id = generateId();
        broadcasts.put(id, new BroadcastData(senderName, color, group, text, fromChatId));
        return id;
    }

    public BroadcastData poll(String id) {
        return broadcasts.asMap().remove(id);
    }

    private String generateId() {
        return UUID.randomUUID().toString().toLowerCase().replace("-", "");
    }
}
