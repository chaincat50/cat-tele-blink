package org.butch.bots.gateway.commands;

import org.butch.bots.base.commands.BaseStartCommand;
import org.butch.bots.gateway.BlinksGatewayBot;
import org.butch.bots.gateway.Constants;
import org.telegram.telegrambots.meta.api.objects.User;

public class StartCommand extends BaseStartCommand<BlinksGatewayBot> {
    public StartCommand(BlinksGatewayBot bot) {
        super(bot);
    }

    @Override
    protected String getUserGreeting(User user, String userLink) {
        final String welcome = bot.getGreeting();
        if (welcome == null || welcome.isEmpty())
            return welcome;
        return welcome.replace(Constants.USER_PLACEHOLDER, userLink);
    }
}
