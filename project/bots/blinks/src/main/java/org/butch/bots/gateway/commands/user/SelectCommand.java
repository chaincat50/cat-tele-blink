package org.butch.bots.gateway.commands.user;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.gateway.BlinksGatewayBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Objects;

public class SelectCommand extends UserCommand {
    public SelectCommand(BlinksGatewayBot bot) {
        super(bot);
    }

    @Override
    public boolean addHint() {
        return true;
    }

    @Override
    public String getCommand() {
        return "select";
    }

    @Override
    public String getDescription() {
        return "Выбрать регион";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Запрос на выбор бота, в зависимости от региона и " +
                "предпочтений по типам акций. Разрешено использовать только в личке бота.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        if (!Objects.equals(msg.getChatId(), msg.getUserId())) {
            return;
        }
        bot.select(msg.getChatId());
    }
}
