package org.butch.bots.blinks.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class InitCommand extends Command<BlinksBot> {
    public InitCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public boolean addHint() {
        return false;
    }

    @Override
    public boolean addHelp() {
        return false;
    }

    @Override
    public String getCommand() {
        return "init";
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getHelp() {
        return null;
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final Result result = bot.init(msg.getChatId());
        if (result.getStatus() == ResultStatus.OK) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Бот успешно инициализирован. Теперь вы - его владелец."
            );
        } else {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось инициализировать бота. " + result.getDescription()
            );
        }
    }
}
