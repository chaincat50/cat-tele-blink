package org.butch.bots.blinks.commands.user;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.base.params.ParamsParser;
import org.butch.bots.base.params.ParserOptions;
import org.butch.bots.base.params.conditions.ConstCondition;
import org.butch.bots.base.params.conditions.IntegerCondition;
import org.butch.bots.base.params.conditions.OneOfCondition;
import org.butch.bots.base.params.terms.IntegerTerm;
import org.butch.bots.base.params.terms.StringTerm;
import org.butch.bots.base.params.terms.Term;
import org.butch.bots.base.params.terms.TermType;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.text.ParseException;
import java.util.List;

public class ForwardCommand extends UserCommand {
    private static final Logger LOGGER = LoggerFactory.getLogger(ForwardCommand.class);

    private static final String ALL = "all";
    private static final ParserOptions OPTIONS = new ParserOptions().setFirstLine(true);

    public ForwardCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "forward";
    }

    @Override
    public String getDescription() {
        return "Переслать сообщение";
    }

    @Override
    public String getHelp(Role userRole) {
        if (userRole.getNumber() <= Role.USER.getNumber()) {
            return getHelp();
        } else {
            return "" +
                    COMMAND_PREFIX + getCommand() + " [&lt;id&gt;|all]\n" +
                    "Переслать сообщение. " +
                    "Эту команду нужно выполнить в ответ на пересылаемое сообщение.\n" +
                    "<b>id</b> - номер пользователя/чата в боте.\n" +
                    "<b>all</b> - переслать всем.\n";
        }
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Переслать сообщение. " +
                "Эту команду нужно выполнить в ответ на пересылаемое сообщение.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String chatId = msg.getChatId();
        final String userId = msg.getUserId();

        final Role role = bot.getRole(chatId, userId);

        int id = -1;
        boolean all = false;
        String text;
        if (role.getNumber() <= Role.USER.getNumber()) {
            text = msg.getParams().trim();
        } else {
            try {
                final ConstCondition allCondition = ConstCondition.create(ALL);
                final List<Term> terms = ParamsParser.parse(msg.getParams(), OPTIONS,
                        OneOfCondition.create(
                                IntegerCondition.create(),
                                allCondition
                        )
                );
                if (terms.size() == 1) {
                    final Term first = terms.get(0);
                    if (first.getType() == TermType.INTEGER) {
                        id = ((IntegerTerm) first).getInt();
                        text = null;
                    } else if (allCondition.matches(first)) {
                        all = true;
                        text = null;
                    } else {
                        text = msg.getParams().trim();
                    }
                } else if (terms.size() == 2) {
                    final Term first = terms.get(0);
                    final Term second = terms.get(1);
                    if (first.getType() == TermType.INTEGER && second.getType() == TermType.STRING) {
                        id = ((IntegerTerm) first).getInt();
                        text = ((StringTerm) second).getValue().trim();
                    } else if (allCondition.matches(first) && second.getType() == TermType.STRING) {
                        all = true;
                        text = ((StringTerm) second).getValue().trim();
                    } else {
                        throw new IllegalStateException("Unexpected terms: " + terms);
                    }
                } else {
                    throw new IllegalStateException("Unexpected number of terms: " + terms);
                }
            } catch (ParseException | IllegalStateException ex) {
                throw new TelegramApiException("Ошибка парсинга", ex);
            }
        }

        if (text != null && !text.isEmpty() && LOGGER.isDebugEnabled()) {
            LOGGER.debug("Forward text: " + text);
        }

        final Message replyTo = msg.getMessage().getReplyToMessage();
        if (role.getNumber() <= Role.USER.getNumber() && !bot.isSendingFilesAllowed()) {
            if (replyTo != null && replyTo.hasDocument()) {
                bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                        "Не удалось переслать сообщение. Отправлять файлы запрещено.");
                return null;
            }
        }
        if (replyTo != null) {
            if (all) {
                final Result result = bot.forwardToAll(replyTo);
                if (result.getStatus() != ResultStatus.OK) {
                    bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                            "Не удалось переслать сообщение. " + result.getDescription()
                    );
                }
            } else if (id >= 0) {
                final Result result = bot.forwardTo(id, replyTo);
                if (result.getStatus() != ResultStatus.OK) {
                    bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                            "Не удалось переслать сообщение. " + result.getDescription()
                    );
                }
            } else {
                final Result result = bot.forwardToAdmin(replyTo);
                if (result.getStatus() != ResultStatus.OK) {
                    bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                            "Не удалось переслать сообщение. " + result.getDescription()
                    );
                }
            }
        } else {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Эту команду нужно выполнить в ответ на пересылаемое сообщение."
            );
        }
        return null;
    }
}
