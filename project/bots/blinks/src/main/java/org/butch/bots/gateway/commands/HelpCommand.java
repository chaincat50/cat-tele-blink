package org.butch.bots.gateway.commands;

import org.butch.bots.base.commands.BaseHelpCommand;
import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.gateway.BlinksGatewayBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class HelpCommand extends BaseHelpCommand<BlinksGatewayBot> {
    public static final int MAX_LENGTH = 2048;

    public HelpCommand(BlinksGatewayBot bot) {
        super(bot);
    }

    @Override
    public String getHelp() {
        return "" +
                "<u><b>Описание работы бота.</b></u>\n" +
                "\n" +
                "<u>Список поддерживаемых команд:</u>\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final boolean isOwner = bot.isOwner(msg.getUserId());
        final StringBuilder builder = new StringBuilder();
        builder.append(getHelp()).append("\n"); // Add this help first
        for (Command<?> command : bot.getCommands()) {
            if (command == this) // Don't print this help like general command
                continue;
            if (!command.addHelp())
                continue;
            if (command instanceof BlinksGatewayCommand) {
                final BlinksGatewayCommand blinksCommand = (BlinksGatewayCommand) command;
                if (!isOwner && blinksCommand.isOwnerCommand())
                    continue;
                builder.append(blinksCommand.getHelp(isOwner)).append("\n");
            } else {
                builder.append(command.getHelp()).append("\n");
            }
            if (builder.length() > MAX_LENGTH) {
                bot.sendHtml(msg.getChatId(), builder.toString());
                builder.setLength(0);
            }
        }
        if (builder.length() > 0) {
            bot.sendHtml(msg.getChatId(), builder.toString());
        }
    }
}
