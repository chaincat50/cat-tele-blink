package org.butch.bots.blinks.commands.superadmin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.commands.CommandResult;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

// TODO: implement
public class ConfigCommand extends SuperAdminCommand {
    public ConfigCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "config";
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getHelp() {
        return null;
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        return null;
    }
}
