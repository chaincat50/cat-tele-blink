package org.butch.bots.gateway.commands.owner;

import org.butch.bots.gateway.BlinksGatewayBot;
import org.butch.bots.gateway.commands.BlinksGatewayCommand;

public abstract class OwnerCommand extends BlinksGatewayCommand {
    public OwnerCommand(BlinksGatewayBot bot) {
        super(bot);
    }

    @Override
    public boolean isOwnerCommand() {
        return true;
    }
}
