package org.butch.bots.blinks.commands.user;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.base.params.ParserOptions;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class ResendCommand extends UserCommand {
    private static final ParserOptions OPTIONS = new ParserOptions().setFirstLine(true);

    public ResendCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "resend";
    }

    @Override
    public String getDescription() {
        return "Переслать сообщение";
    }

    @Override
    public String getHelp() {
        return null;
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String chatId = msg.getChatId();
        final String userId = msg.getUserId();

        final Message replyTo = msg.getMessage().getReplyToMessage();
        if (replyTo != null) {
            final Result result = bot.resend(chatId, userId, msg.getMessage().getMessageId(), replyTo);
            if (result.getStatus() != ResultStatus.OK) {
                bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                        "Не удалось переслать сообщение. " + result.getDescription()
                );
            }
        }
        return null;
    }
}
