package org.butch.bots.gateway.repository;

public interface Storage {
    void init();

    CommonDao getConfig();
    int setConfig(CommonDao config);
}
