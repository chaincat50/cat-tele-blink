package org.butch.bots.blinks.commands;

import org.butch.bots.blinks.ResultStatus;

public class CommandResult {
    private final ResultStatus status;
    private final String params;

    public CommandResult(ResultStatus status, String params) {
        this.status = status;
        this.params = params;
    }

    public ResultStatus getStatus() {
        return status;
    }

    public String getParams() {
        return params;
    }
}
