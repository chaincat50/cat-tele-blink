package org.butch.bots.gateway.repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.butch.bots.gateway.Constants;
import org.butch.bots.gateway.model.CommonDto;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlinksGatewayConfigDto extends CommonDto {
    private String ownerId = "";
    private String greeting = String.format("Привет, %s!", Constants.USER_PLACEHOLDER);

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }
}
