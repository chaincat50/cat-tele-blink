package org.butch.bots.blinks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TagUtils {
    public static String escape(String tag) {
        return "<tag=\"" + tag + "\">";
    }

    public static String parse(String text, String tagPattern) {
        if (text == null)
            return null;
        final Pattern pattern = Pattern.compile("<tag=\"(" + tagPattern + ")\">");
        final Matcher matcher = pattern.matcher(text);
        if (!matcher.find())
            return null;
        return matcher.group(1);
    }
}
