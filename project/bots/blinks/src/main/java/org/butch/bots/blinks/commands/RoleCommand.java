package org.butch.bots.blinks.commands;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.model.Role;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class RoleCommand extends BlinksCommand {
    public RoleCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public Role getRole() {
        return Role.NONE;
    }

    @Override
    public String getCommand() {
        return "role";
    }

    @Override
    public String getDescription() {
        return "Напечатать мою роль";
    }

    @Override
    public String getHelp(Role userRole) {
        if (userRole.getNumber() <= Role.USER.getNumber()) {
            return null; // Don't print this help for the user.
        } else {
            return getHelp();
        }
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Печатает эффективную роль пользователя (с учётом роли чата).\n" +
                "Для того, чтобы получить реальную роль, напишите эту команду боту в личку.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final Role role = bot.getRole(msg.getChatId(), msg.getUserId());
        bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), role.toString());
        return null;
    }
}
