package org.butch.bots.blinks.commands.superadmin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.UserDto;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class UpCommand extends SuperAdminCommand {
    public UpCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "up";
    }

    @Override
    public String getDescription() {
        return "<id> Повысить роль";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;id&gt;\n" +
                "Запрос на повышение роли пользователя/чата. \n" +
                "Роль чата может быть изменена только: " +
                "<b>User</b> -&gt; <b>Admin</b>.\n" +
                "Роль пользователя может быть изменена только: " +
                "<b>User</b> -&gt; <b>Admin</b> -&gt; <b>Super Admin</b>.\n" +
                "<b>id</b> - номер пользователя/чата в боте.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String params = msg.getParams().trim();
        final int id;
        try {
            id = Integer.parseInt(params);
        } catch (NumberFormatException ex) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Неправильный номер пользователя."
            );
            return new CommandResult(ResultStatus.PARAMS_ERROR, params);
        }
        final Result result = bot.up(msg.getUserId(), id);
        if (result.getStatus() == ResultStatus.OK) {
            final UserDto user = result.getUser();
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Роль успешно повышена до: " + user.getRole()
            );
            return new CommandResult(ResultStatus.OK, user.getDescription());
        } else {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось повысить роль пользователя. " + result.getDescription()
            );
            return new CommandResult(result.getStatus(), params);
        }
    }
}
