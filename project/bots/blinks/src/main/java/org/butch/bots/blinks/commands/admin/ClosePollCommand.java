package org.butch.bots.blinks.commands.admin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.commands.CommandResult;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

// TODO: Version 2
public class ClosePollCommand extends AdminCommand {
    public ClosePollCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "close_poll";
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getHelp() {
        return null;
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        return null;
    }
}
