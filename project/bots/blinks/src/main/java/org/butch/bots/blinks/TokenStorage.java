package org.butch.bots.blinks;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class TokenStorage {
    private Cache<String, Object> tokens;

    public TokenStorage(long timeout) {
        tokens = CacheBuilder.newBuilder()
                .expireAfterWrite(timeout, TimeUnit.MILLISECONDS)
                .build();
    }

    public void setTimeout(long timeout) {
        final Cache<String, Object> old = tokens;
        tokens = CacheBuilder.newBuilder()
                .expireAfterWrite(timeout, TimeUnit.MILLISECONDS)
                .build();
        tokens.putAll(old.asMap());
        old.asMap().clear();
        old.cleanUp();
    }

    public String register(Object result) {
        final String token = generateToken();
        tokens.put(token, result);
        return token;
    }

    public Object test(String token) {
        return tokens.asMap().remove(token);
    }

    public void returnRemoved(String token, Object result) {
        tokens.put(token, result);
    }

    public static String getTokenPattern() {
        return "[abcdef0-9]{32}";
    }

    private String generateToken() {
        return UUID.randomUUID().toString().toLowerCase().replace("-", "");
    }
}
