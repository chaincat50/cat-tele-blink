package org.butch.bots.blinks.commands.owner;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.Role;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class ChangeOwnerCommand extends OwnerCommand {
    public ChangeOwnerCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "owner";
    }

    @Override
    public String getDescription() {
        return "Передать владение ботом";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " \n" +
                "Запрос на передачу владения ботом другому пользователю. \n" +
                "В ответе вернёт токен, который пользователь должен будет использовать " +
                "вместе с командой /join.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String token = bot.generateToken(false, Role.OWNER, null);
        if (token == null || token.trim().isEmpty()) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось сгенерировать токен."
            );
            return new CommandResult(ResultStatus.CRITICAL, "");
        }
        bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                "Токен: `" + token + "`"
        );
        return new CommandResult(ResultStatus.OK, "");
    }
}
