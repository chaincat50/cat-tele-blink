package org.butch.bots.blinks.model;

import java.util.Comparator;

public class UsersComparator implements Comparator<UserDto> {
    public static Comparator<UserDto> INSTANCE = new UsersComparator().reversed();

    @Override
    public int compare(UserDto lhs, UserDto rhs) {
        int ret = Integer.compare(lhs.getRole().getNumber(), rhs.getRole().getNumber());
        if (ret != 0)
            return ret;
        ret = Boolean.compare(lhs.getIsChat(), rhs.getIsChat());
        if (ret != 0)
            return ret;
        return -Integer.compare(lhs.getId(), rhs.getId());
    }
}
