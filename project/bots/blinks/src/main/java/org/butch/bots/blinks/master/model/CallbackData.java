package org.butch.bots.blinks.master.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CallbackData {
    private Type type; // t
    private String data; // d
    private Action action; // a

    @JsonProperty("t")
    public Type getType() {
        return type;
    }

    @JsonProperty("t")
    public void setType(Type type) {
        this.type = type;
    }

    @JsonProperty("d")
    public String getData() {
        return data;
    }

    @JsonProperty("d")
    public void setData(String data) {
        this.data = data;
    }

    @JsonProperty("a")
    public Action getAction() {
        return action;
    }

    @JsonProperty("a")
    public void setAction(Action action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "" +
                " type = " + type +
                " action = " + action +
                " data = " + data;
    }

    public enum Type {
        @JsonProperty("b")
        BROADCAST
    }

    public enum Action {
        @JsonProperty("a")
        ACCEPT,
        @JsonProperty("c")
        CANCEL
    }
}
