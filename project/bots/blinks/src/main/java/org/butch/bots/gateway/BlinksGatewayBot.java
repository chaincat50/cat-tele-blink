package org.butch.bots.gateway;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.butch.bots.BotLimits;
import org.butch.bots.base.Bot;
import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.base.messages.OtherUpdate;
import org.butch.bots.gateway.commands.BlinksGatewayCommand;
import org.butch.bots.gateway.commands.HelpCommand;
import org.butch.bots.gateway.commands.InitCommand;
import org.butch.bots.gateway.commands.StartCommand;
import org.butch.bots.gateway.commands.owner.GreetCommand;
import org.butch.bots.gateway.commands.user.SelectCommand;
import org.butch.bots.gateway.model.CallbackData;
import org.butch.bots.gateway.model.Group;
import org.butch.bots.gateway.model.Region;
import org.butch.bots.gateway.repository.BlinksGatewayConfigDto;
import org.butch.bots.gateway.repository.Storage;
import org.butch.bots.gateway.repository.StorageManager;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageMedia;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.*;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.crypto.SecretKey;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BlinksGatewayBot extends Bot {
    private static final String ERROR_CALLBACK_DATA = "ERROR";
    private static final String BLANK_IMAGE = "blank.png";
    private static final int WHITE_INDEX = 1;
    private static final int RED_INDEX = 2;

    private final ObjectMapper mapper = new ObjectMapper();

    private final String token;
    private final List<Group> groups;

    private final StorageManager storage;
    private final BlinksGatewayConfigDto config;

    private final List<InputStream> mapStreams = new ArrayList<>();
    private final List<String> mapIds = new ArrayList<>();
    private final InputStream blankImage;
    private String blankImageId = null;

    public BlinksGatewayBot(String token, List<Group> groups,
                            Storage storage, SecretKey secretKey) {
        this.token = token;
        this.groups = new ArrayList<>();
        this.storage = new StorageManager(storage, secretKey);
        this.storage.init();
        this.config = this.storage.getConfig();
        for (Group group : groups) {
            if (group.getRegions().size() <= 0)
                continue;
            this.groups.add(group);
            if (group.getImage() != null) {
                mapStreams.add(getClass().getResourceAsStream("/" + group.getImage()));
            } else {
                mapStreams.add(null);
            }
            mapIds.add(null);
        }
        blankImage = getClass().getResourceAsStream("/" + BLANK_IMAGE);

        addCommand(new HelpCommand(this));
        addCommand(new StartCommand(this));
        addCommand(new InitCommand(this));
        // Owner
        addCommand(new GreetCommand(this));
        // User
        addCommand(new SelectCommand(this));
    }

    @Override
    public String getBotUsername() {
        return "BlinksGatewayBot";
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    protected void processCommandMessage(Command<?> command, CommandMessage msg) throws TelegramApiException {
        if (command instanceof BlinksGatewayCommand) {
            final BlinksGatewayCommand blinksCommand = (BlinksGatewayCommand) command;
            if (blinksCommand.checkAllowed(msg)) {
                blinksCommand.process(msg);
            }
        } else {
            super.processCommandMessage(command, msg);
        }
    }

    @Override
    protected void processOtherUpdate(OtherUpdate msg) {
        final Update update = msg.getUpdate();
        if (!update.hasCallbackQuery())
            return;
        final CallbackQuery callbackQuery = update.getCallbackQuery();
        final User user = callbackQuery.getFrom();
        final String userId = Long.toString(user.getId());
        try {
            final AnswerCallbackQuery answer = new AnswerCallbackQuery();
            answer.setCallbackQueryId(callbackQuery.getId());
            execute(answer);

            final String data = callbackQuery.getData();
            if (data == null || data.isEmpty()) {
                LOGGER.warn("Callback without data called: " + callbackQuery);
                return;
            }
            if (Objects.equals(data, ERROR_CALLBACK_DATA)) {
                sendMessage(userId, "" +
                        "В работе бота произошла ошибка. Сообщите, пожалуйста, разработчику."
                );
            }
            final CallbackData callbackData;
            try {
                callbackData = mapper.readValue(data, CallbackData.class);
            } catch (JsonProcessingException e) {
                sendMessage(userId, "" +
                        "Бот обновился и старые кнопки больше не работают. " +
                        "Выполните, пожалуйста, команду выбора заново."
                );
                return;
            }
            select(userId, callbackData, callbackQuery.getMessage());
        } catch (TelegramApiException ex) {
            onError(userId, ex);
        }
    }

    public String getGreeting() {
        return config.getGreeting();
    }

    public boolean isOwner(String userId) {
        return userId != null && userId.equals(config.getOwnerId());
    }

    public boolean init(String chatId) {
        if (config.getOwnerId() != null && !config.getOwnerId().isEmpty())
            return false;

        final String prev = config.getOwnerId();
        config.setOwnerId(chatId);
        final int id = storage.setConfig(config);
        if (id <= 0) {
            config.setOwnerId(prev);
            LOGGER.warn("Could not change ownerId.");
            return false;
        }
        config.setId(id);
        return true;
    }

    public boolean setGreeting(String userId, String text) {
        if (!Objects.equals(config.getOwnerId(), userId))
            return false;

        final String prev = config.getGreeting();
        config.setGreeting(text);
        final int id = storage.setConfig(config);
        if (id <= 0) {
            config.setGreeting(prev);
            LOGGER.warn("Could not change greeting.");
            return false;
        }
        config.setId(id);
        return true;
    }

    public void select(String chatId) throws TelegramApiException {
        final Integer imageId;
        final SendPhoto sendPhoto = new SendPhoto();
        sendPhoto.setChatId(chatId);
        if (blankImageId != null) {
            sendPhoto.setPhoto(new InputFile(blankImageId));
            final Message response = execute(sendPhoto);
            imageId = response.getMessageId();
        } else {
            sendPhoto.setPhoto(new InputFile(blankImage, BLANK_IMAGE));
            final Message response = execute(sendPhoto);
            blankImageId = response.getPhoto().get(0).getFileId();
            imageId = response.getMessageId();
        }

        final SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText("Выбор");
        sendMessage.setParseMode("HTML");
        sendMessage.setDisableWebPagePreview(true);
        final Message message = execute(sendMessage);

        final CallbackData callbackData = new CallbackData();
        callbackData.setImageId(imageId);
        callbackData.setSelectedImage(CallbackData.BLANK_IMAGE);
        callbackData.setStage(CallbackData.Stage.SELECT);

        select(chatId, callbackData, message);
    }

    private void select(String chatId, CallbackData data, Message originalMessage) throws TelegramApiException {
        switch (data.getStage()) {
            case SELECT: {
                data.setStage(CallbackData.Stage.GROUP);
                selectGroup(chatId, data, originalMessage);
                break;
            }

            case GROUP: {
                data.setStage(CallbackData.Stage.REGION);
                final Group group = groups.get(data.getGroupIndex());
                selectRegion(chatId, data, originalMessage, group);
                break;
            }

            case REGION: {
                if (data.getRegionIndex() == CallbackData.BACK_INDEX) {
                    data.setStage(CallbackData.Stage.SELECT);
                    select(chatId, data, originalMessage);
                } else {
                    data.setStage(CallbackData.Stage.COLOR);
                    final Group group = groups.get(data.getGroupIndex());
                    final Region region = group.getRegions().get(data.getRegionIndex());
                    selectColor(chatId, data, originalMessage, group, region);
                }
                break;
            }

            case COLOR: {
                if (data.getColorIndex() == CallbackData.BACK_INDEX) {
                    data.setStage(CallbackData.Stage.GROUP);
                    select(chatId, data, originalMessage);
                } else {
                    data.setStage(CallbackData.Stage.FINAL);
                    final Group group = groups.get(data.getGroupIndex());
                    final Region region = group.getRegions().get(data.getRegionIndex());
                    final String color = data.getColorIndex() == RED_INDEX
                            ? region.getRed()
                            : region.getWhite();
                    selectFinal(chatId, data, originalMessage, group, region, color);
                }
                break;
            }

            case FINAL: {
                data.setStage(CallbackData.Stage.REGION);
                select(chatId, data, originalMessage);
                break;
            }
        }
    }

    private void selectGroup(String chatId, CallbackData data, Message originalMessage) throws TelegramApiException {
        data.setSelectedImage(setBlankImage(chatId, data.getImageId(), data.getSelectedImage()));

        final InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup();
        keyboard.setKeyboard(new ArrayList<>());

        for (int i = 0; i < groups.size(); ++i) {
            final Group group = groups.get(i);
            final int row = i / 4;
            while (keyboard.getKeyboard().size() <= row) {
                keyboard.getKeyboard().add(new ArrayList<>());
            }
            data.setGroupIndex(i);
            keyboard.getKeyboard().get(row).add(
                    InlineKeyboardButton.builder()
                            .text(group.getName())
                            .callbackData(map(data))
                            .build()
            );
        }

        final EditMessageText request = EditMessageText.builder()
                .chatId(chatId)
                .messageId(originalMessage.getMessageId())
                .text("Выберите ваш регион:")
                .parseMode("HTML")
                .disableWebPagePreview(true)
                .replyMarkup(keyboard)
                .build();
        execute(request);
    }

    private void selectRegion(String chatId, CallbackData data, Message originalMessage,
                              Group group) throws TelegramApiException {
        if (group.getImage() == null) {
            data.setSelectedImage(setBlankImage(chatId, data.getImageId(), data.getSelectedImage()));
        } else {
            data.setSelectedImage(setGroupImage(chatId, data.getImageId(), data.getGroupIndex(),
                    group.getImage(), data.getSelectedImage()));
        }

        final InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup();
        keyboard.setKeyboard(new ArrayList<>());

        for (int i = 0; i < group.getRegions().size(); ++i) {
            final Region region = group.getRegions().get(i);
            final int row = i / 4;
            while (keyboard.getKeyboard().size() <= row) {
                keyboard.getKeyboard().add(new ArrayList<>());
            }
            data.setRegionIndex(i);
            keyboard.getKeyboard().get(row).add(
                    InlineKeyboardButton.builder()
                            .text(region.getName())
                            .callbackData(map(data))
                            .build()
            );
        }

        keyboard.getKeyboard().add(new ArrayList<>());
        data.setRegionIndex(CallbackData.BACK_INDEX);
        keyboard.getKeyboard().get(keyboard.getKeyboard().size() - 1).add(
                InlineKeyboardButton.builder()
                        .text("Назад")
                        .callbackData(map(data))
                        .build()
        );

        final EditMessageText request = EditMessageText.builder()
                .chatId(chatId)
                .messageId(originalMessage.getMessageId())
                .text(String.format("<b>%s</b> / Выберите ваш регион:", group.getName()))
                .parseMode("HTML")
                .disableWebPagePreview(true)
                .replyMarkup(keyboard)
                .build();
        execute(request);
    }

    private void selectColor(String chatId, CallbackData data, Message originalMessage,
                             Group group, Region region) throws TelegramApiException {
        if (group.getImage() == null) {
            data.setSelectedImage(setBlankImage(chatId, data.getImageId(), data.getSelectedImage()));
        } else {
            data.setSelectedImage(setGroupImage(chatId, data.getImageId(), data.getGroupIndex(),
                    group.getImage(), data.getSelectedImage()));
        }

        final InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup();
        keyboard.setKeyboard(new ArrayList<>());

        if (region.getWhite() != null || region.getRed() != null) {
            keyboard.getKeyboard().add(new ArrayList<>());
        }
        if (region.getWhite() != null) {
            data.setColorIndex(WHITE_INDEX);
            keyboard.getKeyboard().get(0).add(
                    InlineKeyboardButton.builder()
                            .text("Белый")
                            .callbackData(map(data))
                            .build()
            );
        }
        if (region.getRed() != null) {
            data.setColorIndex(RED_INDEX);
            keyboard.getKeyboard().get(0).add(
                    InlineKeyboardButton.builder()
                            .text("Красный")
                            .callbackData(map(data))
                            .build()
            );
        }

        keyboard.getKeyboard().add(new ArrayList<>());
        data.setColorIndex(CallbackData.BACK_INDEX);
        keyboard.getKeyboard().get(keyboard.getKeyboard().size() - 1).add(
                InlineKeyboardButton.builder()
                        .text("Назад")
                        .callbackData(map(data))
                        .build()
        );

        final EditMessageText request = EditMessageText.builder()
                .chatId(chatId)
                .messageId(originalMessage.getMessageId())
                .text(String.format("<b>%s</b> / <b>%s</b> / Выберите цвет:",
                        group.getName(), region.getName()))
                .parseMode("HTML")
                .disableWebPagePreview(true)
                .replyMarkup(keyboard)
                .build();
        execute(request);
    }

    private void selectFinal(String chatId, CallbackData data, Message originalMessage,
                             Group group, Region region, String name) throws TelegramApiException {
        if (group.getImage() == null) {
            data.setSelectedImage(setBlankImage(chatId, data.getImageId(), data.getSelectedImage()));
        } else {
            data.setSelectedImage(setGroupImage(chatId, data.getImageId(), data.getGroupIndex(),
                    group.getImage(), data.getSelectedImage()));
        }

        final InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup();
        keyboard.setKeyboard(new ArrayList<>());
        keyboard.getKeyboard().add(new ArrayList<>());
        keyboard.getKeyboard().get(keyboard.getKeyboard().size() - 1).add(
                InlineKeyboardButton.builder()
                        .text("Назад")
                        .callbackData(map(data))
                        .build()
        );

        final EditMessageText request = EditMessageText.builder()
                .chatId(chatId)
                .messageId(originalMessage.getMessageId())
                .text(String.format("Вы выбрали <b>%s</b> / <b>%s</b> / <b>%s</b>.\n\n" +
                                "Добавьте в ваш чат бота: %s и выполните его команду /join.\n" +
                                "Вам не надо делать его администратором канала. " +
                                "Бот не будет видеть ваши сообщения (это ограничение телеграмма). " +
                                "Для получения списка доступных команд выполните команду бота /help.\n\n" +
                                "Не забудьте удалить текущего бота. Он вам больше не понадобится.",
                        group.getName(), region.getName(),
                        data.getColorIndex() == RED_INDEX ? "Красный" : "Белый",
                        name))
                .parseMode("HTML")
                .disableWebPagePreview(true)
                .replyMarkup(keyboard)
                .build();
        execute(request);
    }

    private Integer setBlankImage(String chatId, Integer messageId,
                                  Integer current) throws TelegramApiException {
        if (current != null && Objects.equals(current, CallbackData.BLANK_IMAGE))
            return CallbackData.BLANK_IMAGE;
        final String newMediaId = changeImage(chatId, messageId,
                blankImageId, blankImage, BLANK_IMAGE);
        if (newMediaId != null) {
            blankImageId = newMediaId;
        }
        return CallbackData.BLANK_IMAGE;
    }

    private Integer setGroupImage(String chatId, Integer messageId, int index,
                                  String name, Integer current) throws TelegramApiException {
        if (current != null && Objects.equals(current, index))
            return index;
        final String newMediaId = changeImage(chatId, messageId,
                mapIds.get(index), mapStreams.get(index), name);
        if (newMediaId != null) {
            mapIds.set(index, newMediaId);
        }
        return index;
    }

    private String changeImage(String chatId, Integer messageId,
                               String mediaId, InputStream stream, String name) throws TelegramApiException {

        final InputMediaPhoto photo = new InputMediaPhoto();
        if (mediaId != null) {
            photo.setMedia(mediaId);
        } else {
            photo.setMedia(stream, name);
        }
        final EditMessageMedia request = EditMessageMedia.builder()
                .chatId(chatId)
                .messageId(messageId)
                .media(photo)
                .build();
        final Serializable serializable = execute(request);
        if (mediaId == null && serializable instanceof Message) {
            final Message message = (Message) serializable;
            return message.getPhoto().get(0).getFileId();
        }
        return null;
    }

    private String map(CallbackData data) {
        try {
            final String ret = mapper.writeValueAsString(data);
            if (ret.length() > BotLimits.MAX_CALLBACK_DATA_LENGTH) {
                LOGGER.warn("MAX_LENGTH: " + ret);
                return ERROR_CALLBACK_DATA;
            }
            return ret;
        } catch (JsonProcessingException ex) {
            LOGGER.warn("JSON: " + ex);
            return ERROR_CALLBACK_DATA;
        }
    }
}
