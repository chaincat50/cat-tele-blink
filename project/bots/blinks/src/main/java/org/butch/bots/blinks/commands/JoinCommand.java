package org.butch.bots.blinks.commands;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.model.Role;
import org.butch.bots.blinks.model.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class JoinCommand extends LoggableCommand {
    private static final Logger LOGGER = LoggerFactory.getLogger(JoinCommand.class);

    private final boolean autoJoinAllowed;

    public JoinCommand(BlinksBot bot, boolean autoJoinAllowed) {
        super(bot);
        this.autoJoinAllowed = autoJoinAllowed;
    }

    public boolean addHint() {
        return false;
    }

    public boolean addHelp() {
        return false;
    }

    @Override
    public String getCommand() {
        return "join";
    }

    @Override
    public String getDescription() {
        return "<token> Присоединиться к боту";
    }

    @Override
    public String getHelp() {
        if (autoJoinAllowed) {
            return "" +
                    COMMAND_PREFIX + getCommand() + " [token]\n" +
                    "Запрос на смену роли в боте для чата или пользователя. " +
                    "Роль и то, кому она меняется, определяется токеном. " +
                    "При этом, роль нельзя понизить.\n" +
                    "Можно добавиться без токена в роли пользователя.";
        } else {
            return "" +
                    COMMAND_PREFIX + getCommand() + " &lt;token&gt;\n" +
                    "Запрос на смену роли в боте для чата или пользователя. " +
                    "Роль и то, кому она меняется, определяется токеном. " +
                    "При этом, роль нельзя понизить.\n";
        }
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String token = msg.getParams().trim();
        final Result result = bot.join(msg.getChatId(), msg.getUserId(),
                msg.getMessage().getMessageId(), token);
        if (result.getStatus() == ResultStatus.OK) {
            final UserDto user = result.getUser();
            if (user == null) {
                LOGGER.error("user == null. Command: /join " + token);
                bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                        "Регистрация завершилась неожиданным способом. " +
                        "Пожалуйста, сообщите об этом владельцу бота."
                );
                return new CommandResult(ResultStatus.CRITICAL, "");
            }
            if (user.getIsChat()) {
                if (user.getRole() == Role.USER) {
                    bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                            "Чат успешно зарегистрирован."
                    );
                } else {
                    bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                            "Чат успешно зарегистрирован. Роль: " + user.getRole()
                    );
                }
            } else {
                if (user.getRole() == Role.USER) {
                    bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                            "Пользователь успешно зарегистрирован."
                    );
                } else if (user.getRole() == Role.OWNER) {
                    bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                            "Владелец бота успешно изменён."
                    );
                } else {
                    bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                            "Пользователь успешно зарегистрирован. Роль: " + user.getRole()
                    );
                }
            }
            return new CommandResult(ResultStatus.OK, user.getDescription());
        } else {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось присоединиться. " + result.getDescription()
            );
            return new CommandResult(result.getStatus(), result.getComment());
        }
    }
}
