package org.butch.bots.blinks.commands.admin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.UserDto;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class WhoisCommand extends AdminCommand {
    public WhoisCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "whois";
    }

    @Override
    public String getDescription() {
        return "<id> Напечатать информацию";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;id&gt;\n" +
                "Печатает детальную информацию о пользователе/чате.\n" +
                "<b>id</b> - номер пользователя/чата в боте.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final int id;
        try {
            final String params = msg.getParams().trim();
            id = Integer.parseInt(params);
        } catch (NumberFormatException ex) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Неправильный номер пользователя."
            );
            return null;
        }
        final Result result = bot.getUserInfo(msg.getUserId(), id);
        if (result.getStatus() == ResultStatus.OK) {
            final UserDto user = result.getUser();
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    (user.getIsChat() ? "Чат " : "Пользователь ") +
                    "№: " + user.getId() + "\n" +
                    "Имя: " + user.getName() + "\n" +
                    "Роль: " + user.getRole() + "\n" +
                    "Статус: " + user.getStatus() +
                    (user.needReason() ? " (" + user.getReason() + ")" : "") + "\n"
            );
        } else {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось получить информацию о пользователе. " + result.getDescription()
            );
        }
        return null;
    }
}
