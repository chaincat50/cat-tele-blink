package org.butch.bots.blinks.repository;

import org.butch.bots.blinks.model.Role;
import org.butch.bots.blinks.model.UserDto;

import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;

public class UserStorage {
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final HashMap<Integer, UserDto> usersById = new HashMap<>();
    private final HashMap<String, UserDto> usersByUserId = new HashMap<>();

    public void add(UserDto user) {
        lock.writeLock().lock();
        try {
            usersById.put(user.getId(), user);
            usersByUserId.put(user.getUserId(), user);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public <T> T visit(String userId, Function<UserDto, T> visitor) {
        lock.readLock().lock();
        try {
            return visitor.apply(usersByUserId.get(userId));
        } finally {
            lock.readLock().unlock();
        }
    }

    public <T> T visit(int id, Function<UserDto, T> visitor) {
        lock.readLock().lock();
        try {
            return visitor.apply(usersById.get(id));
        } finally {
            lock.readLock().unlock();
        }
    }

    public UserDto remove(String userId) {
        lock.writeLock().lock();
        try {
            final UserDto ret = usersByUserId.remove(userId);
            if (ret != null) {
                usersById.remove(ret.getId());
            }
            return ret;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public UserDto remove(int id) {
        lock.writeLock().lock();
        try {
            final UserDto ret = usersById.remove(id);
            if (ret != null) {
                usersByUserId.remove(ret.getUserId());
            }
            return ret;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public int size() {
        lock.readLock().lock();
        try {
            return usersByUserId.size();
        } finally {
            lock.readLock().unlock();
        }
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public List<UserDto> list(Role... filter) {
        final HashSet<Role> uniqueRoles = new HashSet<>(Arrays.asList(filter));
        final List<UserDto> ret = new ArrayList<>();
        lock.readLock().lock();
        try {
            for (UserDto userDto : usersById.values()) {
                if (uniqueRoles.isEmpty() || uniqueRoles.contains(userDto.getRole())) {
                    ret.add(userDto.clone());
                }
            }
        } finally {
            lock.readLock().unlock();
        }
        return ret;
    }
}
