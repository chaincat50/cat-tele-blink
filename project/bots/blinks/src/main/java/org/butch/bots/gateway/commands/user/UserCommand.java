package org.butch.bots.gateway.commands.user;

import org.butch.bots.gateway.BlinksGatewayBot;
import org.butch.bots.gateway.commands.BlinksGatewayCommand;

public abstract class UserCommand extends BlinksGatewayCommand {
    public UserCommand(BlinksGatewayBot bot) {
        super(bot);
    }

    @Override
    public boolean isOwnerCommand() {
        return false;
    }
}
