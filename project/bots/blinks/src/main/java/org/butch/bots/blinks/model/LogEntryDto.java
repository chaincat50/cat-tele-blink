package org.butch.bots.blinks.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.butch.bots.base.commands.Command;
import org.butch.bots.blinks.ResultStatus;

import java.text.DateFormat;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LogEntryDto extends CommonDto {
    public static final int NO_USER = -1;
    public static final int UNKNOWN_USER = -2;

    private long timestamp;
    private int chatId;
    private String chatName;
    private Role chatRole;
    private int userId;
    private String userName;
    private Role userRole;
    private String command;
    private ResultStatus status;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public Role getChatRole() {
        return chatRole;
    }

    public void setChatRole(Role chatRole) {
        this.chatRole = chatRole;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Role getUserRole() {
        return userRole;
    }

    public void setUserRole(Role userRole) {
        this.userRole = userRole;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public ResultStatus getStatus() {
        return status;
    }

    public void setStatus(ResultStatus status) {
        this.status = status;
    }

    @JsonIgnore
    public String getDescription(DateFormat format) {
        final StringBuilder builder = new StringBuilder();
        builder.append("[").append(format.format(new Date(timestamp))).append("] ");
        if (userId == UNKNOWN_USER) {
            builder.append("неизвестный - ");
        } else if (userId != NO_USER) {
            builder.append(userName).append(" - ");
        }
        builder.append("\"").append(Command.COMMAND_PREFIX).append(command).append("\" ")
                .append(status);
        return builder.toString();
    }
}
