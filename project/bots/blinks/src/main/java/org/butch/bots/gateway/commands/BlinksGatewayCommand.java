package org.butch.bots.gateway.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.gateway.BlinksGatewayBot;

public abstract class BlinksGatewayCommand extends Command<BlinksGatewayBot> {
    public BlinksGatewayCommand(BlinksGatewayBot bot) {
        super(bot);
    }

    @Override
    public boolean addHint() {
        return false;
    }

    public boolean checkAllowed(CommandMessage msg) {
        return !isOwnerCommand() || bot.isOwner(msg.getUserId());
    }

    public abstract boolean isOwnerCommand();

    public String getHelp(boolean owner) {
        return getHelp();
    }
}
