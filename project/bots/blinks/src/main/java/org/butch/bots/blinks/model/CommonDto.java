package org.butch.bots.blinks.model;

public class CommonDto {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
