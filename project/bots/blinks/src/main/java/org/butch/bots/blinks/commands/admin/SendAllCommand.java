package org.butch.bots.blinks.commands.admin;

import org.butch.bots.base.Utils;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.MessageMeta;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class SendAllCommand extends AdminCommand {
    public SendAllCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "send_all";
    }

    @Override
    public String getDescription() {
        return "[text] Послать сообщение всем";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " [text]\n" +
                "Послать сообщение всем.\n" +
                "Если написать эту команду в ответ на сообщение, " +
                "бот перешлёт это сообщение. " +
                "Таким образом можно переслать, например, картинку.\n" +
                "Кроме того, можно написать эту команду без текста и " +
                "ответить на сообщение бота.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String chatId = msg.getChatId();
        final String userId = msg.getUserId();

        final String text = msg.getParams().trim();

        final Message replyTo = msg.getMessage().getReplyToMessage();
        if (replyTo != null) {
            final Result result = bot.sendToAll(replyTo);
            if (result.getStatus() != ResultStatus.OK) {
                bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                        "Не удалось отправить сообщение. " + result.getDescription()
                );
            }
        } else if (!text.isEmpty()) {
            final Result result = bot.sendToAll(chatId, userId, msg.getMessage().getMessageId(), text);
            if (result.getStatus() != ResultStatus.OK) {
                bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                        "Не удалось отправить сообщение. " + result.getDescription()
                );
            }
        } else {
            final User user = msg.getMessage().getFrom();
            final String greet = Utils.getUsername(user);
            final String tag = bot.getTag(userId, MessageMeta.Type.BROADCAST, 0);
            bot.sendReply(chatId, msg.getMessage().getMessageId(), greet + "" +
                    ", напишите ответ на это сообщение для рассылки. \n\n" + tag);
        }
        return null;
    }
}
