package org.butch.bots.blinks;

import org.butch.bots.base.Bot;
import org.butch.bots.base.Emoji;
import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.commands.*;
import org.butch.bots.blinks.commands.admin.*;
import org.butch.bots.blinks.commands.owner.ChangeOwnerCommand;
import org.butch.bots.blinks.commands.superadmin.*;
import org.butch.bots.blinks.commands.user.ResendCommand;
import org.butch.bots.blinks.commands.user.SendCommand;
import org.butch.bots.blinks.master.BlinksMasterBot;
import org.butch.bots.blinks.model.*;
import org.butch.bots.blinks.repository.BlinksConfigDto;
import org.butch.bots.blinks.repository.Storage;
import org.butch.bots.blinks.repository.StorageManager;
import org.butch.bots.blinks.repository.UserStorage;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatMemberCount;
import org.telegram.telegrambots.meta.api.methods.send.*;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.*;
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMember;
import org.telegram.telegrambots.meta.api.objects.stickers.Sticker;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import javax.crypto.SecretKey;
import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

public class BlinksBot extends Bot {
    private final String token;
    private final boolean autoJoinAllowed;

    protected final StorageManager storage;
    protected final UserStorage userStorage;
    private final TokenStorage tokenStorage;
    private final BlinksConfigDto config;

    private final BlinksMasterBot masterBot;

    public BlinksBot(String token, Storage storage, SecretKey secretKey,
                     boolean autoJoinAllowed, String userSendHelp,
                     BlinksMasterBot masterBot) {
        this.token = token;
        this.autoJoinAllowed = autoJoinAllowed;
        this.storage = new StorageManager(storage, secretKey);
        this.storage.init();
        this.config = this.storage.getConfig();
        this.userStorage = new UserStorage();
        this.tokenStorage = new TokenStorage(this.config.getTokenTimeout());
        this.masterBot = masterBot;

        addCommand(new HelpCommand(this));
        addCommand(new StartCommand(this));
        addCommand(new InitCommand(this));
        addCommand(new JoinCommand(this, autoJoinAllowed));
        addCommand(new RoleCommand(this));
        // Owner
        addCommand(new ChangeOwnerCommand(this));
        // Super Admin
//        addCommand(new ConfigCommand(this));
        addCommand(new GreetCommand(this));
        addCommand(new LogsCommand(this));
        addCommand(new AddSuperAdminCommand(this));
        addCommand(new AddAdminCommand(this));
        addCommand(new AddAdminChatCommand(this));
        addCommand(new SetAdminChatCommand(this));
        addCommand(new UpCommand(this));
        addCommand(new DownCommand(this));
        addCommand(new ListAdminCommand(this));
        addCommand(new ListAllCommand(this));
        addCommand(new WhoisCommand(this));
        addCommand(new WhisperCommand(this));
//        addCommand(new StatsCommand(this));
        // Admin
        addCommand(new AllowFilesCommand(this));
        addCommand(new DenyFilesCommand(this));
        addCommand(new AddUserCommand(this));
        addCommand(new AddUserChatCommand(this));
        addCommand(new RevokeCommand(this));
        addCommand(new RemoveCommand(this));
        addCommand(new ListCommand(this));
        addCommand(new RenameCommand(this));
        addCommand(new BlockCommand(this));
//        addCommand(new BlockSmartCommand(this));
        addCommand(new UnblockCommand(this));
//        addCommand(new PollCommand(this));
//        addCommand(new ClosePollCommand(this));
//        addCommand(new ListPollsCommand(this));
        addCommand(new SendAllCommand(this));
        // User
        addCommand(new SendCommand(this, userSendHelp));
        // TODO: Remove /forward in the future.
//        if (allowForwarding) {
//            addCommand(new ForwardCommand(this));
//        }
        if (masterBot != null) {
            addCommand(new ResendCommand(this));
        }
    }

    @Override
    public String getBotUsername() {
        return "BlinksBot";
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public boolean replyErrors() {
        return true;
    }

    @Override
    public void onRegister() {
        super.onRegister();
        final List<UserDto> users = storage.queryUsers();
        for (UserDto user : users) {
            userStorage.add(user);
        }
        for (UserDto user : users) {
            if (!testAvailability(user)) {
                LOGGER.warn("Could not find chat: " + user);
            }
        }
        if (masterBot != null) {
            masterBot.registerSlaveBot(this);
        }
    }

    public boolean testAvailability(UserDto user) {
        try {
            sendApiMethod(GetChatMemberCount.builder().chatId(user.getUserId()).build());
        } catch (TelegramApiException ignored) {
            try {
                final Message msg = sendApiMethod(SendMessage.builder().chatId(user.getUserId()).text("Started").build());
                sendApiMethod(DeleteMessage.builder().chatId(user.getUserId()).messageId(msg.getMessageId()).build());
            } catch (TelegramApiRequestException ex) {
                LOGGER.warn(ex.toString());
                final ResponseParameters parameters = ex.getParameters();
                if (parameters != null && parameters.getMigrateToChatId() != null) {
                    LOGGER.info("Chat has changed id. Migrating: " + user);
                    tryMigrate(user.getUserId(), Long.toString(parameters.getMigrateToChatId()));
                } else {
                    return false;
                }
            } catch (TelegramApiException ignored2) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onUpdateReceived(Update update) {
        super.onUpdateReceived(update);

        if (update.hasMyChatMember()) {
            processMyChatMember(update.getMyChatMember());
        } else if (update.hasMessage()) {
            processMessage(update.getMessage());
        }
    }

    @Override
    protected void processCommandMessage(Command<?> command, CommandMessage msg) throws TelegramApiException {
        if (command instanceof BlinksCommand) {
            final BlinksCommand blinksCommand = (BlinksCommand) command;
            if (blinksCommand.checkAllowed(msg)) {
                blinksCommand.process(msg);
            }
        } else {
            super.processCommandMessage(command, msg);
        }
    }

    public boolean checkAllowed(CommandMessage command, Role required) {
        final Role userRole = getRole(command.getChatId(), command.getUserId());
        return userRole.isAllowedCommand(required);
    }

    @NotNull
    public Role getRole(String chatId, String userId) {
        final Role chatRole = getRole(chatId);
        final Role userRole = getRole(userId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("CHAT " + chatId + " = " + chatRole);
            LOGGER.debug("USER " + userId + " = " + userRole);
        }

        if (userRole == Role.OWNER)
            return Role.OWNER;  // Owner is allowed to do anything anywhere

        switch (chatRole) {
            case NONE:
                return Role.NONE;

            case USER:
                return Role.USER;

            case ADMIN:
                switch (userRole) {
                    case NONE:
                    case USER:
                        return Role.USER;

                    case ADMIN:
                        return Role.ADMIN;

                    case SUPER_ADMIN:
                        return Role.SUPER_ADMIN;
                }

            case SUPER_ADMIN:
                switch (userRole) {
                    case NONE:
                    case USER:
                    case ADMIN: {
                        LOGGER.error("Unexpected user role for SUPER_ADMIN chat: " + userRole);
                        return Role.NONE;
                    }

                    case SUPER_ADMIN:
                        return Role.SUPER_ADMIN;
                }

            case OWNER: {
                LOGGER.error("Unexpected user role for OWNER chat: " + userRole);
                return Role.NONE;
            }
        }

        LOGGER.error("Unexpected combination of chat/user roles: " + chatRole + "/" + userRole);
        return Role.NONE;
    }

    private Role getRole(String userId) {
        return userStorage.visit(userId, user -> {
            if (user != null && user.getStatus() == Status.NORMAL)
                return user.getRole();
            return Role.NONE;
        });
    }

    public boolean isSendingFilesAllowed() {
        return config.getFilesAllowed();
    }

    public String getGreeting() {
        return config.getGreeting();
    }

    public Result init(String chatId) {
        if (!userStorage.isEmpty())
            return new Result(ResultStatus.INIT_ERROR);

        final UserDto user = new UserDto();
        user.setUserId(chatId);
        user.setIsChat(false);
        user.setRole(Role.OWNER);
        user.setStatus(Status.NORMAL);
        // Race condition here. Not too critical, since we only run init once
        return addUser(user);
    }

    public Result setTokenTimeout(String fromId, long tokenTimeout) {
        final String name = userStorage.visit(fromId, BlinksBot::getUserName);
        if (name == null)
            return new Result(ResultStatus.USER_NOT_FOUND);

        final long prev = config.getTokenTimeout();
        config.setTokenTimeout(tokenTimeout);
        config.setAdminId(fromId);
        config.setAdminName(name);
        final int id = storage.setConfig(config);
        if (id <= 0) {
            config.setTokenTimeout(prev);
            LOGGER.warn("Could not change token timeout.");
            return new Result(ResultStatus.DB_ERROR);
        }
        config.setId(id);
        tokenStorage.setTimeout(tokenTimeout);
        return Result.OK;
    }

    public Result changeAllowSendingFiles(String fromId, boolean allow) {
        final String name = userStorage.visit(fromId, BlinksBot::getUserName);
        if (name == null)
            return new Result(ResultStatus.USER_NOT_FOUND);

        final boolean prev = config.getFilesAllowed();
        config.setFilesAllowed(allow);
        config.setAdminId(fromId);
        config.setAdminName(name);
        final int id = storage.setConfig(config);
        if (id <= 0) {
            config.setFilesAllowed(prev);
            LOGGER.warn("Could not change files allowed.");
            return new Result(ResultStatus.DB_ERROR);
        }
        config.setId(id);
        return Result.OK;
    }

    public Result changeGreeting(String fromId, String text) {
        final String name = userStorage.visit(fromId, BlinksBot::getUserName);
        if (name == null)
            return new Result(ResultStatus.USER_NOT_FOUND);

        final String prev = config.getGreeting();
        config.setGreeting(text);
        config.setAdminId(fromId);
        config.setAdminName(name);
        final int id = storage.setConfig(config);
        if (id <= 0) {
            config.setGreeting(prev);
            LOGGER.warn("Could not change greeting.");
            return new Result(ResultStatus.DB_ERROR);
        }
        config.setId(id);
        return Result.OK;
    }

    public boolean log(String command, ResultStatus status) {
        final LogEntryDto logEntry = new LogEntryDto();
        logEntry.setTimestamp(System.currentTimeMillis());
        logEntry.setChatId(LogEntryDto.NO_USER);
        logEntry.setUserId(LogEntryDto.NO_USER);
        logEntry.setCommand(command);
        logEntry.setStatus(status);
        return storage.insertLogEntry(logEntry);
    }

    public boolean log(String chatId, String userId, String command, ResultStatus status) {
        final UserDto chat = userStorage.visit(chatId, BlinksBot::cloneUser);
        final UserDto user = userStorage.visit(userId, BlinksBot::cloneUser);

        final LogEntryDto logEntry = new LogEntryDto();
        logEntry.setTimestamp(System.currentTimeMillis());
        if (chat == null) {
            logEntry.setChatId(LogEntryDto.UNKNOWN_USER);
        } else {
            logEntry.setChatId(chat.getId());
            logEntry.setChatName(chat.getName());
            logEntry.setChatRole(chat.getRole());
        }
        if (user == null) {
            logEntry.setUserId(LogEntryDto.UNKNOWN_USER);
        } else {
            logEntry.setUserId(user.getId());
            logEntry.setUserName(user.getName());
            logEntry.setUserRole(user.getRole());
        }
        logEntry.setCommand(command);
        logEntry.setStatus(status);
        return storage.insertLogEntry(logEntry);
    }

    public List<LogEntryDto> getLogs(int count) {
        final List<LogEntryDto> ret = storage.queryLogEntries(count);
        for (LogEntryDto logEntry : ret) {
            if (logEntry.getChatId() != LogEntryDto.NO_USER &&
                    logEntry.getChatId() != LogEntryDto.UNKNOWN_USER) {
                final String name = userStorage.visit(logEntry.getChatId(), BlinksBot::getUserName);
                if (name == null) {
                    logEntry.setChatName(logEntry.getChatName() + " (удалён)");
                } else {
                    logEntry.setChatName(name);
                }
            }
            if (logEntry.getUserId() != LogEntryDto.NO_USER &&
                    logEntry.getUserId() != LogEntryDto.UNKNOWN_USER) {
                final String name = userStorage.visit(logEntry.getUserId(), BlinksBot::getUserName);
                if (name == null) {
                    logEntry.setUserName(logEntry.getUserName() + " (удалён)");
                } else {
                    logEntry.setUserName(name);
                }
            }
        }
        return ret;
    }

    public List<UserDto> listAll() {
        return userStorage.list();
    }

    public List<UserDto> listAdmins() {
        return userStorage.list(Role.ADMIN, Role.SUPER_ADMIN, Role.OWNER);
    }

    public List<UserDto> listUsers() {
        return userStorage.list(Role.USER);
    }

    public Result getUserInfo(String fromId, int id) {
        final UserDto user = userStorage.visit(id, BlinksBot::cloneUser);
        if (user == null)
            return new Result(ResultStatus.USER_NOT_FOUND);
        final Role role = getRole(fromId);
        if (!role.isAllowedModifyOther(user.getRole())) {
            return new Result(ResultStatus.NOT_ALLOWED_USER);
        }
        return new Result(ResultStatus.OK, user);
    }

    public String generateToken(boolean isChat, Role role, String name) {
        return tokenStorage.register(UserDto.createForToken(isChat, role, name));
    }

    public boolean revoke(String token) {
        return tokenStorage.test(token) != null;
    }

    // If OK - must return user.
    public Result join(String chatId, String userId, Integer messageId, String token) {
        final UserDto user;
        if ((token == null || token.isEmpty()) && autoJoinAllowed) {
            user = new UserDto();
            user.setUserId(chatId);
            user.setIsChat(!Objects.equals(chatId, userId));
            user.setRole(Role.USER);
            user.setStatus(Status.NORMAL);
            user.setName(null);
        } else {
            final Object obj = tokenStorage.test(token);
            if (obj == null)
                return new Result(ResultStatus.TOKEN_NOT_FOUND);
            if (!(obj instanceof UserDto)) {
                LOGGER.error("Unexpected type: " + obj);
                return new Result(ResultStatus.CRITICAL);
            }
            user = (UserDto) obj;
        }
        if (user.getIsChat()) {
            if (Objects.equals(chatId, userId)) {
                tokenStorage.returnRemoved(token, user);
                return new Result(ResultStatus.JOIN_USER_AS_CHAT);
            }
            LOGGER.debug("Adding chat: " + chatId);
            user.setUserId(chatId);
        } else {
            LOGGER.debug("Adding user: " + userId);
            user.setUserId(userId);
        }
        final Result ret = addUser(user);
        if (ret.getStatus() == ResultStatus.OK) {
            sendToAdmin(chatId, userId, messageId,
                    "Новый присоединившийся: " + ret.getUser().getDescription());
        }
        return ret;
    }

    public Result addChat(String chatId, Role role, String name) {
        final UserDto user = new UserDto();
        user.setUserId(chatId);
        user.setIsChat(true);
        user.setRole(role);
        user.setStatus(Status.NORMAL);
        user.setName(name);
        return addUser(user);
    }

    private Result addUser(UserDto user) {
        final UserDto existing = userStorage.remove(user.getUserId());
        if (existing != null) {
            if (user.getRole().getNumber() <= existing.getRole().getNumber()) {
                userStorage.add(existing);
                user.setRole(existing.getRole());
                return new Result(ResultStatus.CANNOT_LOWER_ROLE, user);
            }
            final Role newRole = user.getRole();
            user.copyFrom(existing);
            user.setRole(newRole);
            ResultStatus result = ResultStatus.OK;
            if (storage.updateUser(user)) {
                existing.setRole(user.getRole());
            } else {
                LOGGER.warn("Could not save user: " + user);
                result = ResultStatus.DB_ERROR;
            }
            userStorage.add(existing);
            return new Result(result, user);
        } else {
            final int id = storage.insertUser(user);
            if (id < 0) {
                LOGGER.warn("Could not save user: " + user);
                return new Result(ResultStatus.DB_ERROR);
            }
            user.setId(id);
            userStorage.add(user);
            return new Result(ResultStatus.OK, user);
        }
    }

    public Result blockUser(String fromId, int id, boolean smart, String reason) {
        final Role role = getRole(fromId);
        final UserDto user = userStorage.remove(id);
        if (user == null)
            return new Result(ResultStatus.USER_NOT_FOUND);
        try {
            if (!role.isAllowedModifyOther(user.getRole())) {
                return new Result(ResultStatus.NOT_ALLOWED_USER);
            }
            final Status prevStatus = user.getStatus();
            final String prevReason = user.getReason();
            user.setStatus(smart ? Status.SMART_BLOCKED : Status.BLOCKED);
            user.setReason(reason);
            if (!storage.updateUser(user)) {
                user.setStatus(prevStatus);
                user.setReason(prevReason);
                return new Result(ResultStatus.DB_ERROR);
            }
            return Result.OK;
        } finally {
            userStorage.add(user);
        }
    }

    public Result unblockUser(String fromId, int id) {
        final Role role = getRole(fromId);
        final UserDto user = userStorage.remove(id);
        if (user == null)
            return new Result(ResultStatus.USER_NOT_FOUND);
        try {
            if (!role.isAllowedModifyOther(user.getRole())) {
                return new Result(ResultStatus.NOT_ALLOWED_USER);
            }
            final Status prev = user.getStatus();
            user.setStatus(Status.NORMAL);
            if (!storage.updateUser(user)) {
                user.setStatus(prev);
                return new Result(ResultStatus.DB_ERROR);
            }
            return Result.OK;
        } finally {
            userStorage.add(user);
        }
    }

    public Result setUserName(String fromId, int id, String name) {
        final Role role = getRole(fromId);
        final UserDto user = userStorage.remove(id);
        if (user == null)
            return new Result(ResultStatus.USER_NOT_FOUND);
        try {
            if (!role.isAllowedModifyOther(user.getRole())) {
                return new Result(ResultStatus.NOT_ALLOWED_USER);
            }
            final String prev = user.getName();
            if (name == null || name.isEmpty()) {
                user.setName(null);
            } else {
                user.setName(name);
            }
            if (!storage.updateUser(user)) {
                user.setName(prev);
                return new Result(ResultStatus.DB_ERROR);
            }
            return Result.OK;
        } finally {
            userStorage.add(user);
        }
    }

    public Result up(String fromId, int id) {
        final Role role = getRole(fromId);
        final UserDto user = userStorage.remove(id);
        if (user == null)
            return new Result(ResultStatus.USER_NOT_FOUND);
        try {
            if (!role.isAllowedModifyOther(user.getRole())) {
                return new Result(ResultStatus.NOT_ALLOWED_USER);
            }
            final Role prev = user.getRole();
            if (user.getRole() == Role.USER) {
                user.setRole(Role.ADMIN);
            } else if (user.getRole() == Role.ADMIN && !user.getIsChat()) {
                user.setRole(Role.SUPER_ADMIN);
            } else {
                return new Result(ResultStatus.DENIED, "Нельзя повысить роль выше существующей.");
            }
            if (!storage.updateUser(user)) {
                user.setRole(prev);
                return new Result(ResultStatus.DB_ERROR);
            }
            return new Result(ResultStatus.OK, user.clone());
        } finally {
            userStorage.add(user);
        }
    }

    public Result down(String fromId, int id) {
        final Role role = getRole(fromId);
        final UserDto user = userStorage.remove(id);
        if (user == null)
            return new Result(ResultStatus.USER_NOT_FOUND);
        try {
            if (!role.isAllowedModifyOther(user.getRole())) {
                return new Result(ResultStatus.NOT_ALLOWED_USER);
            }
            final Role prev = user.getRole();
            if (user.getRole() == Role.ADMIN) {
                user.setRole(Role.USER);
            } else if (user.getRole() == Role.SUPER_ADMIN && !user.getIsChat()) {
                user.setRole(Role.ADMIN);
            } else {
                return new Result(ResultStatus.DENIED, "Нельзя понизить роль ниже существующей.");
            }
            if (!storage.updateUser(user)) {
                user.setRole(prev);
                return new Result(ResultStatus.DB_ERROR);
            }
            return new Result(ResultStatus.OK, user.clone());
        } finally {
            userStorage.add(user);
        }
    }

    public Result removeUser(String fromId, int id) {
        final Role role = getRole(fromId);
        final UserDto user = userStorage.remove(id);
        if (user == null)
            return new Result(ResultStatus.USER_NOT_FOUND);
        if (user.getRole() == Role.OWNER) {
            userStorage.add(user);
            return new Result(ResultStatus.NOT_ALLOWED_USER); // Deleting owner is never allowed
        }
        if (!role.isAllowedModifyOther(user.getRole())) {
            userStorage.add(user);
            return new Result(ResultStatus.NOT_ALLOWED_USER);
        }
        if (storage.deleteUser(user)) {
            return Result.OK;
        } else {
            userStorage.add(user);
            return new Result(ResultStatus.DB_ERROR);
        }
    }

    public String getTag(String fromId, MessageMeta.Type type, int to) {
        final MessageMeta meta = new MessageMeta();
        meta.setFromId(fromId);
        meta.setType(type);
        meta.setTo(to);
        return TagUtils.escape(tokenStorage.register(meta));
    }

    public Result sendToAdmin(String fromChatId,
                              String fromUserId,
                              Integer fromMessageId,
                              String message) {
        return sendToAdmin(fromChatId, fromUserId, fromMessageId, message, this::sendMessageTo, null, true);
    }

    public Result sendTo(String fromChatId,
                         String fromUserId,
                         Integer fromMessageId,
                         int id,
                         String message) {
        return sendTo(fromChatId, fromUserId, fromMessageId, id, message, this::sendMessageTo);
    }

    public Result sendToAndAdmin(String fromChatId,
                                 String fromUserId,
                                 Integer fromMessageId,
                                 int id,
                                 String message) {
        return sendToAndAdmin(fromChatId, fromUserId, fromMessageId, id, message, this::sendMessageTo);
    }

    public Result sendToAll(String fromChatId,
                            String fromUserId,
                            Integer fromMessageId,
                            String message) {
        return sendToAll(fromChatId, fromUserId, fromMessageId, message, this::sendMessageTo);
    }

    private void processMyChatMember(ChatMemberUpdated chatMemberUpdated) {
        final ChatMember chatMember = chatMemberUpdated.getNewChatMember();
        final String status = chatMember.getStatus();
        if (!Objects.equals(status, "member") && !Objects.equals(status, "administrator")) {
            return;
        }

        final String chatId = Long.toString(chatMemberUpdated.getChat().getId());
        final String greeting = config.getGreeting();

        try {
            sendHtml(chatId, greeting);
        } catch (TelegramApiException ex) {
            LOGGER.warn("Could not send message: " + ex, ex);
        }
    }

    private void processMessage(Message message) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(message.toString());
        }

        final String chatId = Long.toString(message.getChatId());

        final Long migrateFromId = message.getMigrateFromChatId();
        final Long migrateToId = message.getMigrateToChatId();
        if (migrateFromId != null) {
            tryMigrate(Long.toString(migrateFromId), chatId);
        }
        if (migrateToId != null) {
            tryMigrate(chatId, Long.toString(migrateToId));
        }

        final String userId = Long.toString(message.getFrom().getId());
        final Role role = getRole(chatId, userId);
        if (role.getNumber() <= Role.USER.getNumber() && !isSendingFilesAllowed()) {
            if (message.hasDocument()) {
                sendReplyOrError(chatId, message.getMessageId(), "" +
                        "Не удалось переслать сообщение. Отправлять файлы запрещено.");
                return;
            }
        }

        final Message original = message.getReplyToMessage();
        if (original != null && Objects.equals(original.getFrom().getId(), getBotUser().getId())) {
            // This is a reply to a bots message
            final String tag = TagUtils.parse(original.getText(), TokenStorage.getTokenPattern());
            if (tag == null) {
                if (!message.isCommand()) {
                    sendReplyOrError(chatId, message.getMessageId(), "" +
                            "Отправьте команду /send в ответ на ваше сообщение чтобы отправить сообщение."
                    );
                    return;
                }
                return;
            }
            final Object obj = tokenStorage.test(tag);
            if (obj == null) {
                sendReplyOrError(chatId, message.getMessageId(), "" +
                        "Не получилось отправить сообщение. Возможно, запрос устарел. " +
                        "Пошлите команду ещё раз и отправьте своё сообщение " +
                        "в ответ на пришедшее сообщение бота."
                );
                return;
            }
            if (!(obj instanceof MessageMeta)) {
                LOGGER.error("Unexpected meta type: " + obj.getClass());
                return;
            }
            final MessageMeta meta = (MessageMeta) obj;
            if (!Objects.equals(meta.getFromId(), userId)) {
                sendReplyOrError(chatId, message.getMessageId(), "" +
                        "Нельзя отправить сообщение в ответ на не свой запрос."
                );
                return;
            }
            final Result result;
            switch (meta.getType()) {
                case ADMIN:
                    result = sendToAdmin(message);
                    break;

                case DIRECT:
                    result = sendTo(meta.getTo(), message);
                    break;

                case DIRECT_AND_ADMIN:
                    result = sendToAndAdmin(meta.getTo(), message);
                    break;

                case BROADCAST:
                    result = sendToAll(message);
                    break;

                default:
                    result = null;
            }

            if (result == null) {
                LOGGER.error("Unexpected meta: " + meta);
            } else if (result.getStatus() != ResultStatus.OK) {
                sendReplyOrError(chatId, message.getMessageId(), "" +
                        "Не удалось отправить сообщение. " + result.getDescription()
                );
            }
        } else if (original == null && Objects.equals(chatId, userId)) {
            if (!message.isCommand()) {
                sendReplyOrError(chatId, message.getMessageId(), "" +
                        "Отправьте команду /send в ответ на ваше сообщение чтобы отправить сообщение."
                );
            }
        }
    }

    public void tryMigrate(String fromId, String toId) {
        final UserDto user = userStorage.remove(fromId);
        if (user == null)
            return;
        try {
            user.setUserId(toId);
            if (!storage.updateUser(user)) {
                LOGGER.warn("Could not change group id from " + fromId + " to " + toId);
            } else {
                LOGGER.debug("Changed group id from " + fromId + " to " + toId);
            }
        } finally {
            userStorage.add(user);
        }
    }

    public Result sendToAdmin(Message message) {
        final String fromChatId = Long.toString(message.getChatId());
        final String fromUserId = Long.toString(message.getFrom().getId());
        final Integer fromMessageId = message.getMessageId();
        return sendToAdmin(fromChatId, fromUserId, fromMessageId, message, this::sendMessageTo, null, true);
    }

    public Result sendTo(int id, Message message) {
        final String fromChatId = Long.toString(message.getChatId());
        final String fromUserId = Long.toString(message.getFrom().getId());
        final Integer fromMessageId = message.getMessageId();
        return sendTo(fromChatId, fromUserId, fromMessageId, id, message, this::sendMessageTo);
    }

    public Result sendToAndAdmin(int id, Message message) {
        final String fromChatId = Long.toString(message.getChatId());
        final String fromUserId = Long.toString(message.getFrom().getId());
        final Integer fromMessageId = message.getMessageId();
        return sendToAndAdmin(fromChatId, fromUserId, fromMessageId, id, message, this::sendMessageTo);
    }

    public Result sendToAll(Message message) {
        final String fromChatId = Long.toString(message.getChatId());
        final String fromUserId = Long.toString(message.getFrom().getId());
        final Integer fromMessageId = message.getMessageId();
        return sendToAll(fromChatId, fromUserId, fromMessageId, message, this::sendMessageTo);
    }

    public Result forwardToAdmin(Message message) {
        final String fromChatId = Long.toString(message.getChatId());
        final String fromUserId = Long.toString(message.getFrom().getId());
        final Integer fromMessageId = message.getMessageId();
        return sendToAdmin(fromChatId, fromUserId, fromMessageId, message, this::forwardMessageTo, null, true);
    }

    public Result forwardTo(int id, Message message) {
        final String fromChatId = Long.toString(message.getChatId());
        final String fromUserId = Long.toString(message.getFrom().getId());
        final Integer fromMessageId = message.getMessageId();
        return sendTo(fromChatId, fromUserId, fromMessageId, id, message, this::forwardMessageTo);
    }

    public Result forwardToAll(Message message) {
        final String fromChatId = Long.toString(message.getChatId());
        final String fromUserId = Long.toString(message.getFrom().getId());
        final Integer fromMessageId = message.getMessageId();
        return sendToAll(fromChatId, fromUserId, fromMessageId, message, this::forwardMessageTo);
    }

    public <T> Result sendToAdmin(String fromChatId,
                                  String fromUserId,
                                  Integer fromMessageId,
                                  T message,
                                  SendMessageTo<T> sender,
                                  String receiver,
                                  boolean confirm) {
        String senderName = userStorage.visit(fromUserId, BlinksBot::getUserName);
        if (senderName == null) {
            senderName = userStorage.visit(fromChatId, BlinksBot::getUserName);
        }
        if (senderName == null) {
            LOGGER.error("Sender not found: " + fromChatId + ", " + fromUserId);
            LOGGER.error("Message was: " + message);
            return new Result(ResultStatus.USER_NOT_FOUND);
        }
        final String senderStr;
        if (Objects.equals(fromUserId, fromChatId)) {
            senderStr = senderName;
        } else {
            final String chatName = userStorage.visit(fromChatId, BlinksBot::getUserName);
            if (chatName == null) {
                senderStr = "[] / " + senderName;
            } else {
                senderStr = chatName + " / " + senderName;
            }
        }
        final String prefix;
        if (receiver == null) {
            prefix = Emoji.Mail.ENVELOPE + " от: <b>" + senderStr + "</b>\n\n";
        } else {
            prefix = Emoji.Mail.ENVELOPE + " <b>" + senderStr + "</b> " + Emoji.Arrow.RIGHT + " <b>" + receiver + "</b>\n\n";
        }
        final List<UserDto> all = listAll();
        boolean hasAdminChats = false;
        int successCount = 0;
        int errorsCount = 0;
        for (UserDto user : all) {
            if (user.getStatus() != Status.NORMAL)
                continue;
            if (user.getRole() == Role.ADMIN && user.getIsChat()) {
                hasAdminChats = true;
                if (!Objects.equals(fromChatId, user.getUserId())) {
                    if (sender.apply(user.getUserId(), prefix, message,
                            reply -> sendReplyOrError(fromChatId, fromMessageId, reply))) {
                        ++successCount;
                    } else {
                        ++errorsCount;
                    }
                }
            }
        }
        if (!hasAdminChats) {
            for (UserDto user : all) {
                if (user.getStatus() != Status.NORMAL)
                    continue;
                if (user.getRole() == Role.ADMIN ||
                        user.getRole() == Role.SUPER_ADMIN ||
                        user.getRole() == Role.OWNER) {
                    if (!Objects.equals(fromChatId, user.getUserId())) {
                        if (sender.apply(user.getUserId(), prefix, message,
                                reply -> sendReplyOrError(fromChatId, fromMessageId, reply))) {
                            ++successCount;
                        } else {
                            ++errorsCount;
                        }
                    }
                }
            }
        }
        if (confirm) {
            Role senderRole = getRole(fromChatId, fromUserId);
            if (senderRole == Role.ADMIN ||
                    senderRole == Role.SUPER_ADMIN ||
                    senderRole == Role.OWNER) {
                if (successCount != 0 && errorsCount == 0) {
                    sendReplyOrError(fromChatId, fromMessageId, "Сообщение успешно отправлено. Количество получателей: " + successCount);
                } else if (successCount != 0) {
                    sendReplyOrError(fromChatId, fromMessageId, "Сообщение отправлено не всем. Количество получателей: " + successCount);
                } else if (errorsCount != 0) {
                    sendReplyOrError(fromChatId, fromMessageId, "Не получилось отправить сообщение.");
                } else {
                    sendReplyOrError(fromChatId, fromMessageId, "Сообщение отправлять некому.");
                }
            } else {
                if (successCount != 0) {
                    sendReplyOrError(fromChatId, fromMessageId, "Сообщение отправлено.");
                } else {
                    sendReplyOrError(fromChatId, fromMessageId, "Сообщение не отправлено. Обратитесь в бот @kpd_by_bot за помощью.");
                }
            }
        }
        return Result.OK;
    }

    public <T> Result sendTo(String fromChatId,
                             String fromUserId,
                             Integer fromMessageId,
                             int id,
                             T message,
                             SendMessageTo<T> sender) {
        final String senderName = userStorage.visit(fromUserId, BlinksBot::getUserName);
        if (senderName == null) {
            LOGGER.error("User not found: " + fromUserId);
            LOGGER.error("Message was: " + message);
            return new Result(ResultStatus.USER_NOT_FOUND);
        }
        final String senderStr;
        if (Objects.equals(fromUserId, fromChatId)) {
            senderStr = senderName;
        } else {
            final String chatName = userStorage.visit(fromChatId, BlinksBot::getUserName);
            if (chatName == null) {
                senderStr = "[] / " + senderName;
            } else {
                senderStr = chatName + " / " + senderName;
            }
        }
        final UserDto receiver = userStorage.visit(id, BlinksBot::cloneUser);
        if (receiver == null) {
            return new Result(ResultStatus.USER_NOT_FOUND);
        }
        final String prefix;
        if (receiver.getRole() == Role.ADMIN ||
                receiver.getRole() == Role.SUPER_ADMIN ||
                receiver.getRole() == Role.OWNER) {
            prefix = Emoji.Mail.ENVELOPE + " от: <b>" + senderStr + "</b>\n\n";
        } else {
            prefix = null;
        }
        if (sender.apply(receiver.getUserId(), prefix, message,
                reply -> sendReplyOrError(fromChatId, fromMessageId, reply),
                ex -> onError(fromChatId, fromMessageId, receiver.getDescription(), ex))) {
            sendReplyOrError(fromChatId, fromMessageId, "Сообщение успешно отправлено: " + receiver.getDescription());
        } else {
            sendReplyOrError(fromChatId, fromMessageId, "Не получилось отправить сообщение.");
        }
        return new Result(ResultStatus.OK, receiver);
    }

    public <T> Result sendToAndAdmin(String fromChatId,
                                     String fromUserId,
                                     Integer fromMessageId,
                                     int id,
                                     T message,
                                     SendMessageTo<T> sender) {
        final Result result = sendTo(fromChatId, fromUserId, fromMessageId, id, message, sender);
        if (result.getStatus() != ResultStatus.OK)
            return result;
        return sendToAdmin(fromChatId, fromUserId, fromMessageId, message, sender, result.getUser().getDescription(), false);
    }

    public <T> Result sendToAll(String fromChatId,
                                String fromUserId,
                                Integer fromMessageId,
                                T message,
                                SendMessageTo<T> sender) {
        final String senderName = userStorage.visit(fromUserId, BlinksBot::getUserName);
        if (senderName == null) {
            LOGGER.error("User not found: " + fromUserId);
            LOGGER.error("Message was: " + message);
            return new Result(ResultStatus.USER_NOT_FOUND);
        }
        final List<UserDto> all = listAll();
        int successCount = 0;
        int errorsCount = 0;
        for (UserDto user : all) {
            if (user.getStatus() != Status.NORMAL)
                continue;
            if (user.getRole() == Role.USER) {
                if (sender.apply(user.getUserId(), null, message,
                        reply -> sendReplyOrError(fromChatId, fromMessageId, reply),
                        ex -> onError(fromChatId, fromMessageId, user.getDescription(), ex))) {
                    ++successCount;
                } else {
                    ++errorsCount;
                }
            }
        }
        final String senderStr;
        if (Objects.equals(fromUserId, fromChatId)) {
            senderStr = senderName;
        } else {
            final String chatName = userStorage.visit(fromChatId, BlinksBot::getUserName);
            if (chatName == null) {
                senderStr = "[] / " + senderName;
            } else {
                senderStr = chatName + " / " + senderName;
            }
        }
        final String prefix = Emoji.Mail.PACKAGE + " от: <b>" + senderStr + "</b> всем\n\n";
        boolean hasAdminChats = false;
        for (UserDto user : all) {
            if (user.getStatus() != Status.NORMAL)
                continue;
            if (user.getRole() == Role.ADMIN && user.getIsChat()) {
                hasAdminChats = true;
                if (!Objects.equals(fromChatId, user.getUserId())) {
                    if (sender.apply(user.getUserId(), prefix, message,
                            reply -> sendReplyOrError(fromChatId, fromMessageId, reply),
                            ex -> onError(fromChatId, fromMessageId, user.getDescription(), ex))) {
                        ++successCount;
                    } else {
                        ++errorsCount;
                    }
                }
            }
        }
        if (!hasAdminChats) {
            for (UserDto user : all) {
                if (user.getStatus() != Status.NORMAL)
                    continue;
                if (user.getRole() == Role.ADMIN ||
                        user.getRole() == Role.SUPER_ADMIN ||
                        user.getRole() == Role.OWNER) {
                    if (!Objects.equals(fromChatId, user.getUserId())) {
                        if (sender.apply(user.getUserId(), prefix, message,
                                reply -> sendReplyOrError(fromChatId, fromMessageId, reply))) {
                            ++successCount;
                        }
                    }
                }
            }
        }
        if (successCount != 0 && errorsCount == 0) {
            sendReplyOrError(fromChatId, fromMessageId, "Сообщение успешно отправлено. Количество получателей: " + successCount);
        } else if (successCount != 0) {
            sendReplyOrError(fromChatId, fromMessageId, "Сообщение отправлено не всем. Количество получателей: " + successCount);
        } else if (errorsCount != 0) {
            sendReplyOrError(fromChatId, fromMessageId, "Не получилось отправить сообщение.");
        } else {
            sendReplyOrError(fromChatId, fromMessageId, "Сообщение отправлять некому.");
        }
        return Result.OK;
    }

    public <T> Result broadcastFromMaster(String senderName,
                                          T message,
                                          SendMessageTo<T> sender,
                                          Consumer<String> onReply,
                                          Consumer<TelegramApiException> onError) {
        final List<UserDto> all = listAll();
        int successCount = 0;
        int errorsCount = 0;
        for (UserDto user : all) {
            if (user.getStatus() != Status.NORMAL)
                continue;
            if (user.getRole() == Role.USER) {
                if (sender.apply(user.getUserId(), null, message, onReply, onError)) {
                    ++successCount;
                } else {
                    ++errorsCount;
                }
            }
        }
        final String senderStr = "[Мастер-бот] / " + senderName;
        final String prefix = Emoji.Mail.PACKAGE + " от: <b>" + senderStr + "</b> всем\n\n";
        boolean hasAdminChats = false;
        for (UserDto user : all) {
            if (user.getStatus() != Status.NORMAL)
                continue;
            if (user.getRole() == Role.ADMIN && user.getIsChat()) {
                hasAdminChats = true;
                if (sender.apply(user.getUserId(), prefix, message, onReply, onError)) {
                    ++successCount;
                } else {
                    ++errorsCount;
                }
            }
        }
        if (!hasAdminChats) {
            for (UserDto user : all) {
                if (user.getStatus() != Status.NORMAL)
                    continue;
                if (user.getRole() == Role.ADMIN ||
                        user.getRole() == Role.SUPER_ADMIN ||
                        user.getRole() == Role.OWNER) {
                    if (sender.apply(user.getUserId(), prefix, message, onReply)) {
                        ++successCount;
                    }
                }
            }
        }
        if (successCount != 0 && errorsCount == 0) {
            onReply.accept("Сообщение успешно отправлено. Количество получателей: " + successCount);
        } else if (successCount != 0) {
            onReply.accept("Сообщение отправлено не всем. Количество получателей: " + successCount);
        } else if (errorsCount != 0) {
            onReply.accept("Не получилось отправить сообщение.");
        } else {
            onReply.accept("Сообщение отправлять некому.");
        }
        return Result.OK;
    }

    protected boolean sendMessageTo(String chatId,
                                    String prefix,
                                    Message message,
                                    Consumer<String> onReply,
                                    Consumer<TelegramApiException> onError) {
        return sendMessageTo(chatId, prefix, message, null, onReply, onError);
    }

    // TODO: find a way of processing albums
    protected boolean sendMessageTo(String chatId,
                                    String prefix,
                                    Message message,
                                    File file,
                                    Consumer<String> onReply,
                                    Consumer<TelegramApiException> onError) {
        if (message.hasText()) {
            return sendMessageTo(chatId, prefix, message.getText(), onReply, onError);
        } else if (message.hasPhoto()) {
            final List<PhotoSize> list = message.getPhoto();
            final String caption = getCaption(prefix, message);
            final SendPhoto sendPhoto = new SendPhoto();
            sendPhoto.setChatId(chatId);
            if (file != null) {
                sendPhoto.setPhoto(new InputFile(file));
            } else {
                sendPhoto.setPhoto(new InputFile(list.get(0).getFileId()));
            }
            sendPhoto.setCaption(caption);
            sendPhoto.setParseMode("HTML");
            try {
                execute(sendPhoto);
                return true;
            } catch (TelegramApiException ex) {
                onError.accept(ex);
                return false;
            }
        } else if (message.hasVideo()) {
            final Video video = message.getVideo();
            final String caption = getCaption(prefix, message);
            final SendVideo sendVideo = new SendVideo();
            sendVideo.setChatId(chatId);
            if (file != null) {
                sendVideo.setVideo(new InputFile(file));
            } else {
                sendVideo.setVideo(new InputFile(video.getFileId()));
            }
            sendVideo.setDuration(video.getDuration());
            sendVideo.setCaption(caption);
            sendVideo.setParseMode("HTML");
            sendVideo.setWidth(video.getWidth());
            sendVideo.setHeight(video.getHeight());
            try {
                execute(sendVideo);
                return true;
            } catch (TelegramApiException ex) {
                onError.accept(ex);
                return false;
            }
        } else if (message.hasVideoNote()) {
            final VideoNote videoNode = message.getVideoNote();
            final SendVideoNote sendVideoNote = new SendVideoNote();
            sendVideoNote.setChatId(chatId);
            if (file != null) {
                sendVideoNote.setVideoNote(new InputFile(file));
            } else {
                sendVideoNote.setVideoNote(new InputFile(videoNode.getFileId()));
            }
            sendVideoNote.setDuration(videoNode.getDuration());
            sendVideoNote.setLength(videoNode.getLength());
            try {
                execute(sendVideoNote);
                return true;
            } catch (TelegramApiException ex) {
                onError.accept(ex);
                return false;
            }
        } else if (message.hasAudio()) {
            final Audio audio = message.getAudio();
            final String caption = getCaption(prefix, message);
            final SendAudio sendAudio = new SendAudio();
            sendAudio.setChatId(chatId);
            if (file != null) {
                sendAudio.setAudio(new InputFile(file));
            } else {
                sendAudio.setAudio(new InputFile(audio.getFileId()));
            }
            sendAudio.setDuration(audio.getDuration());
            sendAudio.setCaption(caption);
            sendAudio.setParseMode("HTML");
            sendAudio.setTitle(audio.getTitle());
            try {
                execute(sendAudio);
                return true;
            } catch (TelegramApiException ex) {
                onError.accept(ex);
                return false;
            }
        } else if (message.hasVoice()) {
            final Voice voice = message.getVoice();
            final String caption = getCaption(prefix, message);
            final SendVoice sendVoice = new SendVoice();
            sendVoice.setChatId(chatId);
            if (file != null) {
                sendVoice.setVoice(new InputFile(file));
            } else {
                sendVoice.setVoice(new InputFile(voice.getFileId()));
            }
            sendVoice.setDuration(voice.getDuration());
            sendVoice.setCaption(caption);
            sendVoice.setParseMode("HTML");
            try {
                execute(sendVoice);
                return true;
            } catch (TelegramApiException ex) {
                onError.accept(ex);
                return false;
            }
        } else if (message.hasDocument()) {
            final Document document = message.getDocument();
            final String caption = getCaption(prefix, message);
            final SendDocument sendDocument = new SendDocument();
            sendDocument.setChatId(chatId);
            if (file != null) {
                sendDocument.setDocument(new InputFile(file));
            } else {
                sendDocument.setDocument(new InputFile(document.getFileId()));
            }
            sendDocument.setCaption(caption);
            sendDocument.setParseMode("HTML");
            try {
                execute(sendDocument);
                return true;
            } catch (TelegramApiException ex) {
                onError.accept(ex);
                return false;
            }
        } else if (message.hasSticker()) {
            final Sticker sticker = message.getSticker();
            final SendSticker sendSticker = new SendSticker();
            sendSticker.setChatId(chatId);
            if (file != null) {
                sendSticker.setSticker(new InputFile(file));
            } else {
                sendSticker.setSticker(new InputFile(sticker.getFileId()));
            }
            try {
                execute(sendSticker);
                return true;
            } catch (TelegramApiException ex) {
                onError.accept(ex);
                return false;
            }
        } else {
            onReply.accept("Данный тип сообщений нельзя переслать через бота.");
            return false;
        }
    }

    public boolean sendMessageTo(String chatId,
                                 String prefix,
                                 String text,
                                 Consumer<String> onReply,
                                 Consumer<TelegramApiException> onError) {
        if (prefix != null) {
            text = prefix + text;
        }
        try {
            sendHtml(chatId, text);
            return true;
        } catch (TelegramApiException ex) {
            onError.accept(ex);
            return false;
        }
    }

    private boolean forwardMessageTo(String chatId,
                                     String prefix,
                                     Message message,
                                     Consumer<String> onReply,
                                     Consumer<TelegramApiException> onError) {
        try {
            if (prefix != null && !prefix.isEmpty()) {
                sendHtml(chatId, prefix);
            }
            forwardMessage(chatId, message);
            return true;
        } catch (TelegramApiException ex) {
            onError.accept(ex);
            return false;
        }
    }

    public Result resend(String fromChatId, String fromUserId, Integer fromMessageId, Message message) throws TelegramApiException {
        if (masterBot == null)
            return new Result(ResultStatus.CRITICAL, "Мастер-бот не настроен.");

        final Role userRole = getRole(fromUserId);
        if (userRole.getNumber() < Role.ADMIN.getNumber())
            return new Result(ResultStatus.NOT_ALLOWED);

        String fileId = null;
        if (message.hasPhoto()) {
            int size = 0;
            for (PhotoSize photo: message.getPhoto()) {
                if (photo.getHeight() > size) {
                    size = photo.getHeight();
                    fileId = photo.getFileId();
                }
            }
        } else if (message.hasVideo()) {
            fileId = message.getVideo().getFileId();
        } else if (message.hasVideoNote()) {
            fileId = message.getVideoNote().getFileId();
        } else if (message.hasAudio()) {
            fileId = message.getAudio().getFileId();
        } else if (message.hasVoice()) {
            fileId = message.getVoice().getFileId();
        } else if (message.hasDocument()) {
            fileId = message.getVoice().getFileId();
//        } else if (message.hasSticker()) {
//            fileId = message.getSticker().getFileId();
        }

        final File file = fileId == null ? null : getFile(fileId);

        return masterBot.resendToAdmin(message, file,
                reply -> sendReplyOrError(fromChatId, fromMessageId, reply),
                ex -> onError(fromChatId, fromMessageId, ex));
    }

    private static String getCaption(String prefix, Message message) {
        String caption = message.getCaption();
        if (caption == null) {
            caption = "";
        }
        if (prefix != null) {
            caption = prefix + caption;
        }
        return caption;
    }

    protected static String getUserName(UserDto user) {
        if (user == null)
            return null;
        return user.getDescription();
    }

    protected static UserDto cloneUser(UserDto user) {
        if (user == null)
            return null;
        return user.clone();
    }
}
