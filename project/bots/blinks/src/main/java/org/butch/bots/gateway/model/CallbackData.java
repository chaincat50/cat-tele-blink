package org.butch.bots.gateway.model;

import com.fasterxml.jackson.annotation.JsonProperty;

// Must not be grater than 64 bytes when serialized.
public class CallbackData {
    public static final int BACK_INDEX = -1;
    public static final int BLANK_IMAGE = -1;

    private Stage stage; // st
    private Integer imageId; // i
    private Integer selectedImage; // s
    private int groupIndex = BACK_INDEX; // g
    private int regionIndex = BACK_INDEX; // r
    private int colorIndex = BACK_INDEX; // c

    @JsonProperty("st")
    public Stage getStage() {
        return stage;
    }

    @JsonProperty("st")
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @JsonProperty("i")
    public Integer getImageId() {
        return imageId;
    }

    @JsonProperty("i")
    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    @JsonProperty("s")
    public Integer getSelectedImage() {
        return selectedImage;
    }

    @JsonProperty("s")
    public void setSelectedImage(Integer selectedImage) {
        this.selectedImage = selectedImage;
    }

    @JsonProperty("g")
    public int getGroupIndex() {
        return groupIndex;
    }

    @JsonProperty("g")
    public void setGroupIndex(int groupIndex) {
        this.groupIndex = groupIndex;
    }

    @JsonProperty("r")
    public int getRegionIndex() {
        return regionIndex;
    }

    @JsonProperty("r")
    public void setRegionIndex(int regionIndex) {
        this.regionIndex = regionIndex;
    }

    @JsonProperty("c")
    public int getColorIndex() {
        return colorIndex;
    }

    @JsonProperty("c")
    public void setColorIndex(int colorIndex) {
        this.colorIndex = colorIndex;
    }

    public enum Stage {
        @JsonProperty("1")
        SELECT,
        @JsonProperty("2")
        GROUP,
        @JsonProperty("3")
        REGION,
        @JsonProperty("4")
        COLOR,
        @JsonProperty("5")
        FINAL
    }
}
