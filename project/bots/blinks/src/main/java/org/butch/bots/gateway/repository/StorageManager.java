package org.butch.bots.gateway.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.butch.bots.gateway.model.CommonDto;
import org.butch.encryption.CryptoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKey;

public class StorageManager {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final Storage storage;
    private final SecretKey secretKey;
    private final ObjectMapper mapper = new ObjectMapper();

    public StorageManager(Storage storage, SecretKey secretKey) {
        this.storage = storage;
        this.secretKey = secretKey;
    }

    public void init() {
        storage.init();
    }

    public BlinksGatewayConfigDto getConfig() {
        CommonDao dao = null;
        try {
            dao = storage.getConfig();
            if (dao == null) {
                final BlinksGatewayConfigDto ret = new BlinksGatewayConfigDto();
                final int id = setConfig(ret); // Save default config to database
                if (id > 0) {
                    ret.setId(id);
                }
                return ret;
            }
            return toDto(dao, BlinksGatewayConfigDto.class);
        } catch (Throwable ex) {
            LOGGER.warn("getConfig: " + dao, ex);
            return new BlinksGatewayConfigDto();
        }
    }

    public int setConfig(BlinksGatewayConfigDto dto) {
        try {
            final CommonDao dao = fromDto(dto);
            return storage.setConfig(dao);
        } catch (Throwable ex) {
            LOGGER.warn("setConfig: " + dto, ex);
            return -1;
        }
    }

    private <T extends CommonDto> CommonDao fromDto(T dto) throws Exception {
        final CommonDao dao = new CommonDao();
        dao.setId(dto.getId());
        dao.setBody(CryptoUtils.encrypt(secretKey, mapper.writeValueAsString(dto)));
        return dao;
    }

    private <T extends CommonDto> T toDto(CommonDao dao, Class<T> tClass) throws Exception {
        final T dto = mapper.readValue(CryptoUtils.decrypt(secretKey, dao.getBody()), tClass);
        dto.setId(dao.getId());
        return dto;
    }
}
