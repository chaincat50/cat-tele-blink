package org.butch.bots.blinks.commands.admin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class RevokeCommand extends AdminCommand {
    public RevokeCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "revoke";
    }

    @Override
    public String getDescription() {
        return "<token> Отозвать токен";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;token&gt;\n" +
                "Запрос на отзыв токена. " +
                "Отозванный токен уже нельзя будет использовать для подключения к боту. " +
                "Нельзя отозвать уже использованный токен.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String token = msg.getParams().trim();
        if (bot.revoke(token)) {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Токен отозван и больше не может быть использован."
            );
            return new CommandResult(ResultStatus.OK, "");
        } else {
            bot.sendReply(msg.getChatId(), msg.getMessage().getMessageId(), "" +
                    "Не удалось отозвать токен. " +
                    "Возможно, он уже был использован, или удалён по таймеру."
            );
            return new CommandResult(ResultStatus.PARAMS_ERROR, "");
        }
    }
}
