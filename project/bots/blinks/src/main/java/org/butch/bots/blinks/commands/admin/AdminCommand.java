package org.butch.bots.blinks.commands.admin;

import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.commands.BlinksCommand;
import org.butch.bots.blinks.model.Role;

public abstract class AdminCommand extends BlinksCommand {
    public AdminCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public Role getRole() {
        return Role.ADMIN;
    }
}
