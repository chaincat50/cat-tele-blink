package org.butch.bots.blinks.repository;

import java.util.List;

public interface Storage {
    void init();

    CommonDao getConfig();
    int setConfig(CommonDao config);

    int insertUser(CommonDao user);
    boolean updateUser(CommonDao user);
    boolean deleteUser(CommonDao user);
    List<CommonDao> queryUsers();

    boolean insertLogEntry(CommonDao logEntry);
    List<CommonDao> queryLogEntries(int count);
}
