package org.butch.bots.blinks.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public abstract class LoggableCommand extends Command<BlinksBot> {
    private static final int MAX_PARAM_LENGTH = 64;

    public LoggableCommand(BlinksBot bot) {
        super(bot);
    }

    public void process(CommandMessage msg) throws TelegramApiException {
        final CommandResult result = perform(msg);
        if (result == null)
            return;
        final String command = getCommandLog(msg, result.getParams());
        bot.log(msg.getChatId(), msg.getUserId(), command, result.getStatus());
    }

    public String getCommandLog(CommandMessage msg, String params) {
        if (params == null || params.isEmpty()) {
            return msg.getCommand();
        } else  {
            if (params.length() > MAX_PARAM_LENGTH) {
                return msg.getCommand() + " " + params.substring(0, MAX_PARAM_LENGTH - 3) + "...";
            } else {
                return msg.getCommand() + " " + params;
            }
        }
    }

    public abstract CommandResult perform(CommandMessage msg) throws TelegramApiException;
}
