package org.butch.bots.blinks.repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.butch.bots.blinks.Constants;
import org.butch.bots.blinks.model.CommonDto;
import org.butch.utils.TimeUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlinksConfigDto extends CommonDto {
    // ID of the admin that was last to change config.
    private String adminId = "";
    // Name of the admin that was last to change config.
    private String adminName = "";
    private long tokenTimeout = 10 * TimeUtils.MS_IN_MINUTE;
    private String greeting = String.format("Привет, %s!", Constants.USER_PLACEHOLDER);
    private boolean filesAllowed = false;

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public long getTokenTimeout() {
        return tokenTimeout;
    }

    public void setTokenTimeout(long tokenTimeout) {
        this.tokenTimeout = tokenTimeout;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public boolean getFilesAllowed() {
        return filesAllowed;
    }

    public void setFilesAllowed(boolean filesAllowed) {
        this.filesAllowed = filesAllowed;
    }
}
