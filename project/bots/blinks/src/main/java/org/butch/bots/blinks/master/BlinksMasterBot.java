package org.butch.bots.blinks.master;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.butch.bots.BotLimits;
import org.butch.bots.base.messages.OtherUpdate;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.master.commands.admin.BroadcastCommand;
import org.butch.bots.blinks.master.commands.superadmin.DecodeCommand;
import org.butch.bots.blinks.master.commands.superadmin.EncodeCommand;
import org.butch.bots.blinks.master.commands.superadmin.REncodeCommand;
import org.butch.bots.blinks.master.commands.superadmin.WEncodeCommand;
import org.butch.bots.blinks.master.model.Body;
import org.butch.bots.blinks.master.model.BroadcastData;
import org.butch.bots.blinks.master.model.CallbackData;
import org.butch.bots.blinks.master.model.EncodedText;
import org.butch.bots.blinks.model.Role;
import org.butch.bots.blinks.model.UserDto;
import org.butch.bots.blinks.repository.Storage;
import org.butch.bots.common.Color;
import org.butch.bots.gateway.model.Group;
import org.butch.bots.gateway.model.Region;
import org.butch.utils.Randomizer;
import org.butch.utils.TimeUtils;
import org.jetbrains.annotations.Nullable;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;

public class BlinksMasterBot extends BlinksBot {
    private static final String RSA_ALGORITHM = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding";
    private static final String SYM_ALGORITHM = "AES";
    private static final String SEPARATOR = ":";
    private static final int MIN_SALT_LENGTH = 0;
    private static final int MAX_SALT_LENGTH = 1024;
    private static final String WHITE_IMAGE_NAME = "medal_white.png";
    private static final String WHITE_IMAGE_PATH = "/" + WHITE_IMAGE_NAME;
    private static final String RED_IMAGE_NAME = "medal_red.png";
    private static final String RED_IMAGE_PATH = "/" + RED_IMAGE_NAME;
    private static final long BROADCAST_TIMEOUT_MS = TimeUtils.MS_IN_10_MIN;
    private static final String ERROR_CALLBACK_DATA = "ERROR";

    private final List<SlaveBotInfo> slaveBots = new ArrayList<>();

    private final ObjectMapper mapper = new ObjectMapper();
    private final RSAPublicKey encryptKey;
    private final RSAPrivateKey decryptKey;
    private final boolean encryptToTelegraph;

    private final Randomizer rnd = new Randomizer();
    private final TelegraphWriter writer = new TelegraphWriter();

    private final BroadcastStorage broadcastStorage = new BroadcastStorage(BROADCAST_TIMEOUT_MS);

    public BlinksMasterBot(String token, List<Group> groups, Storage storage, SecretKey secretKey,
                           boolean autoJoinAllowed, String telegraphWriter,
                           RSAPublicKey encryptKey, RSAPrivateKey decryptKey) throws Exception {
        super(token, storage, secretKey, autoJoinAllowed, null, null);
        this.encryptKey = encryptKey;
        this.decryptKey = decryptKey;
        this.encryptToTelegraph = telegraphWriter != null;

        if (groups.size() > 0) {
            final List<String> allGroups = new ArrayList<>();
            for (Group group : groups) {
                allGroups.add(group.getName());
                for (Region region : group.getRegions()) {
                    if (region.getRed() != null) {
                        slaveBots.add(new SlaveBotInfo(group.getName(), Color.RED, region.getRed()));
                    }
                    if (region.getWhite() != null) {
                        slaveBots.add(new SlaveBotInfo(group.getName(), Color.WHITE, region.getWhite()));
                    }
                }
            }
            addCommand(new BroadcastCommand(this, allGroups));
        }
        if (encryptKey != null) {
            if (encryptToTelegraph) {
                addCommand(new WEncodeCommand(this));
                addCommand(new REncodeCommand(this));
            } else {
                addCommand(new EncodeCommand(this));
            }
        }
        if (decryptKey != null) {
            addCommand(new DecodeCommand(this));
        }

        if (encryptToTelegraph) {
            writer.init(telegraphWriter,
                    getClass().getResourceAsStream(WHITE_IMAGE_PATH), WHITE_IMAGE_NAME,
                    getClass().getResourceAsStream(RED_IMAGE_PATH), RED_IMAGE_NAME);
        }
    }

    public void registerSlaveBot(BlinksBot blinksBot) {
        boolean registered = false;
        for (SlaveBotInfo slaveBotInfo : slaveBots) {
            if (slaveBotInfo.getName().equals("@" + blinksBot.getBotUser().getUserName()) ||
                    slaveBotInfo.getName().equals(blinksBot.getBotUser().getUserName())) {
                slaveBotInfo.setBot(blinksBot);
                registered = true;
                LOGGER.info("Registered slave bot: " + slaveBotInfo.getName());
            }
        }
        if (!registered) {
            LOGGER.warn("Could not register bot: " + blinksBot.getBotUser().getUserName());
        }
    }

    @Override
    protected void processOtherUpdate(OtherUpdate msg) {
        final Update update = msg.getUpdate();
        if (update.hasCallbackQuery()) {
            processCallbackQuery(update.getCallbackQuery());
        }
    }

    private void processCallbackQuery(CallbackQuery callbackQuery) {
        final User user = callbackQuery.getFrom();
        final String userId = Long.toString(user.getId());
        try {
            final AnswerCallbackQuery answer = new AnswerCallbackQuery();
            answer.setCallbackQueryId(callbackQuery.getId());
            execute(answer);

            final String data = callbackQuery.getData();
            if (data == null) {
                LOGGER.warn("Callback without data called: " + callbackQuery);
                return;
            }
            if (Objects.equals(data, ERROR_CALLBACK_DATA)) {
                sendMessage(userId, "" +
                        "В работе бота произошла ошибка. Сообщите, пожалуйста, разработчику."
                );
            }
            final CallbackData callbackData;
            try {
                callbackData = mapper.readValue(data, CallbackData.class);
            } catch (JsonProcessingException e) {
                sendMessage(userId, "" +
                        "Бот обновился и старые кнопки больше не работают. " +
                        "Отправьте, пожалуйста, команду заново."
                );
                return;
            }
            // Do switch by "callbackData.Type" when it's more than one.
            processBroadcastButton(userId, callbackQuery.getMessage().getMessageId(), callbackData);
        } catch (TelegramApiException ex) {
            onError(userId, ex);
        }
    }

    public Result resendToAdmin(Message message, File file,
                                Consumer<String> onReply,
                                Consumer<TelegramApiException> onError) {
        final List<UserDto> all = listAll();
        int successCount = 0;
        int errorsCount = 0;
        boolean hasAdminChats = false;
        final String senderStr = message.getFrom().getUserName();
        final String prefix = "Сообщение от бота: <b>@" + senderStr + "</b>\n\n";

        for (UserDto user : all) {
            if (user.getRole() == Role.ADMIN && user.getIsChat()) {
                hasAdminChats = true;
                if (sendMessageTo(user.getUserId(), prefix, message, file, onReply, onError)) {
                    ++successCount;
                } else {
                    ++errorsCount;
                }
            }
        }
        if (!hasAdminChats) {
            for (UserDto user : all) {
                if (user.getRole() == Role.ADMIN ||
                        user.getRole() == Role.SUPER_ADMIN ||
                        user.getRole() == Role.OWNER) {
                    if (sendMessageTo(user.getUserId(), prefix, message, file, onReply, ex -> {})) {
                        ++successCount;
                    } else {
                        ++errorsCount;
                    }
                }
            }
        }
        if (successCount != 0 && errorsCount == 0) {
            onReply.accept("Сообщение успешно отправлено. Количество получателей: " + successCount);
        } else if (successCount != 0) {
            onReply.accept("Сообщение отправлено не всем. Количество получателей: " + successCount);
        } else if (errorsCount != 0) {
            onReply.accept("Не получилось отправить сообщение.");
        } else {
            onReply.accept("Сообщение отправлять некому.");
        }
        return Result.OK;
    }

    public String encode(String text) throws TelegramApiException {
        final EncodedText result = encrypt(text);
        if (result == null)
            return null;
        return result.getText();
    }

    public String encodeWhite(String text) throws TelegramApiException {
        final EncodedText result = encrypt(text);
        if (result == null)
            return null;
        if (encryptToTelegraph) {
            try {
                return writer.saveTextWhite(result.getId(), result.getText());
            } catch (IOException e) {
                throw new TelegramApiException(e);
            }
        } else {
            return result.getText();
        }
    }

    public String encodeRed(String text) throws TelegramApiException {
        final EncodedText result = encrypt(text);
        if (result == null)
            return null;
        if (encryptToTelegraph) {
            try {
                return writer.saveTextRed(result.getId(), result.getText());
            } catch (IOException e) {
                throw new TelegramApiException(e);
            }
        } else {
            return result.getText();
        }
    }

    @Nullable
    private EncodedText encrypt(String text) throws TelegramApiException {
        if (encryptKey == null) {
            LOGGER.error("encryptKey == null");
            return null;
        }
        try {
            final KeyGenerator generator = KeyGenerator.getInstance(SYM_ALGORITHM);
            generator.init(256);
            final SecretKey secKey = generator.generateKey();

            final Cipher rsa = Cipher.getInstance(RSA_ALGORITHM);
            rsa.init(Cipher.ENCRYPT_MODE, encryptKey);
            final String part1 = Base64.getEncoder().encodeToString(rsa.doFinal(secKey.getEncoded()));

            final Cipher aes = Cipher.getInstance(SYM_ALGORITHM);
            aes.init(Cipher.ENCRYPT_MODE, secKey);
            final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss.SSS");
            final Body body = new Body();
            body.setId(UUID.randomUUID().toString());
            body.setTimestamp(System.currentTimeMillis());
            body.setTimeString(dateFormat.format(new Date(body.getTimestamp())));
            body.setText(text);
            body.setSalt(rnd.nextString(MIN_SALT_LENGTH, MAX_SALT_LENGTH));
            final byte[] bytes = mapper.writeValueAsBytes(body);
            final String part2 = Base64.getEncoder().encodeToString(aes.doFinal(bytes));

            final String result = part1 + SEPARATOR + part2;
            return new EncodedText(body.getId(), result);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            LOGGER.error(e.toString());
            throw new TelegramApiException(e);
        } catch (Exception e) {
            LOGGER.warn(e.toString());
            throw new TelegramApiException(e);
        }
    }

    public String decrypt(String text) throws TelegramApiException {
        if (decryptKey == null) {
            LOGGER.error("decryptKey == null");
            return null;
        }
        try {
            final String[] parts = text.split(SEPARATOR);
            if (parts.length != 2)
                throw new IllegalArgumentException("Cannot split: " + text);
            final String part1 = parts[0];
            final String part2 = parts[1];

            final Cipher rsa = Cipher.getInstance(RSA_ALGORITHM);
            rsa.init(Cipher.DECRYPT_MODE, decryptKey);
            final byte[] secBytes = rsa.doFinal(Base64.getDecoder().decode(part1));
            final SecretKey secKey = new SecretKeySpec(secBytes , 0, secBytes.length, SYM_ALGORITHM);

            final Cipher aes = Cipher.getInstance(SYM_ALGORITHM);
            aes.init(Cipher.DECRYPT_MODE, secKey);
            final byte[] bytes = aes.doFinal(Base64.getDecoder().decode(part2));
            final Body body = mapper.readValue(bytes, Body.class);
            return body.toString();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            LOGGER.error(e.toString());
            throw new TelegramApiException(e);
        } catch (Exception e) {
            LOGGER.warn(e.toString());
            throw new TelegramApiException(e);
        }
    }

    public void broadcast(String fromChatId, String fromUserId, Integer fromMessageId,
                          Color color, String group, String text) throws TelegramApiException {
        final String senderName = userStorage.visit(fromUserId, BlinksBot::getUserName);
        if (senderName == null) {
            sendReply(fromChatId, fromMessageId, ResultStatus.USER_NOT_FOUND.getDescription());
            return;
        }
        final String id = broadcastStorage.store(senderName, color, group, text, fromChatId);

        final CallbackData data = new CallbackData();
        data.setType(CallbackData.Type.BROADCAST);
        data.setData(id);

        final InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup();
        keyboard.setKeyboard(new ArrayList<>());
        keyboard.getKeyboard().add(new ArrayList<>());

        data.setAction(CallbackData.Action.ACCEPT);
        keyboard.getKeyboard().get(0).add(
                InlineKeyboardButton.builder()
                        .text("Отправить")
                        .callbackData(map(data))
                        .build()
        );

        data.setAction(CallbackData.Action.CANCEL);
        keyboard.getKeyboard().get(0).add(
                InlineKeyboardButton.builder()
                        .text("Отменить")
                        .callbackData(map(data))
                        .build()
        );

        final String message = getBroadcastMessage(color, group);
        final SendMessage sendMessage = SendMessage.builder()
                .chatId(fromChatId)
                .replyToMessageId(fromMessageId)
                .text(message)
                .parseMode("HTML")
                .disableWebPagePreview(true)
                .replyMarkup(keyboard)
                .build();
        execute(sendMessage);
    }

    private void processBroadcastButton(String chatId, Integer messageId,
                                        CallbackData callbackData) throws TelegramApiException {
        final String id = callbackData.getData();
        final BroadcastData broadcastData = broadcastStorage.poll(id);
        if (broadcastData != null) {
            chatId = broadcastData.getFromChatId();
        }
        if (callbackData.getAction() == CallbackData.Action.CANCEL) {
            final Color color = broadcastData == null ? null : broadcastData.getColor();
            final String group = broadcastData == null ? null : broadcastData.getGroup();
            final String message = getBroadcastMessage(color, group);
            final EditMessageText request = EditMessageText.builder()
                    .chatId(chatId)
                    .messageId(messageId)
                    .text(message)
                    .parseMode("HTML")
                    .disableWebPagePreview(true)
                    .build();
            execute(request);
            sendReplyOrError(chatId, messageId, "Отправка отменена.");
        } else if (callbackData.getAction() == CallbackData.Action.ACCEPT) {
            if (broadcastData == null) {
                final String message = getBroadcastMessage(null, null);
                final EditMessageText request = EditMessageText.builder()
                        .chatId(chatId)
                        .messageId(messageId)
                        .text(message)
                        .parseMode("HTML")
                        .disableWebPagePreview(true)
                        .build();
                execute(request);
                sendReplyOrError(chatId, messageId, "" +
                        "Сообщение не найдено. " +
                        "Возможно, отправка была отменена, либо время истекло. " +
                        "Повторите, пожалуйста, отправку команды заново."
                );
                return;
            }
            final Color color = broadcastData.getColor();
            final String group = broadcastData.getGroup();
            final String message = getBroadcastMessage(color, group);
            final EditMessageText request = EditMessageText.builder()
                    .chatId(chatId)
                    .messageId(messageId)
                    .text(message)
                    .parseMode("HTML")
                    .disableWebPagePreview(true)
                    .build();
            execute(request);
            final StringBuilder replyBuilder = new StringBuilder();
            for (SlaveBotInfo slaveBotInfo : slaveBots) {
                if (slaveBotInfo.matches(broadcastData.getColor(), broadcastData.getGroup())) {
                    if (slaveBotInfo.getBot() == null) {
                        LOGGER.error("Bot is not registered: " + slaveBotInfo.getName());
                        sendReplyOrError(chatId, messageId, "" +
                                "Бот не зарегистрирован: " + slaveBotInfo.getName());
                        continue;
                    }
                    slaveBotInfo.getBot().broadcastFromMaster(
                            broadcastData.getSenderName(),
                            broadcastData.getText(),
                            slaveBotInfo.getBot()::sendMessageTo,
                            reply -> replyBuilder.append("<b>INFO</b> ")
                                    .append(slaveBotInfo.getName())
                                    .append(": ")
                                    .append(reply)
                                    .append("\n"),
                            ex -> replyBuilder.append("<b>ERROR</b> ")
                                    .append(slaveBotInfo.getName())
                                    .append(": ")
                                    .append(ex.getMessage())
                                    .append("\n")
                    );
                }
            }
            try {
                sendReplyHtml(chatId, messageId, replyBuilder.toString());
            } catch (TelegramApiException e) {
                onError(chatId, messageId, e);
            }
        } else {
            LOGGER.warn("Unsupported action: " + callbackData);
            sendReplyOrError(chatId, messageId, "" +
                    "Ошибка. Действие не поддерживается: " + callbackData.getAction()
            );
        }
    }

    private String getBroadcastMessage(Color color, String group) {
        if (color == null) {
            return ".";
        } else {
            int count = 0;
            for (SlaveBotInfo slaveBotInfo : slaveBots) {
                if (slaveBotInfo.matches(color, group))
                    ++count;
            }

            final StringBuilder builder = new StringBuilder();
            builder.append("Выбран фильтр: <b>").append(color.toString().toLowerCase());
            if (group != null) {
                builder.append(" / ").append(group);
            }
            builder.append("</b>\n")
                    .append("Сообщение будет отправлено <b>").append(count).append("</b> ботам.\n")
                    .append("Подтвердите, пожалуйста, или отмените отправку. " +
                            "На принятие решения у вас 10 минут, после чего текст сообщения " +
                            "будет удалён из памяти бота.");
            return builder.toString();
        }
    }

    private String map(CallbackData data) {
        try {
            final String ret = mapper.writeValueAsString(data);
            if (ret.length() > BotLimits.MAX_CALLBACK_DATA_LENGTH) {
                LOGGER.warn("MAX_LENGTH: " + ret);
                return ERROR_CALLBACK_DATA;
            }
            return ret;
        } catch (JsonProcessingException ex) {
            LOGGER.warn("JSON: " + ex);
            return ERROR_CALLBACK_DATA;
        }
    }

    private static class SlaveBotInfo {
        private final String group;
        private final Color color;
        private final String name;
        private BlinksBot bot = null;

        private SlaveBotInfo(String group, Color color, String name) {
            this.group = group;
            this.color = color;
            this.name = name;
        }

        public String getGroup() {
            return group;
        }

        public Color getColor() {
            return color;
        }

        public String getName() {
            return name;
        }

        public BlinksBot getBot() {
            return bot;
        }

        public void setBot(BlinksBot bot) {
            this.bot = bot;
        }

        public boolean matches(Color color, String group) {
            return (color == Color.ALL || color == getColor()) &&
                    (group == null || Objects.equals(group, getGroup()));
        }
    }
}
