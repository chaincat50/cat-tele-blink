package org.butch.bots.blinks.commands.admin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Result;
import org.butch.bots.blinks.ResultStatus;
import org.butch.bots.blinks.commands.CommandResult;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class AllowFilesCommand extends AdminCommand {
    public AllowFilesCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "allow_files";
    }

    @Override
    public String getDescription() {
        return "Разрешить пользователям отправку файлов";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Запрос на разрешение пользователям отправлять произвольные файлы.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        final Result result = bot.changeAllowSendingFiles(userId, true);
        if (result == Result.OK) {
            bot.sendMessage(chatId, "" +
                    "Пересылка файлов разрешена."
            );
        } else {
            bot.sendMessage(chatId, "" +
                    "Не удалось разрешить пересылку файлов. " + result.getDescription()
            );
        }
        return new CommandResult(ResultStatus.OK, "");
    }
}
