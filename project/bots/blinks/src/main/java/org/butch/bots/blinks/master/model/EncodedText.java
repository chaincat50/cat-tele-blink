package org.butch.bots.blinks.master.model;

public class EncodedText {
    private final String id;
    private final String text;

    public EncodedText(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }
}
