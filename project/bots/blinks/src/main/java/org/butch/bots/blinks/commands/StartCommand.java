package org.butch.bots.blinks.commands;

import org.butch.bots.base.commands.BaseStartCommand;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.Constants;
import org.telegram.telegrambots.meta.api.objects.User;

public class StartCommand extends BaseStartCommand<BlinksBot> {
    public StartCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    protected String getUserGreeting(User user, String userLink) {
        final String greeting = bot.getGreeting();
        if (greeting == null || greeting.isEmpty())
            return super.getUserGreeting(user, userLink);
        return greeting.replace(Constants.USER_PLACEHOLDER, userLink);
    }
}
