package org.butch.bots.blinks.model;

public enum Status {
    // Not in system
    NONE(0),
    // Normal status. No restrictions.
    NORMAL(1),
    // Cannot send commands, send and receive messages.
    // Each attempt to do so results in a "Blocked" message from bot.
    BLOCKED(2),
    // Cannot send commands, send and receive messages.
    // Each attempt to do so looks normal, but doesn't do anything.
    SMART_BLOCKED(3);

    private final int number;

    Status(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public boolean isAllowed(Role commandRole) {
        return getNumber() >= commandRole.getNumber();
    }

    public static Status fromNumber(int number) {
        switch (number) {
            case 0:
                return NONE;

            case 1:
                return NORMAL;

            case 2:
                return BLOCKED;

            case 3:
                return SMART_BLOCKED;
        }

        throw new IllegalArgumentException("Unsupported value: " + number);
    }
}
