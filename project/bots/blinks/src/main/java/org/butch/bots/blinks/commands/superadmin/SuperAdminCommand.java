package org.butch.bots.blinks.commands.superadmin;

import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.commands.BlinksCommand;
import org.butch.bots.blinks.model.Role;

public abstract class SuperAdminCommand extends BlinksCommand {
    public SuperAdminCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public Role getRole() {
        return Role.SUPER_ADMIN;
    }
}
