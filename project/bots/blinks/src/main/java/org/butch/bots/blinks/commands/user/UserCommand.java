package org.butch.bots.blinks.commands.user;

import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.commands.BlinksCommand;
import org.butch.bots.blinks.model.Role;

public abstract class UserCommand extends BlinksCommand {
    public UserCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public Role getRole() {
        return Role.USER;
    }
}
