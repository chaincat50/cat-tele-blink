package org.butch.bots.blinks.master.model;

import org.butch.bots.common.Color;

public class BroadcastData {
    private final String senderName;
    private final Color color;
    private final String group;
    private final String text;
    private final String fromChatId;

    public BroadcastData(String senderName, Color color, String group, String text, String fromChatId) {
        this.senderName = senderName;
        this.color = color;
        this.group = group;
        this.text = text;
        this.fromChatId = fromChatId;
    }

    public String getSenderName() {
        return senderName;
    }

    public Color getColor() {
        return color;
    }

    public String getGroup() {
        return group;
    }

    public String getFromChatId() { return fromChatId; }

    public String getText() {
        return text;
    }
}
