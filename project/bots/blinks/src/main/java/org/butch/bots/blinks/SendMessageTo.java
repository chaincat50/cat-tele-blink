package org.butch.bots.blinks;

import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.function.Consumer;

@FunctionalInterface
public interface SendMessageTo<T> {
    default boolean apply(String chatId,
                          String prefix,
                          T msg,
                          Consumer<String> onReply) {
        return apply(chatId, prefix, msg, onReply, e -> {});
    }

    boolean apply(String chatId,
                  String prefix,
                  T msg,
                  Consumer<String> onReply,
                  Consumer<TelegramApiException> onError);
}
