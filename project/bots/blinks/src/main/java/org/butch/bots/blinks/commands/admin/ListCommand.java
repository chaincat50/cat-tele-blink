package org.butch.bots.blinks.commands.admin;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.blinks.BlinksBot;
import org.butch.bots.blinks.commands.CommandResult;
import org.butch.bots.blinks.model.UserDto;
import org.butch.bots.blinks.model.UsersComparator;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

public class ListCommand extends AdminCommand {
    private static final int USERS_PER_MESSAGE = 50;

    public ListCommand(BlinksBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "list";
    }

    @Override
    public String getDescription() {
        return "Напечатать список пользователей";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Печатает список пользователей, подключённых к боту.\n";
    }

    @Override
    public CommandResult perform(CommandMessage msg) throws TelegramApiException {
        final List<UserDto> users = bot.listUsers();
        users.sort(UsersComparator.INSTANCE);
        final StringBuilder builder = new StringBuilder();
        int usersCount = 0;
        int chatsCount = 0;
        for (int i = 0; i < users.size(); ++i) {
            final UserDto user = users.get(i);
            if (user.getIsChat()) {
                ++chatsCount;
            } else {
                ++usersCount;
            }
            builder.append(user.getDescription()).append("\n");
            if ((i + 1) % USERS_PER_MESSAGE == 0) {
                bot.sendMessage(msg.getChatId(), builder.toString());
                builder.setLength(0);
            }
        }
        if (users.size() % USERS_PER_MESSAGE != 0) {
            bot.sendMessage(msg.getChatId(), builder.toString());
        }
        bot.sendMessage(msg.getChatId(),
                String.format("Всего чатов: %d, пользователей: %d", chatsCount, usersCount));
        return null;
    }
}
