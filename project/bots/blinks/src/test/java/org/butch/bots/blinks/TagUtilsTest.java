package org.butch.bots.blinks;

import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class TagUtilsTest {
    @Test(timeout = 2000)
    public void testParseTag() {
        final String tagPattern = "[abcdef0-9]{32}";
        final String tag = UUID.randomUUID().toString().toLowerCase().replace("-", "");
        final String msg = "Message " + TagUtils.escape(tag);
        final String parsed = TagUtils.parse(msg, tagPattern);
        Assert.assertEquals(tag, parsed);
    }

    @Test(timeout = 2000)
    public void testLineFeed() {
        final String tagPattern = "[abcdef0-9]{32}";
        final String tag = "966d3f05d4b3427cacc96e6e8688793d";
        final String msg = "Message \n" + TagUtils.escape("966d3f05d4b3427cacc96e6e8688793d");
        final String parsed = TagUtils.parse(msg, tagPattern);
        Assert.assertEquals(tag, parsed);
    }
}
