package org.butch.cache;

import org.butch.cache.simple.SimpleCache;
import org.butch.cache.suffix.SuffixArrayCache;
import org.butch.utils.Randomizer;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;

@BenchmarkMode(Mode.AverageTime)
@Fork(value = 1, warmups = 0)
@Warmup(iterations = 3, time = 2)
@Measurement(iterations = 3, time = 5)
@State(Scope.Benchmark)
public class SearchCacheTest {
    private static final int SAMPLE_SIZE = 100_000;
    private static final int STR_MIN = 7;
    private static final int STR_MAX = 1000;

    private final Randomizer rnd;

    @Param({"", "a", "qwe", "LongLongString"})
    private String mask;

    private final SearchCache<String> simpleCache = new SimpleCache<>();
    private final SearchCache<String> suffixArrayCache = new SuffixArrayCache<>();

    public SearchCacheTest() {
        try {
            rnd = new Randomizer();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Setup(Level.Trial)
    public void setUp() {
        final List<String> strings = new ArrayList<>();
        for (int i = 0; i < SAMPLE_SIZE; ++i) {
            strings.add(rnd.nextString(STR_MIN, STR_MAX));
        }

        for (String string : strings) {
            // Init all caches here.
            simpleCache.load(string, string);
        }
    }

    @Benchmark
    public void testSimpleCache(SearchCacheTest test) {
        simpleCache.search(test.mask);
    }

    @Benchmark
    public void testSuffixArrayCache(SearchCacheTest test) {
        suffixArrayCache.search(test.mask);
    }
}
