package org.butch.encryption;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class CryptoUtils {
    public static final String KEY_ALGO = "AES";
    public static final String ENCRYPT_ALGO = "AES/ECB/PKCS5Padding";
    public static final Charset CHARSET = StandardCharsets.UTF_8;

    private CryptoUtils() {}

    public static SecretKey getAESKey() throws NoSuchAlgorithmException {
        javax.crypto.KeyGenerator keyGen = javax.crypto.KeyGenerator.getInstance(KEY_ALGO);
        keyGen.init(256, SecureRandom.getInstanceStrong());
        return keyGen.generateKey();
    }

    public static String keyToString(SecretKey key) {
        return hex(key.getEncoded());
    }

    public static SecretKey keyFromString(String str) throws DecoderException {
        return new SecretKeySpec(unhex(str), KEY_ALGO);
    }

    public static String encrypt(SecretKey key, String payload) throws Exception {
        final Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return hex(cipher.doFinal(payload.getBytes(CHARSET)));
    }

    public static String decrypt(SecretKey key, String payload) throws Exception {
        final Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return new String(cipher.doFinal(unhex(payload)), CHARSET);
    }

    static String hex(byte[] bytes) {
        return Hex.encodeHexString(bytes);
    }

    static byte[] unhex(String str) throws DecoderException {
        return Hex.decodeHex(str);
    }
}
