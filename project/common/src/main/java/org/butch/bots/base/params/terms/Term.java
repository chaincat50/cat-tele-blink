package org.butch.bots.base.params.terms;

public interface Term {
    TermType getType();
}
