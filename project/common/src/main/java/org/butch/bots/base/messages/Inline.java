package org.butch.bots.base.messages;

import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;

public class Inline extends Base {
    public Inline(Update update) {
        super(update);
    }

    @Override
    public Type getType() {
        return Type.INLINE;
    }

    public InlineQuery getQuery() {
        return getUpdate().getInlineQuery();
    }
}
