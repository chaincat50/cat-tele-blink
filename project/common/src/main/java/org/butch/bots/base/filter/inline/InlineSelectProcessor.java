package org.butch.bots.base.filter.inline;

import org.butch.bots.base.BotConsumer;
import org.butch.bots.base.filter.Filter;
import org.butch.bots.base.messages.InlineSelect;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class InlineSelectProcessor implements Filter {
    private final BotConsumer<InlineSelect> consumer;

    public InlineSelectProcessor(@NotNull BotConsumer<InlineSelect> consumer) {
        this.consumer = consumer;
    }

    @Override
    @NotNull
    public String getName() {
        return "InlineSelectProcessor";
    }

    @Override
    public void accept(@NotNull Update update) throws TelegramApiException {
        if (!update.hasChosenInlineQuery())
            return;
        consumer.consume(new InlineSelect(update));
    }
}
