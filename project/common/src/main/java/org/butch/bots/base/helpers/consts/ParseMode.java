package org.butch.bots.base.helpers.consts;

public enum ParseMode {
    MARKDOWN_V2("MarkdownV2"),
    HTML("HTML");

    private final String value;

    ParseMode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
