package org.butch.bots.base.params2;

import java.util.ArrayList;
import java.util.List;

public class Result {
    private final List<String> terms = new ArrayList<>();
    private ResultStatus status = ResultStatus.OK;
    private String error = null;

    public List<String> getTerms() {
        return terms;
    }

    public ResultStatus getStatus() {
        return status;
    }

    void setStatus(ResultStatus status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    void setError(String error) {
        this.error = error;
    }
}
