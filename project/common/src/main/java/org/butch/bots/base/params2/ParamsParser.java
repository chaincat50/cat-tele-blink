package org.butch.bots.base.params2;

import org.butch.bots.base.params2.conditions.Condition;

import javax.validation.constraints.NotNull;

public class ParamsParser {
    @NotNull
    public static Result parse(String str, Condition... conditions) {
        final Result result = new Result();

        for (Condition condition : conditions) {
            condition.reset();
        }

        int i = 0;
        int blockStart = 0;
        int current = 0;
        while (i < str.length() && current < conditions.length) {
            final Condition condition = conditions[current];
            final char c = str.charAt(i);
            if (condition.accept(c)) {
                ++i;
            } else {
                if (condition.isComplete()) {
                    ++current;
                    ++i;
                    blockStart = i;
                } else {
                    if (condition.isRequired()) {
                        result.setStatus(ResultStatus.PARSING_ERROR);
                        result.setError(str.substring(blockStart, i + 1));
                        return result;
                    } else {
                        ++current;
                        i = blockStart;
                    }
                }
            }
        }
        while (i < str.length()) {
            if (ParamUtils.isSeparator(str.charAt(i))) {
                ++i;
            } else {
                result.setStatus(ResultStatus.PARSING_ERROR);
                result.setError(str.substring(i));
                return result;
            }
        }
        while (current < conditions.length) {
            final Condition condition = conditions[current];
            condition.accept('\0');
            if (condition.isComplete() || !condition.isRequired()) {
                ++current;
            } else {
                result.setStatus(ResultStatus.NOT_ENOUGH_DATA);
                return result;
            }
        }

        for (Condition condition : conditions) {
            if (condition.isComplete()) {
                result.getTerms().add(condition.getTerm());
            } else {
                result.getTerms().add(null);
            }
        }

        return result;
    }
}
