package org.butch.bots.base.params2;

public enum ResultStatus {
    OK,
    PARSING_ERROR,
    NOT_ENOUGH_DATA
}
