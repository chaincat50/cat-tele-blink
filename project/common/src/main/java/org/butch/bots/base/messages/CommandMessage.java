package org.butch.bots.base.messages;

import org.butch.bots.base.commands.Command;
import org.telegram.telegrambots.meta.api.objects.Update;

import javax.validation.constraints.NotNull;

public class CommandMessage extends BaseMessage {
    private final String bot;
    private final String command;
    private final String params;

    public CommandMessage(@NotNull String chatId,
                          @NotNull Update update,
                          @NotNull String bot,
                          @NotNull String params,
                          @NotNull String command) {
        super(chatId, update);
        this.bot = bot;
        this.command = command;
        this.params = params;
    }

    @NotNull
    @Override
    public Type getType() {
        return Type.COMMAND;
    }

    @NotNull
    public String getBot() {
        return bot;
    }

    @NotNull
    public String getCommand() {
        return command;
    }

    @NotNull
    public String getParams() {
        return params;
    }

    @Override
    public String toString() {
        final String b = getBot();
        if (b == null || b.isEmpty()) {
            return Command.COMMAND_PREFIX + getCommand() + params;
        } else {
            return Command.COMMAND_PREFIX + getCommand() + "@" + getBot() + params;
        }
    }
}
