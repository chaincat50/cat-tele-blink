package org.butch.bots.base.filter.text;

import org.butch.bots.base.BotConsumer;
import org.butch.bots.base.MessageParser;
import org.butch.bots.base.filter.MessageFilter;
import org.butch.bots.base.messages.TextMessage;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class TextProcessor extends MessageFilter {
    private final BotConsumer<TextMessage> consumer;

    public TextProcessor(@NotNull BotConsumer<TextMessage> consumer) {
        this.consumer = consumer;
    }

    @Override
    @NotNull
    public String getName() {
        return "TextProcessor";
    }

    @Override
    protected void accept(@NotNull Update update,
                          @NotNull Message message,
                          @NotNull String chatId) throws TelegramApiException {
        if (!message.hasText())
            return;
        if (MessageParser.isCommand(message.getText()))
            return; // Don't process commands as normal text
        consumer.consume(new TextMessage(chatId, update, message.getText()));
    }
}
