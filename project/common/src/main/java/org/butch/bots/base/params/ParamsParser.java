package org.butch.bots.base.params;

import org.butch.bots.base.params.conditions.ChainCondition;
import org.butch.bots.base.params.conditions.Condition;
import org.butch.bots.base.params.terms.ListTerm;
import org.butch.bots.base.params.terms.StringTerm;
import org.butch.bots.base.params.terms.Term;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;

/**
 * Deprecated: use {@link org.butch.bots.base.params2.ParamsParser} instead.
 */
@Deprecated
public class ParamsParser {
    public static List<Term> parse(String str, Condition... conditions) throws ParseException {
        return parse(str, new ParserOptions(), conditions);
    }

    public static List<Term> parse(String str,
                                   ParserOptions options,
                                   Condition... conditions) throws ParseException {
        final ChainCondition chainCondition = ChainCondition.create(conditions);
        chainCondition.reset();
        int index = 0;
        while (index < str.length()) {
            final char c = str.charAt(index);
            try {
                if (options.getFirstLine() && c == '\n') {
                    final Result result = chainCondition.accept('\0');
                    if (result == Result.EXACT_MATCH) {
                        final List<Term> ret = chainCondition.getTerms();
                        if (index + 1 < str.length()) {
                            final String rest = str.substring(index + 1).trim();
                            if (!rest.isEmpty()) {
                                ret.add(new StringTerm(rest));
                            }
                        }
                        return ret;
                    } else {
                        return Collections.singletonList(new StringTerm(str));
                    }
                }
                final Result result = chainCondition.accept(c);
                if (result == Result.EXACT_MATCH) {
                    final ListTerm listTerm = chainCondition.getTerm();
                    final List<Term> ret = listTerm.getTerms();
                    if (index + 1 < str.length()) {
                        final String rest = str.substring(index + 1).trim();
                        if (!rest.isEmpty()) {
                            ret.add(new StringTerm(rest));
                        }
                    }
                    return ret;
                }
                if (result == Result.NO_MATCH) {
                    return Collections.singletonList(new StringTerm(str));
                }
                ++index;
            } catch (IllegalStateException ex) {
                throw new ParseException(ex.getMessage(), index);
            }
        }
        try {
            final Result result = chainCondition.accept('\0');
            if (result == Result.EXACT_MATCH) {
                return chainCondition.getTerms();
            } else {
                return Collections.singletonList(new StringTerm(str));
            }
        } catch (IllegalStateException ex) {
            throw new ParseException(ex.getMessage(), str.length());
        }
    }
}
