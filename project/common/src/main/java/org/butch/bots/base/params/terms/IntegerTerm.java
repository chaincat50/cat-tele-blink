package org.butch.bots.base.params.terms;

public class IntegerTerm implements Term {
    private final long value;

    public IntegerTerm(long value) {
        this.value = value;
    }

    @Override
    public TermType getType() {
        return TermType.INTEGER;
    }

    public long getValue() {
        return value;
    }

    public int getInt() {
        return (int) value;
    }

    @Override
    public String toString() {
        return "integer: " + value;
    }
}
