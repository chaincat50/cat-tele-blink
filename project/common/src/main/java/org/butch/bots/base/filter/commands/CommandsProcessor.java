package org.butch.bots.base.filter.commands;

import org.butch.bots.base.Bot2;
import org.butch.bots.base.BotConsumer;
import org.butch.bots.base.CommandHolder;
import org.butch.bots.base.MessageParser;
import org.butch.bots.base.commands.AbstractHelpCommand;
import org.butch.bots.base.commands.Command;
import org.butch.bots.base.filter.MessageFilter;
import org.butch.bots.base.messages.CommandMessage;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommandsProcessor extends MessageFilter implements CommandHolder {
    private final ArrayList<Command<?>> commands = new ArrayList<>();
    private final HashMap<String, Command<?>> commandsByName = new HashMap<>();
    private BotConsumer<CommandMessage> unknownCommandProcessor = null;

    private String userName;

    @Override
    @NotNull
    public String getName() {
        return "CommandsProcessor";
    }

    public CommandsProcessor addCommand(Command<?> command) {
        commands.add(command);
        commandsByName.put(command.getCommand(), command);
        return this;
    }

    public CommandsProcessor addHelp(AbstractHelpCommand<?> command) {
        command.setHolder(this);
        commands.add(command);
        commandsByName.put(command.getCommand(), command);
        return this;
    }

    public CommandsProcessor onUnknownCommand(BotConsumer<CommandMessage> consumer) {
        unknownCommandProcessor = consumer;
        return this;
    }

    @Override
    protected void accept(@NotNull Update update,
                          @NotNull Message message,
                          @NotNull String chatId) throws TelegramApiException {
        if (!message.hasText())
            return;

        final CommandMessage commandMessage = MessageParser.parseCommand(chatId, update, message.getText());
        if (commandMessage == null)
            return;
        if (commandMessage.getBot() != null &&
                !commandMessage.getBot().isEmpty() &&
                !commandMessage.getBot().equalsIgnoreCase(userName)) {
            return; // Command for a different bot
        }

        final Command<?> command = commandsByName.get(commandMessage.getCommand());
        if (command != null) {
            command.process(commandMessage);
        } else {
            if (unknownCommandProcessor != null) {
                unknownCommandProcessor.consume(commandMessage);
            }
        }
    }

    @Override
    public boolean register(@NotNull Bot2 bot) throws TelegramApiException {
        this.userName = bot.getBotUser().getUserName();

        final List<BotCommand> list = new ArrayList<>();
        for (Command<?> command : commands) {
            if (!command.addHint())
                continue;
            list.add(
                    BotCommand.builder()
                            .command(command.getCommand())
                            .description(command.getDescription())
                            .build()
            );
        }
        if (list.isEmpty())
            return true;

        final SetMyCommands request = new SetMyCommands();
        request.setCommands(list);
        return bot.execute(request);
    }

    @Override
    public List<Command<?>> getCommands() {
        return commands;
    }
}
