package org.butch.bots.base;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.BaseMessage;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.base.messages.TextMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public class MessageParser {
    public static boolean isCommand(String text) {
        return text != null && text.startsWith(Command.COMMAND_PREFIX);
    }

    public static BaseMessage parseMessage(String chatId, Update update, String text) {
        if (!(isCommand(text)))
            return new TextMessage(chatId, update, text);

        return parseCommand(chatId, update, text);
    }

    public static CommandMessage parseCommand(String chatId, Update update, String text) {
        if (!(isCommand(text)))
            return null;

        final StringBuilder commandBuilder = new StringBuilder();
        final StringBuilder botBuilder = new StringBuilder();
        final StringBuilder paramsBuilder = new StringBuilder();
        Stage stage = Stage.COMMAND;
        for (int i = Command.COMMAND_PREFIX.length(); i < text.length(); ++i) {
            final char c = text.charAt(i);
            switch (stage) {
                case COMMAND:
                    if (c == '_' || '0' <= c && c <= '9' || 'a' <= c && c <= 'z') {
                        commandBuilder.append(c);
                    } else if (c == '@') {
                        stage = Stage.BOT_NAME;
                    } else {
                        stage = Stage.PARAMS;
                        paramsBuilder.append(c);
                    }
                    break;

                case BOT_NAME:
                    if (c == '_' || '0' <= c && c <= '9' || 'a' <= c && c <= 'z' || 'A' <= c && c <= 'Z') {
                        botBuilder.append(c);
                    } else {
                        stage = Stage.PARAMS;
                        paramsBuilder.append(c);
                    }
                    break;

                case PARAMS:
                    paramsBuilder.append(c);
                    break;
            }
        }

        return new CommandMessage(
                chatId,
                update,
                botBuilder.toString().toLowerCase().trim(),
                paramsBuilder.toString(),
                commandBuilder.toString()
        );
    }

    private enum Stage {
        COMMAND,
        BOT_NAME,
        PARAMS
    }
}
