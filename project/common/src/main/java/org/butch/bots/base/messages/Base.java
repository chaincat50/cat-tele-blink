package org.butch.bots.base.messages;

import org.telegram.telegrambots.meta.api.objects.Update;

public abstract class Base {
    private final Update update;

    protected Base(Update update) {
        this.update = update;
    }

    public abstract Type getType();

    public Update getUpdate() {
        return update;
    }
}
