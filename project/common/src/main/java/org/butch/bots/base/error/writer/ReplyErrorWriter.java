package org.butch.bots.base.error.writer;

import org.butch.bots.base.Bot2;
import org.butch.bots.base.helpers.send.DefaultSendMessageHelper;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class ReplyErrorWriter implements ErrorWriter {
    private final DefaultSendMessageHelper helper;

    public ReplyErrorWriter(Bot2 bot) {
        this.helper = new DefaultSendMessageHelper(bot);
    }

    @Override
    public boolean accept(Update update, String filter, TelegramApiException ex) {
        if (update.hasMessage()) {
            final Message message = update.getMessage();
            final String chatId = Long.toString(message.getChatId());
            try {
                helper.reply(chatId, message.getMessageId(), ex.getMessage());
                return true;
            } catch (TelegramApiException e) {
                return false;
            }
        }
        return false;
    }
}
