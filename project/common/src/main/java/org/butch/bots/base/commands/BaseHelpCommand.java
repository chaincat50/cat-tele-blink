package org.butch.bots.base.commands;

import org.butch.bots.base.Bot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public abstract class BaseHelpCommand<TBot extends Bot> extends AbstractHelpCommand<TBot> {
    public BaseHelpCommand(TBot bot) {
        super(bot);
    }

    @Override
    protected void sendHtml(String chatId, String text) throws TelegramApiException {
        bot.sendHtml(chatId, text);
    }
}
