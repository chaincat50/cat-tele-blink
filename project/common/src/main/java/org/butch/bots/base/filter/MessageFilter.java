package org.butch.bots.base.filter;

import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public abstract class MessageFilter implements Filter {
    @Override
    public void accept(@NotNull Update update) throws TelegramApiException {
        if (!update.hasMessage())
            return;
        accept(update, update.getMessage(), Long.toString(update.getMessage().getChatId()));
    }

    protected abstract void accept(@NotNull Update update,
                                   @NotNull Message message,
                                   @NotNull String chatId) throws TelegramApiException;
}
