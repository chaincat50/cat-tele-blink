package org.butch.bots.base.params.terms;

import java.util.ArrayList;
import java.util.List;

public class ListTerm implements Term {
    private final List<Term> terms;

    public ListTerm() {
        this.terms = new ArrayList<>();
    }

    public ListTerm(List<Term> terms) {
        this.terms = terms;
    }

    @Override
    public TermType getType() {
        return TermType.LIST;
    }

    public List<Term> getTerms() {
        return terms;
    }

    @Override
    public String toString() {
        return "list: " + terms;
    }
}
