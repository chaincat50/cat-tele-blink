package org.butch.bots.base.params;

public class ParserOptions {
    // All parameters must be located on first line
    private boolean firstLine = false;

    public ParserOptions setFirstLine(boolean firstLine) {
        this.firstLine = firstLine;
        return this;
    }

    public boolean getFirstLine() {
        return firstLine;
    }
}
