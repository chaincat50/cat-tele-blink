package org.butch.bots.base.helpers;

import org.butch.bots.base.helpers.consts.ChatMemberStatus;
import org.butch.bots.base.helpers.consts.ChatType;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatMember;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMember;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class ChatHelper {
    private final TelegramLongPollingBot bot;

    public ChatHelper(TelegramLongPollingBot bot) {
        this.bot = bot;
    }

    public boolean isAllowedAdminActions(Chat chat, User user) throws TelegramApiException {
        if (chat.getType().equals(ChatType.PRIVATE.getValue()))
            return true;
        final ChatMember chatMember = getChatMember(Long.toString(chat.getId()), user.getId());
        return chatMember.getStatus().equals(ChatMemberStatus.CREATOR.getValue()) ||
                chatMember.getStatus().equals(ChatMemberStatus.ADMINISTRATOR.getValue());
    }

    public ChatMember getChatMember(String chatId, Long userId) throws TelegramApiException {
        final GetChatMember request = new GetChatMember();
        request.setChatId(chatId);
        request.setUserId(userId);
        return bot.execute(request);
    }
}
