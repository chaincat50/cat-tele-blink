package org.butch.bots.base;

import org.butch.bots.base.commands.Command;

import java.util.List;

public interface CommandHolder {
    List<Command<?>> getCommands();
}
