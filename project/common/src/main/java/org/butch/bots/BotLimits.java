package org.butch.bots;

public class BotLimits {
    private BotLimits() {}

    public static final int MAX_MESSAGE_LENGTH = 4096;
    public static final int MAX_CALLBACK_DATA_LENGTH = 64;

    public static final int MAX_INLINE_SIZE = 50;
}
