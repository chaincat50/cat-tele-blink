# Бот Мерцаний

Данный бот предназначен для анонимного группового общения через телеграмм.

Инструкция разделена на две части: установка и использование.

## Установка

### Безопасность

Если в стране, в которой запускается бот, существует опасность для его владельца - можно попросить кого-нибудь запустить бота из-за границы. В противном случае настоятельно рекомендуется следовать правилам, описанным в этом пункте.

Все манипуляции с интернетом надо проводить при включенном VPN на уровне всей системы (т.е. Opera VPN - не подойдёт). Инструкция по установке и использованию VPN не является частью этого документа.

Инструкция по установке написана для запуска на [Heroku](https://www.heroku.com). Если у вас есть собственное облачное решение - можете смело адаптировать её под себя. В качестве альтернативы можно использовать, например, [Oracle Cloud](https://www.oracle.com), [Amazon Web Services](https://aws.amazon.com), [Google Cloud](https://cloud.google.com).

В качестве сервиса для выполнения запроса по таймеру в инструкции используется [https://cron-job.org](https://cron-job.org). Вы можете поменять его на любую из существующих альтернатив, например, [EasyCron](https://www.easycron.com), [WebGazer](https://www.webgazer.io), [Healthchecks](https://healthchecks.io).

Использование альтернативных сервисов повышает безопасность.

Система, из которой устанавливается бот, должна быть зашифрована. Инструкция по шифрованию жёстких дисков не является частью этого документа.

В качестве дополнительной меры безопасности, можно проводить все манипуляции с виртуальной машины. Важно при этом, чтобы все правила безопасности распространялись и на гостевую систему тоже.

### Инструкция для программистов

Бот написан на **java 11**, сборка - на **gradle**, в качестве фреймворка используется **Spring Boot**. Данные хранятся в **PostgreSQL**.

Конфиг:

```
src/main/resources/application.yaml
```

Карта для бота Шлюз (бот не будет запущен, если список регионов пуст):

```
project/bots/blinks-gateway/src/main/resources/map.png
```

Сборка:

```bash
./gradlew build
```

Запуск:

```bash
java -jar ./build/libs/blinks-app-1.0.jar
```

Переменные окружения.

Общие:

```
JDBC_DATABASE_URL=jdbc:postgresql://<host>:<port>/<database>?<params>
```

Бот Мерцаний:

```
BLINKSBOT_ID=<telegram bot id>
BLINKSBOT_KEY=<AES-256 encryption key>
```

Переменная `BLINKSBOT_ID` игнорируется, если боты сконфигурированы через `application.yaml`.

Бот Шлюз:

```
BLINKSGATEWAYBOT_ID=<telegram bot id>
BLINKSGATEWAYBOT_KEY=<AES-256 encryption key>
```

### Пошаговая инструкция

0. Включите VPN.
1. Скачайте себе исходники бота, включая эту инструкцию.
2. Установите [git](https://git-scm.com/downloads).
3. Заведите анонимный e-mail ящик.
4. Зарегистрируйтесь в [Heroku](https://www.heroku.com), используя новый e-mail. При регистрации выберите язык `Java`.
5. Подключите к сервису кредитную карту, желательно виртуальную. Для этого перейдите в [Account Settings -> Billing](https://dashboard.heroku.com/account/billing) и заполните информацию. В отличиие от многих других сервисов, **Heroku** не проверяет карту списанием средств. **Тратить деньги не придётся.** Для чего это делается: бесплатный аккаунт имеет ряд ограничений, в частности по количеству времени в месяц, которое программа может бежать без остановки. Если карту не подключить - часть времени бот будет лежать. Подключение карты (без каких либо трат) увеличивает этот лимит до достаточного, чтобы бот бежал 24/7.
6. Установите приложение от **Heroku**: [heroku-cli](https://devcenter.heroku.com/articles/heroku-cli#download-and-install).
7. Создайте в телеграмме бота, используя [BotFather](https://t.me/botfather). Сохраните идентификатор бота, он вам пригодится. (Создайте столько ботов, сколько ботов Мерцаний вы хотите запустить, и одного для бота Шлюз, если он вам нужен).
8. Получите ключ для шифрования данных. Все данные, которые хранятся в базе данных, будут зашифрованы этим ключом. Для получения ключа, откройте в браузере страницу `tools/aes256.html` (файл надо открывать из исходников, скачанных в пункте 2). Длинный набор цифр и букв, который вы увидите, и есть нужный ключ. Обратите внимание, каждый раз при обновлении страницы генерируется новый ключ. Таким образом, ваш будет абсолютно уникальным. Сохрание его.
9. Создайте в **Heroku** приложение. Для этого перейдите в [Apps](https://dashboard.heroku.com/apps), нажмите на [Create new app](https://dashboard.heroku.com/new-app), задайте имя (используйте английские буквы, цифры и знак `"-"` и только их) и нажмите `Create app`. Запомните заданное имя.
10. Подключите базу данных. Для этого перейдите на вкладку `Resources`, вставьте в строку поиска аддонов `Heroku Postgres` и добавьте его с бесплатным планом.
11. Задайте переменные окружения. Для этого перейдите на вкладку `Settings` и нажмите на `Reveal config vars`. В появившейся форме надо будет задать две пары `KEY/VALUE`, после каждой нажимая на кнопку `Add`. Первый ключ (`KEY`): `BLINKSBOT_ID`, значение (`VALUE`): идентификатор телеграмм-бота, сохранённый в пункте 7. Второй ключ: `BLINKSBOT_KEY`, значение - ключ шифрования, сохранённый в пункте 8.

На данном этапе все подготовительные работы сделаны и можно приступать к разворачиванию и запуску приложения.

12. Создайте на вашем компьютере папку, в которой вы будете хранить исходные коды бота. Откройте командную строку в этой папке.
13. Залогиньтесь в приложении, выполнив команду `heroku login` и пройдя всю процедуру.
14. Выполните по очереди следующие команды (`<имя приложения>` - имя, заданное в пункте 9):

```bash
git init
heroku git:remote -a <имя приложения>
git add .
git commit -am "Init."
```

В результате этих действий в созданной папке должна появиться папка `.git`.

15. Скопируйте в созданную папку исходники бота, сохранённые в пункте 1. Важно при этом соблюдать структуру папок. Если вы качали архив - предварительно его распакуйте. Если в исходниках бота есть папка с именем `.git` - её копировать не надо! В результате в созданной папке рядом с `.git` должны появиться папки `gradle`, `project`, `src`, `tools` и файлы `build.gradle`, `gradle.properties`, `gradlew`, `gradlew.bat`, `LICENSE`, `Procfile`, `README.md`, `settings.gradle`, `system.properties`.
16. Если вы хотите запустить больше чем одного бота Мерцаний и/или бота Шлюз, отредактируйте файл `src/main/resources/application.yaml` и, для бота Шлюз - файл `project/bots/blinks-gateway/src/main/resources/map.png`, при необходимости (имя файла меняться при этом не должно). Пример и описание содержимого файла `application.yaml` будет приведено ниже. В этом файле так же содержатся некоторые полезные настройки.
17. Выполните по очереди следующие команды:

```bash
git add -A
git commit -m "Versin 1.0"
git push heroku master
```

Если вы всё сделали правильно, после выполнения последней команды должна начаться сборка проекта, которая завершится запуском бота. После того, как команда отработает, проверьте корректность работы приложения. Для этого откройте страницу `https://<имя приложения>.herokuapp.com/api/v1/test`, где `<имя приложения>` - имя, заданное в пункте 9 данной инструкции. Если всё прошло успешно, должна появиться надпись `Application is running!`. Если надпить появилась - откройте в телеграмме своего бота и напишите команду `/init`. Если бот ответил, что успешно проинициализирован, значит вы всё сделали правильно.

18. Вылогиньтесь из приложения, выполнив команду `heroku logout`.

Остался последний шаг. Ради экономии ресурсов, **Heroku** останавливает приложения после некоторого промежутка неактивности. К сожалению, такой подход плохо сочетается с работой телеграмм-ботов, поскольку вынуждает постоянно запускать бота вручную. К счастью, существует простой способ добиться того, чтобы приложение не останавливалось. Для этого надо сделать следующее:

19. Зайдите на сайт [https://cron-job.org](https://cron-job.org), зарегистрируйтесь в нём, используя новую почту (адрес необходимо подтвердить), [залогиньтесь](https://cron-job.org/en/members/), перейдите на [список задач](https://cron-job.org/en/members/jobs/), нажмите на [create cronjob](https://cron-job.org/en/members/jobs/add/), задайте название (`Title`), в качестве адреса пропишите `https://<имя приложения>.herokuapp.com/api/v1/test`, где `<имя приложения>` - имя, заданное в пункте 9, и нажмите на кнопку `Create cronjob` внизу страницы.

Теперь, каждые 15 минут будет посылаться запрос, который не даст **Heroku** остановить бота автоматически.

### application.yaml

Пример:

```yaml
server:
  port: ${PORT:5000}
  shutdown: graceful

logging:
  level:
    org.springframework: INFO
  pattern:
    console: "%d{yyyy-MM-dd HH:mm:ss.SSS} [%c{1}] %level - %msg%n"

blinks:
  allowAutoJoin: false
  sendUserHelp: "Послать сообщение админам."
  bots:
    - prefix: b1
      token: "abcd:1234567"
    - prefix: b2
      token: "abcd:7654321"

blinksMaster:
  allowAutoJoin: false
  prefix: master
  token: "zzzz:9999999"
  telegraphWriter: "Blinks"
  publicKey: |
    -----BEGIN PUBLIC KEY-----
         << Public key >>
    -----END PUBLIC KEY-----
  privateKey: |
    -----BEGIN PRIVATE KEY-----
         << Private key >>
    -----END PRIVATE KEY-----

blinksGateway:
  groups:
    - name: "GroupName"
      image: "map.png"
      regions:
        - name: "Region #1"
          white: "@b1_bot"
          red: "@b2_bot"
```

Описание:

- `server` - Настройки, относящиеся к серверу. Не должны меняться без веской причины.
- `logging` - Настройки, относящиеся к логам. Не должны меняться без веской причины.
- `blinks` - Настройки, относящиеся к боту Мерцаний.
  - `allowAutoJoin` - разрешить ли присоединяться к боту автоматически с правами `User` или нет. Возможные значения: `true/false`.
  - `sendUserHelp` - Описание команды `/send` для пользователей с правами `User`. Необязательный параметр. Если отсутствует, будет напечатан текст: `"Послать сообщение."`.
  - `bots` - список ботов Мерцаний. Если бот нужен только один, и он сконфигурирован через переменные окружения, этот блок можно оставить пустым.
    - `prefix` - Префикс таблиц в базе данных. Все префиксы должны быть уникальными. Должны состоять из букв латинского алфавита, цифр и символа подчёркивания. Должны начинаться с буквы. Один раз можно использовать пустую строку. Пример: если задан префикс b1, в базе данных будут созданы таблицы b1_blinks_config, b1_blinks_users и b1_blinks_logs.
    - `token` - Токен бота, полученный от BotFather (имеет вид `dheufi4u5:j0rn4pta8vb74tv74t79`).
- `blinksMaster` - Настройки, относящиеся к мастер-боту Мерцаний. Если настройки отсутствуют, или токен пустой - мастер-бот не будет запущен.
  - `allowAutoJoin` - разрешить ли присоединяться к боту автоматически с правами `User` или нет. Возможные значения: `true/false`.
  - `prefix` - Префикс таблиц в базе данных. Все правила, касающиеся префиксов ботов Мерцаний, относятся и к префиксу мастер-бота. Префик мастер-бота не должен конфликтовать с префиксами ботов Мерцаний.
  - `token` - Токен бота, полученный от BotFather (имеет вид `dheufi4u5:j0rn4pta8vb74tv74t79`).
  - `telegraphWriter` - имя, от которого закодированый текст будет залит в telegra.ph. Если параметр отсутствует, закодированый текст будет записан в сообщении. При этом, результат может не вместиться в сообщение из-за ограничений телеграмма.
  - `publicKey` - Ключ для кодирования текста. Это RSA-публичный ключ. Если параметр отсутствует - команды кодирования будут недоступны. Детали ниже.
  - `privateKey` - Ключ для раскодирования текста. Это RSA-приватный ключ. Если параметр отсутствует - команда раскодирования будет недоступна. Детали ниже.
- `blinksGateway` - Настройки, относящиеся к боту Шлюз. Если список групп пуст - бот не будет запущен.
  - `groups` -  Список групп шлюза.
    - `name` - Название группы. Должно быть достаточно коротким, чтобы поместиться на кнопку.
    - `image` - Изображение группы, загружаемое из ресурсов. Необязательный параметр. Если изображение отсутствует, будет использоваться изображение по умолчанию (`blank.png`).
    - `regions` - Список регионов группы.
      - `name` - Название региона. Должно быть достаточно коротким, чтобы поместиться на кнопку.
      - `white` - Имя "белого" бота соответствующего региона.
      - `red` - Имя "красного" бота соответствующего региона.

Боты Шлюза условно разделены на "белых" и "красных". Пользователю будет предложено выбрать цвет, после чего ему предложат перейти к общению с ботом выбранного цвета.

Мастер-бот - находится в процессе разработки.

#### Кодирование/раскодирование

Для шифрования и расшифрования используются два разных ключа. Это публичный и приватный ключи RSA алгоритма. Не смотря на название, оба ключа - секретные, и распростанять их нельзя. Рекомендуется использовать ключи размера не меньше, чем 4096.

Пример получения ключей:

```bash
openssl genrsa -out keypair.pem 4096
openssl rsa -in keypair.pem -pubout -out public.pem
openssl pkcs8 -topk8 -nocrypt -in keypair.pem -out private.pem
```

В результате в файле `public.pem` будет лежать публичный ключ, а в файле `private.pem` - приватный.

**Важно:** приватный ключ - тот, что начинается со строки `-----BEGIN PRIVATE KEY-----` а **не** `-----BEGIN RSA PRIVATE KEY-----` (последний - сохранён не в подходящем формате).

## Использование

### Роли

Роль может назначаться как на пользователя, так и на чат.

- `Owner` - владелец бота. Может быть только один. Эта роль не может назначаться на чат.
- `SAdmin` (`Super Admin`) - главный администратор бота. Может выполнять все команды бота, кроме специфичных для владельца.
  Эта роль не может назначаться на чат.
- `Admin` - администратор бота. Может выполнять некоторые команды, управляющие ботом.
- `User` - пользователь бота. Не может управлять ботом, но может использовать его для общения.
- `None` - отсутствие роли.

Эффективная роль зависит от роли пользователя, выполняющего действие, и роли чата, в котором эта команда выполняется.
Если команда выполняется в личке, то роль чата совпадает с ролью пользователя.

Эффективная роль рассчитывается по следующему правилу (в первом столбце - роль чата, в первой строке - роль пользователя):

| Роль чата | Owner | SAdmin | Admin | User | None |
|-----------|-------|--------|-------|------|------|
| Admin     | Owner | SAdmin | Admin | User | User |
| User      | Owner | User   | User  | User | User |
| None      | Owner | None   | None  | None | None |

При выполнении команды над другим пользователем действуют следующие правила:

- `Owner` - разрешено менять кого угодно.
- `SAdmin` - разрешено менять кого угодно, кроме `Owner`.
- `Admin` - разрешено менять пользователей с ролью `User` и `None`.
- `User`, `None` - не разрешено менять никого.

**Важно:** все анонимные администраторы всех чатов и каналов имеют один и тот же `UserId = 1087968824`.
По этой причине, у бота нет никакой возможности различать разных анонимных администраторов.
Если добавить одного анонимного администратора в бота, все анонимные администраторы всех чатов будут иметь ту же самую роль.
При этом, их эффективная роль будет зависеть от роли чата.

### Команды

Список команд с требуемой эффективной ролью и кратким описанием.

| Команда           | Роль   | Описание                                                                        |
|-------------------|--------|---------------------------------------------------------------------------------|
| `help`            | None   | Напечатать помощь                                                               |
| `init`            | None   | Проинициализировать бота. Выполняется один раз                                  |
| -`config`         | SAdmin | Поменять параметры бота                                                         |
| `greet`           | SAdmin | Поменять приветствие бота                                                       |
| `allow_files`     | Admin  | Разрешить пользователям отправлять файлы                                        |
| `deny_files`      | Admin  | Запретить пользователям отправлять файлы                                        |
| `owner`           | Owner  | Поменять владельца бота                                                         |
| `logs`            | SAdmin | Напечатать историю команд                                                       |
| -`stats`          | SAdmin | Напечатать статистику по чатам                                                  |
| `role`            | None   | Напечатать эффективную роль пользователя                                        |
| `list`            | Admin  | Напечатать список пользователей                                                 |
| `listadmin`       | SAdmin | Напечатать список администраторов                                               |
| `listall`         | SAdmin | Напечатать список пользователей и администраторов                               |
| `whois`           | Admin* | Детальная информация о пользователе/чате                                        |
| `whisper`         | SAdmin | Послать сообщение по заданному адресу                                           |
| `add_super_admin` | SAdmin | Добавить главного администратора                                                |
| `add_admin`       | SAdmin | Добавить администратора                                                         |
| `add_admin_chat`  | SAdmin | Добавить чат с ролью администратора                                             |
| `set_admin_chat`  | SAdmin | Дать текущему чату роль администратора                                          |
| `add_user`        | Admin  | Добавить пользователя                                                           |
| `add_user_chat`   | Admin  | Добавить чат с ролью пользователя                                               |
| `up`              | SAdmin | Повысить роль добавленного пользователя/чата                                    |
| `down`            | SAdmin | Понизить роль добавленного пользователя/чата (нельзя понизить роль ниже `User`) |
| `remove`          | Admin* | Снять роль с пользователя/чата                                                  |
| `block`           | Admin* | Заблокировать пользователя/чат                                                  |
| -`block_smart`    | Admin* | Заблокировать пользователя/чат в тайне от него                                  |
| `unblock`         | Admin* | Разблокировать пользователя/чат                                                 |
| `set_user_name`   | Admin  | Поменять имя пользователя/чата                                                  |
| `join`            | None   | Присоединиться к боту с выданным токеном                                        |
| `revoke`          | Admin* | Запретить использование неиспользованного токена                                |
| `send`            | User*  | Послать сообщение                                                               |
| `send_all`        | Admin  | Послать сообщение всем                                                          |
| `forward`         | User*  | Переслать сообщение                                                             |
| -`list_polls`     | Admin  | Напечатать список текущих опросов                                               |
| -`poll`           | Admin  | Создать опрос                                                                   |
| -`close_poll`     | Admin  | Закрыть опрос и напечатать результат                                            |

> Команды, начинающиеся с символа "-" ещё не добавлены и могут быть изменены.
>
> Роль* - команду может выполнить любой пользователь с эффективной ролью, начиная с означенной,
> но результат может различаться для пользователей с разными ролями.

Дополнительные команды мастер-бота.

| Команда      | Роль   | Описание                                                               |
|--------------|--------|------------------------------------------------------------------------|
| `encode`*    | SAdmin | Закодировать текст и написать результат в чат                          |
| `decode`*    | SAdmin | Раскодировать текст и написать результат в чат                         |
| `wencode`*   | SAdmin | Закодировать текст и залить результат в telegra.ph с белой картинкой   |
| `rencode`*   | SAdmin | Закодировать текст и залить результат в telegra.ph с красной картинкой |
| `broadcast`  | Admin  | Разослать текстовое сообщение по подчинённым ботам                     |

> Команда* - доступность команды зависит от настроек бота.

### Добавление пользователей и чатов

Общий алгоритм добавления пользователей и чатов в систему построен на выдаче временных токенов.

Список команд, которые выдают токен:

- `owner`
- `add_super_admin`
- `add_admin`
- `add_admin_chat`
- `add_user`
- `add_user_chat`

Последовательность действий:

1. Администратор или владелец бота выполняет одну из перечисленных команд и получает в качестве ответа строку - токен.
2. Данный токен передаётся тому, кого планируется добавить в бота.
3. Пользователь выполняет команду `join`, передав выданный токен в качестве параметра.
4. Пользователь (или чат, в котором была выполнена команда `join`) получает соответствующую роль.
   При этом, роль не может быть понижена. Т.е. пользователь с ролью `User` может получить роль `Admin`, но не наоборот.

У пользователя есть некоторое (конфигурируемое) ограниченное время на то, чтобы воспользоваться токеном.
Если этого не происходит - токен сгорает. Каждый токен может быть использован не более одного раза.

### Отправка сообщений

Отправка сообщений различается для пользователей с эффективной ролью `User` и пользователей с эффективной ролью `Admin` и выше.
Сперва будет описан второй случай, т.к. он более сложен и, по сути, включает в себя первый.

Сообщение может быть послано копированием (`send`/`send_all`) или пересылкой (`forward`).

Есть три способа, какими можно послать сообщение копированием (`send`/`send_all`):

1. Написать команду с текстом. В этом случае этот текст и будет отправлен.
2. Написать команду без текста. Бот ответит на неё приглашающей фразой с уникальным тэгом.
   Ответ на приглашающую фразу и будет отправлен.
   В качестве ответа можно писать не только текст. Поддерживаемые типы сообщений перечислены ниже.
   Ответить на приглашающую фразу бота может только тот пользователь, который ввёл исходную команду.
   Ответить на приглашающую фразу бота можно только один раз.
   У пользователя есть некоторое ограниченное время на то, чтобы успеть ответить.
3. Написать команду в ответ на любое сообщение. Это сообщение и будет отправлено.
   Типы сообщений, которые можно переслать таким образом, перечислены ниже.
   **Важно**: Если при этом команда будет содержать текст, то этот текст будет проигнорирован.
   Т.е. приоритет имеет сообщение, на которое идёт ответ.

**Ограничение:** API телеграмма не позволяет посылать копированием альбомы файлов (картинок, видео и др.).
Такие файлы посылаются по одному. Заголовок альбома прикрепляется к первому файлу.
Если послать альбом через сообщение с тэгом, отправлено будет только первое сообщение.

Переслать сообщение (`forward`) можно только 3-м способом. Переслать можно сообщение любого типа, без ограничений.

Куда можно отправить сообщение:

1. Если отправить сообщение без дополнительных параметров, оно будет отправлено в админские чаты (детали см. ниже).
2. Если отправить сообщение с числом в качестве парметра команды, то это сообщение будет отпралено конкретному пользователю или в чат с этим номером.
3. Если отправить сообщение командой `send_all`, то такое сообщение будет отправлено всем пользователям и чатам, подключенным к боту (детали см. ниже).

Типы сообщения, которые бот может переслать копированием (`send`):

- Текст
- Изображение
- Видео
- Видео-заметка (video note)
- Аудио
- Голосовое сообщение
- Документ (файл)
- Стикер
- Альбом вышеперечисленных файлов, с ограничениями

Как сообщение посылается админам (`send`/`forward`):

1. Если есть хотя бы один админский чат - сообщение посылается только в админские чаты.
2. Если нет ни одного админского чата - сообщение посылается в личку всем админам и владельцу бота.

Как сообщение рассылается (`send_all`/`forward all`):

1. Сообщение рассылается всем чатам и пользователям с ролью `User`.
2. Если есть хотя бы один админский чат - сообщение рассылается в админские чаты.
3. Если нет ни одного админского чата - сообщение рассылается в личку всем админам и владельцу бота.

Из всех перечисленных возможностей, пользователи с эффективной ролью `User` могут только посылать или пересылать сообщения в админские чаты.

#### Примеры

```
/send qwe
```

```
/send
qwe
```

```
/send 123
qwe
```

```
/send
123 qwe
```

```
/send_all
Hi all!!!
```

```
/send
all. Hi, admins!!!
```

### Легенда списка пользователей

| Вид                            | Комментарий                        |
|--------------------------------|------------------------------------|
| <u>**1. Владелец**</u>         | Жирный шрифт, подчёркивание        |
| ***2. Главный Администратор*** | Жирный шрифт, курсив               |
| **3. Администратор**           | Жирный шрифт                       |
| **4. [Административный чат]**  | Жирный шрифт, прямоугольные скобки |
| 5. Пользователь                |                                    |
| 6. [Пользовательский чат]      | Прямоугольные скобки               |
